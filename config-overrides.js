const { override } = require('customize-cra');
const cspHtmlWebpackPlugin = require('csp-html-webpack-plugin');
const RawSource = require('webpack-sources').RawSource;

const cspConfigPolicy = {
  'default-src': "'self'",
  'manifest-src': "'self'",
  'img-src': [
    "'self'",
    "data:",
    "*.googletagmanager.com",
  ],
  'connect-src': [
    "*.tripartie.com",
    "*.google-analytics.com",
    "o204930.ingest.sentry.io",
    "*.payline.com",
  ],
  'script-src': [
    "'self'",
    "'unsafe-inline'",
    "*.googletagmanager.com",
  ],
  'style-src': [
    "'self'",
    "'nonce-$ssl_session_id'",
  ],
  'object-src': "'none'",
  'require-trusted-types-for': "'script'",
};

function generateNginxHeaderFile(
  builtPolicy,
  _htmlPluginData,
  _obj,
  compilation
) {
  const sentryCspReportUrl = process.env.REACT_APP_SENTRY_CSP_REPORT_URL || 'https://o204930.ingest.sentry.io/api/5653358/security/?sentry_key=c035321391594731aae9e2fd3f0618e2';
  const reportToHeader = `add_header Report-To '{"max_age":31536000,"endpoints":[{"url":"${sentryCspReportUrl}"}]}';`;
  const cspHeader = `add_header Content-Security-Policy-Report-Only "${builtPolicy} report-to default; report-uri ${sentryCspReportUrl};";`;
  compilation.emitAsset('nginx-csp-header.conf', new RawSource(`${reportToHeader}\n${cspHeader}`));
}

function addCspHtmlWebpackPlugin(config) {
  if (process.env.NODE_ENV === 'production') {
    config.plugins.push(new cspHtmlWebpackPlugin(cspConfigPolicy, {
      nonceEnabled: {
        'script-src': false,
        'style-src': false
      },
      processFn: generateNginxHeaderFile
    }));
    
    config.optimization.minimize = process.env.REACT_APP_DEBUG !== "true";
  }

  return config;
}

module.exports = {
  webpack: override(addCspHtmlWebpackPlugin),
};