import * as React from 'react';

const IconNum4 = (props) => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" {...props}>
    <path
      d="M12.004 0c-9.6 0-12 2.4-12 12s2.4 12 12 12 12-2.4 12-12-2.4-12-12-12z"
      fill="currentColor"
      fillRule="evenodd"
    />
    <text
      transform="translate(12 17.103)"
      fill="#fff"
      fontSize={16.941}
      fontFamily="CircularStd-Bold,Circular Std"
      fontWeight={700}
    >
      <tspan x={-4.65} y={0}>
        4
      </tspan>
    </text>
  </svg>
);

export default IconNum4;
