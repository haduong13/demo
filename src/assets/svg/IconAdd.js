import * as React from 'react';

const IconAdd = (props) => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" {...props}>
    <defs>
      <style>
        {
          '.icon-add_svg__a{fill:none;stroke:currentColor;stroke-miterlimit:10;stroke-width:1.5px}'
        }
      </style>
    </defs>
    <path className="icon-add_svg__a" d="M16 8H0M8 16V0" />
  </svg>
);

export default IconAdd;
