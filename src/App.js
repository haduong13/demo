// Intl import, don't move this !!
import 'intl';
import 'intl/locale-data/jsonp/fr';
import { IntlProvider } from 'react-intl';
// Intl import

import React, {
  useState,
  useEffect,
} from 'react';
import {
  Route,
  Switch,
  useLocation,
} from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import * as Sentry from '@sentry/react';
import { Integrations } from '@sentry/tracing';
import Container from 'react-bootstrap/Container';
import { useTranslation } from 'react-i18next';
// import { CookieBanner } from '@palmabit/react-cookie-law';

import PrivateRoute from 'helpers/PrivateRoute';
import LoadingScreenContextProvider from 'helpers/LoadingScreenContext';
import LoadingScreen from 'components/LoadingScreen';

import { logout } from 'logic/actions/authActions';

import {
  isProd,
  sentryDSN,
  envName,
} from 'helpers/env';

import Login from 'pages/Login';
import Signup from 'pages/Signup';
import Home from 'pages/Home';
import Settings from 'pages/Settings';
import ResetPassword from 'pages/resetPassword/ResetPassword';
import RequestResetPassword from 'pages/resetPassword/RequestResetPassword';
import TransactionPostProcessPreauth from 'pages/transaction/TransactionPostProcessPreauth';
import Transaction from 'pages/transaction/Transaction';
import CheckoutPage from 'pages/checkout';

if (isProd) {
  Sentry.init({
    dsn: sentryDSN,
    integrations: [new Integrations.BrowserTracing()],
    environment: envName,

    // We recommend adjusting this value in production, or using tracesSampler
    // for finer control
    tracesSampleRate: 1.0,
  });
  Sentry.setUser({ email: 'Not logged in' });
}

const App = () => {
  console.log(process.env.REACT_APP_API_URL);
  const { t, i18n } = useTranslation('app');
  const location = useLocation();
  const [allowAnalytics, setAllowAnalytics] = useState(true); // eslint-disable-line no-unused-vars

  useEffect(() => {
    if (allowAnalytics) {
      window.gtag('send', 'page_view', {
        page_path: location.pathname + location.search,
      });
    }
  }, [allowAnalytics, location]);

  return (
    <IntlProvider locale={i18n.language}>
      <LoadingScreenContextProvider>
        <Helmet>
          <title>Tripartie</title>
          <meta name="description" content={t('websiteDescription')} />
        </Helmet>
        <LoadingScreen />
        <Container className="d-flex flex-column min-vh-100" fluid="md">
          <div className="d-flex">
            <Switch>
              <Route exact path="/login" component={Login} />
              <Route exact path="/signup" component={Signup} />
              <Route exact path="/reset-password/request" component={RequestResetPassword} />
              <Route exact path="/reset-password/:token" component={ResetPassword} />
              <Route exact path="/secured-payment/:templateUuid" component={CheckoutPage} />

              <PrivateRoute exact path="/transactions/:transactionId/post-process-preauth" component={TransactionPostProcessPreauth} />
              <PrivateRoute exact path="/" component={Home} />
              <PrivateRoute path="/settings" component={Settings} />
              <PrivateRoute exact path="/transactions/:transactionId" component={Transaction} />
            </Switch>
          </div>
          {/* <CookieBanner
            message={t('cookies.msg')}
            wholeDomain
            policyLink="/terms"
            privacyPolicyLinkText={t('cookies.privacyPolicy')}
            necessaryOptionText={t('cookies.necessary')}
            statisticsOptionText={t('cookies.statistics')}
            acceptButtonText={t('cookies.accept')}
            managePreferencesButtonText={t('cookies.settings')}
            savePreferencesButtonText={t('cookies.saveSettings')}
            showPreferencesOption={false}
            showMarketingOption={false}
            onAccept={() => {}}
            onAcceptPreferences={() => {}}
            onAcceptMarketing={() => {}}
            onAcceptStatistics={() => { setAllowAnalytics(true); }}
            onDeclineStatistics={() => { setAllowAnalytics(false); }}
            styles={{
              dialog: {
                position: 'fixed',
                bottom: 0,
                padding: 10,
                backgroundColor: 'white',
                borderColor: '#d9dbdf',
                borderWidth: 1,
                borderStyle: 'solid',
              }
            }}
          /> */}
        </Container>
      </LoadingScreenContextProvider>
    </IntlProvider>
  );
};

const mapStateToProps = (state) => ({ loggedIn: state.persistent.authReducer.loggedIn });

const mapDispatchToProps = (dispatch) => ({ logout: bindActionCreators(logout, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(App);
