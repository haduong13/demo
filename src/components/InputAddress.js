import React from "react";
import PlacesAutocomplete from "react-places-autocomplete";
import iconMap from "assets/images/icon-map.svg";
import iconClose from "assets/images/icon-close.svg";
import "assets/style/modal-payment.scss";

const InputAddress = ({
  handleChangeAddress,
  handleSelectAddress,
  address,
  onHandleRemoveInput
}) => (
  <PlacesAutocomplete
    value={address}
    onChange={handleChangeAddress}
    onSelect={handleSelectAddress}
    debounce={500}
  >
    {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
      <div className="form-group form-group-input-address">
        <label>Shipping address</label>
        <div className="input-address-custom">
          <img className="icon-map" src={iconMap} alt="" />
          <input
            {...getInputProps({
              placeholder: "",
              className: "form-control input-form",
            })}
          />
          <img onClick={onHandleRemoveInput} className="icon-close" src={iconClose} alt="" />
        </div>
        <div className="autocomplete-dropdown-container autocomplete-dropdown-container-custom">
          {loading && <div>Loading...</div>}
          {suggestions.map((suggestion) => {
            const className = suggestion.active
              ? "suggestion-item--active"
              : "suggestion-item";
            const style = suggestion.active
              ? {
                  backgroundColor: "#D9DBDF",
                  cursor: "pointer",
                }
              : {
                  backgroundColor: "#ffffff",
                  cursor: "pointer",
                };
            return (
              <div
                {...getSuggestionItemProps(suggestion, {
                  className,
                  style,
                })}
              >
                <span className="text-item-address">
                  {suggestion.description}
                </span>
              </div>
            );
          })}
        </div>
      </div>
    )}
  </PlacesAutocomplete>
);

export default InputAddress;
