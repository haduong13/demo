import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import { Formik } from 'formik';
import * as yup from 'yup';
import { useTranslation } from 'react-i18next';
import Button from 'components/Button';
import iconClose from 'assets/images/icon-close.svg';

const CompleteTransactionModal = ({
  visible,
  setVisible,
  onSubmit,
}) => {
  const { t } = useTranslation('completeTransactionModal');

  return (
    <Formik
      validateOnChange={false}
      validateOnBlur={false}
      validationSchema={yup.object({
        verificationCode: yup.string().required(t('emptyVerificationCode')),
      })}
      onSubmit={onSubmit}
      initialValues={{ verificationCode: '' }}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        touched,
        errors,
        isSubmitting,
        resetForm,
      }) => {
        const resetAndHide = () => {
          resetForm();
          setVisible(false);
        };

        return (
          <Modal
            className="checkout-modal-counter-offer"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            show={visible}
            onHide={resetAndHide}
          >
            <Form
              noValidate
              onSubmit={handleSubmit}
            >
              <Modal.Header>
                <p className="modal__title">{t('title')}</p>
                <div
                  onClick={resetAndHide}
                  onKeyPress={resetAndHide}
                  className="modal__icon-close"
                  role="button"
                  tabIndex={0}
                >
                  <img
                    src={iconClose}
                    alt=""
                  />
                </div>
              </Modal.Header>
              <Modal.Body>
                <p>
                  {t('msg')}
                </p>
                <div>
                  <InputGroup className="mb-3">
                    <FormControl
                      placeholder={t('placeholder')}
                      name="verificationCode"
                      value={values.verificationCode}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isInvalid={touched.verificationCode && !!errors.verificationCode}
                      autoFocus
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.verificationCode}
                    </Form.Control.Feedback>
                  </InputGroup>
                  <Button
                    type="submit"
                    disabled={!values.verificationCode}
                    isLoading={isSubmitting}
                    className="w-100 btn-block checkout__btn client-primary-background-color border-0"
                  >
                    {t('submit')}
                  </Button>
                </div>
              </Modal.Body>
            </Form>
          </Modal>
        );
      }}
    </Formik>
  );
};

export default CompleteTransactionModal;
