import React from 'react';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import InputAddress from 'components/InputAddress';
import 'assets/style/modal-payment.scss';

const InfoAddressFormLimit = ({
  handleChangeAddress,
  handleSelectAddress,
  address,
  onHandleRemoveInput
}) => (
  <>
    <Row>
      <Col className="form-group-border-center-left form-group-border-t-b-l mb-25-custom">
        <Form.Group>
          <Form.Control type="text" />
        </Form.Group>
      </Col>
      <Col className="form-group-border-center-right form-group-border-t-b-r mb-25-custom">
        <Form.Group>
          <Form.Control type="text" />
        </Form.Group>
      </Col>
    </Row>
    <Row>
      <Col className="mb-25-custom">
        <InputAddress
          address={address}
          handleChangeAddress={handleChangeAddress}
          handleSelectAddress={handleSelectAddress}
          onHandleRemoveInput={onHandleRemoveInput}
        />
      </Col>
    </Row>
  </>
);

export default InfoAddressFormLimit;
