import React from 'react';
import IconArrowLeft from 'assets/images/iconmonstr-angel-left-thin.svg';

const CheckoutBreadcrumbBar = ({
  adUrl,
  client,
  clientSquareLogo,
}) => (
  <p
    className="m-0 align-items-center checkout-breadcrumb d-flex"
  >
    <a href={adUrl}>
      <img src={IconArrowLeft} width="12px" alt="icon" />
      {clientSquareLogo && (<img className="mx-2" src={clientSquareLogo} width="44px" alt="icon" />)}
      <span className="checkout-breadcrumb__link">{client}</span>
    </a>
  </p>
);

export default CheckoutBreadcrumbBar;
