import React from 'react';
import _ from 'lodash';
import { Formik } from 'formik';
import * as yup from 'yup';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'components/Button';
import { useTranslation } from 'react-i18next';
import { isValidIBAN } from 'ibantools';
import 'assets/style/modal-payment.scss';

const BankAccountForm = ({
  onSubmit,
  countries = [],
  initialValues = {},
}) => {
  const { t } = useTranslation(['bankAccountForm', '_countries']);

  return (
    <Formik
      validateOnChange={false}
      validateOnBlur={false}
      validationSchema={yup.object({
        iban: yup
          .string()
          .required(t('emptyIban'))
          .test('testIban', t('invalidIban'), (iban = '') => isValidIBAN(iban.replaceAll(' ', ''))),
        addressLine1: yup.string().required(t('emptyAddressLine1')),
        addressLine2: yup.string(),
        postcode: yup.string().required(t('emptyPostcode')),
        city: yup.string().required(t('emptyCity')),
        region: yup.string(),
        country: yup.string().required(t('emptyCountry')),
      })}
      onSubmit={(
        {
          iban,
          addressLine1,
          addressLine2 = null,
          postcode,
          city,
          region = null,
          country,
        },
        formikBag,
      ) => {
        onSubmit(
          {
            iban: iban.replaceAll(' ', ''),
            address: {
              addressLine1,
              addressLine2,
              postcode,
              city,
              region,
              country,
            },
          },
          formikBag,
        );
      }}
      initialValues={{
        iban: '',
        addressLine1: '',
        addressLine2: '',
        postcode: '',
        city: '',
        region: '',
        country: '',
        ...initialValues,
      }}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        touched,
        errors,
        isSubmitting,
      }) => (
        <Form noValidate onSubmit={handleSubmit}>
          <span className="modal-add-payment__text-holder-address">
            {t('iban')}
          </span>
          <Row>
            <Col className="form-group-top">
              <Form.Group controlId="iban">
                <Form.Control
                  type="text"
                  name="iban"
                  value={values.iban}
                  onChange={(e) => {
                    e.target.value = e.target.value.replaceAll(
                      /([^ ]{4}) *(?=[^ ])/g,
                      '$1 ',
                    );
                    handleChange(e);
                  }}
                  placeholder="FR76 XXXX XXXX XXXX XXXX"
                  onBlur={handleBlur}
                  isInvalid={touched.iban && !!errors.iban}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.iban}
                </Form.Control.Feedback>
              </Form.Group>
            </Col>
          </Row>
          <span className="modal-add-payment__text-holder-address">
            {t('addressLine1')}
          </span>
          <Row>
            <Col className="form-group-border-top">
              <Form.Group controlId="addressLine1">
                <Form.Control
                  type="text"
                  name="addressLine1"
                  value={values.addressLine1}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isInvalid={touched.addressLine1 && !!errors.addressLine1}
                  placeholder={t('addressLine1')}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.addressLine1}
                </Form.Control.Feedback>
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col className="form-group-border-center-left">
              <Form.Group controlId="postcode">
                <Form.Control
                  type="text"
                  name="postcode"
                  value={values.postcode}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isInvalid={touched.postcode && !!errors.postcode}
                  placeholder={t('postcode')}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.postcode}
                </Form.Control.Feedback>
              </Form.Group>
            </Col>
            <Col className="form-group-border-center-right">
              <Form.Group controlId="city">
                <Form.Control
                  type="text"
                  name="city"
                  value={values.city}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isInvalid={touched.city && !!errors.city}
                  placeholder={t('city')}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.city}
                </Form.Control.Feedback>
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col className="form-group-border-bottom">
              <Form.Group controlId="country">
                <Form.Control
                  as="select"
                  className={`form-control ${!values.country ? 'placeholder' : ''}`}
                  custom
                  name="country"
                  value={values.country}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isInvalid={touched.country && !!errors.country}
                >
                  <option value="" className="placeholder">{t('country')}</option>
                  {_.sortBy(countries, ({ slug }) => (t(`_countries:country.${slug}`))).map(({ slug }) => (
                    <option key={slug} value={slug} className="color-input">{t(`_countries:country.${slug}`)}</option>
                  ))}
                </Form.Control>
                <Form.Control.Feedback type="invalid">
                  {errors.country}
                </Form.Control.Feedback>
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col className="form-group-border-bottom">
              <Button
                variant="primary"
                type="submit"
                disabled={!values.iban || !values.addressLine1 || !values.postcode || !values.city || !values.country}
                isLoading={isSubmitting}
                className="modal-add-payment__button-submit"
              >
                {t('submit')}
              </Button>
            </Col>
          </Row>
        </Form>
      )}
    </Formik>
  );
};

export default BankAccountForm;
