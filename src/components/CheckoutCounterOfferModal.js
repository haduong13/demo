import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import { Formik } from 'formik';
import * as yup from 'yup';
import { useTranslation } from 'react-i18next';
import Button from 'components/Button';
import useCurrencyHelpers from 'helpers/useCurrencyHelpers';
import IconCoin from 'assets/svg/IconCoin';

const CheckoutCounterOfferModal = ({
  visible,
  setVisible,
  transactionTemplate: {
    currency = 'EUR',
  } = {},
  onSubmit,
}) => {
  const { t } = useTranslation('checkoutCounterOfferModal');
  const { symbol, before } = useCurrencyHelpers(currency);

  return (
    <Formik
      validateOnChange={false}
      validateOnBlur={false}
      validationSchema={yup.object({
        subTotal: yup.number().required(t('emptySubTotal')),
      })}
      onSubmit={onSubmit}
      initialValues={{ subTotal: '' }}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        touched,
        errors,
        isSubmitting,
        resetForm,
      }) => {
        const resetAndHide = () => {
          resetForm();
          setVisible(false);
        };

        return (
          <Modal
            className="checkout-modal-counter-offer"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            show={visible}
            onHide={resetAndHide}
          >
            <Form
              noValidate
              onSubmit={handleSubmit}
            >
              <Modal.Header closeButton className="border-0 pb-0" />
              <Modal.Body className="pt-0">
                <div className="text-center">
                  <div>
                    <IconCoin width="140px" />
                  </div>
                  <h4 className="mt-3 mb-3 checkout__title">{t('title')}</h4>
                  <p>
                    {t('subTitle')}
                  </p>
                </div>
                <div>
                  <div>
                    <InputGroup className="mb-3" size="lg">
                      {before && (
                        <InputGroup.Prepend>
                          <InputGroup.Text id="basic-addon2">{symbol}</InputGroup.Text>
                        </InputGroup.Prepend>
                      )}
                      <FormControl
                        type="number"
                        placeholder={t('placeholder')}
                        name="subTotal"
                        value={values.subTotal}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isInvalid={touched.subTotal && !!errors.subTotal}
                      />
                      {!before && (
                        <InputGroup.Append>
                          <InputGroup.Text id="basic-addon2">{symbol}</InputGroup.Text>
                        </InputGroup.Append>
                      )}
                      <Form.Control.Feedback type="invalid">
                        {errors.subTotal}
                      </Form.Control.Feedback>
                    </InputGroup>
                    <Button
                      type="submit"
                      disabled={!values.subTotal}
                      isLoading={isSubmitting}
                      className="w-100 btn-block checkout__btn client-primary-background-color border-0"
                    >
                      {t('submit')}
                    </Button>
                  </div>
                </div>
              </Modal.Body>
            </Form>
          </Modal>
        );
      }}
    </Formik>
  );
};

export default CheckoutCounterOfferModal;
