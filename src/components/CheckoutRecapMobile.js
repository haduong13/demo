import React from 'react';
import { useTranslation } from 'react-i18next';
import { FormattedNumber } from 'react-intl';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';

import IconInfo from 'assets/svg/IconInfo';

const CheckoutRecapMobile = ({
  isDropdown,
  transactionTemplate: {
    title,
    client,
    subTotal = 0,
    currency = 'EUR',
    clientTheme: {
      primary,
    } = {},
  } = {},
  transaction: {
    subTotal: newPrice = 0,
    buyerFees = 0,
    buyerTotal = 0,
  } = {},
}) => {
  const { t } = useTranslation('checkoutRecap');
  const hasCounterOffer = newPrice !== subTotal;

  return (
    <div>
      {!isDropdown && (
        <>
          <div className="checkout-detail-image">
            <div className="checkout-detail-image__inner" />
          </div>
          <h4 className="mt-5 mb-3">{t('title')}</h4>
        </>
      )}
      <div className="border-bottom mb-3 pt-2 pb-2">
        <div className="d-flex justify-content-between mb-3">
          <p className="m-0">{t('purchase')}</p>
          <p className="m-0 pl-5 text-right checkout-checkout__title">
            {title}
          </p>
        </div>

        <div className="d-flex justify-content-between mb-3">
          <p className="m-0">{t('client')}</p>
          <p className="m-0 text-right">{client}</p>
        </div>
      </div>

      <div className="border-bottom mb-3 pt-2 pb-2">
        <div className="d-flex justify-content-between mb-3">
          <p className="m-0">{t('subTotal')}</p>
          {hasCounterOffer ? (
            <p className="m-0 text-right">
              <del><FormattedNumber value={subTotal / 100} style="currency" currency={currency} /></del>
              {' '}
              <FormattedNumber value={newPrice / 100} style="currency" currency={currency} />
            </p>
          ) : (
            <p className="m-0 text-right"><FormattedNumber value={subTotal / 100} style="currency" currency={currency} /></p>
          )}
        </div>

        <div className="d-flex justify-content-between mb-3">
          <p className="m-0">{t('fees')}</p>
          <OverlayTrigger
            placement="right"
            overlay={
              <Tooltip>{t('feesTooltip', { client })}</Tooltip>
            }
          >
            <IconInfo className="ml-2 mr-auto" width="14px" color={primary} />
          </OverlayTrigger>
          <p className="m-0 text-right"><FormattedNumber value={buyerFees / 100} style="currency" currency={currency} /></p>
        </div>
      </div>
      {/* <div className="border-bottom mb-3 pt-2 pb-2">
        <div className="d-flex justify-content-between align-items-center mb-3">
          <p className="m-0">{t('promocode')}</p>
          <p className="m-0 text-right">
            <span className="checkout-mobile__shape">
              {t('addPromocode')}
            </span>
          </p>
        </div>
      </div> */}
      <div className="mb-3 pt-2 pb-2">
        <div className="d-flex justify-content-between mb-3">
          <p className="m-0 checkout__title">{t('total')}</p>
          <p className="m-0 text-right checkout__title"><FormattedNumber value={buyerTotal / 100} style="currency" currency={currency} /></p>
        </div>
      </div>
    </div>
  );
};

export default CheckoutRecapMobile;
