import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Button from 'components/Button';
import { Formik } from 'formik';
import * as yup from 'yup';
import { useTranslation, Trans } from 'react-i18next';
import { Link } from 'react-router-dom';
import StyledRadioButton from 'components/StyledRadioButton';
import 'assets/style/login-register.scss';

const ERROR_EMAIL_ALREADY_EXISTS = 'email_already_exists';

const SignupForm = ({
  onSubmit,
  checkEmail,
  terms,
  isCheckoutPage,
  setShowSignup = {},
  primaryColor = '#004bb4',
}) => {
  const { t } = useTranslation('signupForm');
  const [previousEmail, setPreviousEmail] = useState('');
  const [previousEmailTestResult, setPreviousEmailTestResult] = useState(true);

  return (
    <Formik
      validateOnChange={false}
      validateOnBlur={false}
      validationSchema={yup.object({
        firstName: yup.string().required(t('emptyFirstName')),
        lastName: yup.string().required(t('emptyLastName')),
        email: yup
          .string()
          .required(t('emptyEmail'))
          .email(t('invalidEmail'))
          .test({
            name: 'emailValid',
            test: (newEmail, { createError }) => {
              if (!newEmail || newEmail === previousEmail) {
                return previousEmailTestResult;
              }
              setPreviousEmail(newEmail);
              return checkEmail(newEmail)
                .then(() => {
                  setPreviousEmailTestResult(true);
                  return true;
                })
                .catch(
                  ({
                    error: { response: { data: { errors } = {} } = {} } = {},
                  }) => {
                    let result = createError({ message: t('invalidEmail') });
                    if (errors === ERROR_EMAIL_ALREADY_EXISTS) {
                      result = createError({ message: t('alreadyUsedEmail') });
                    }
                    setPreviousEmailTestResult(result);
                    return result;
                  },
                );
            },
          }),
        password: yup
          .string()
          .required(t('emptyPassword'))
          .min(10, t('invalidPassword')),
        agreeTerms: yup.boolean().oneOf([true]),
      })}
      onSubmit={onSubmit}
      initialValues={{
        email: '',
        firstName: '',
        lastName: '',
        password: '',
        agreeTerms: false,
      }}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        touched,
        errors,
        isSubmitting,
        setFieldValue,
      }) => (
        <Form className="form-login" noValidate onSubmit={handleSubmit}>
          <Form.Row>
            <Form.Group
              as={Col}
              controlId="firstName"
              className="form-group-border-top-left"
            >
              <Form.Control
                type="text"
                name="firstName"
                value={values.firstName}
                onChange={handleChange}
                onBlur={handleBlur}
                isInvalid={touched.firstName && !!errors.firstName}
                placeholder={t('firstName')}
              />
              <Form.Control.Feedback type="invalid">
                {errors.firstName}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group
              as={Col}
              controlId="lastName"
              className="form-group-border-top-right"
            >
              <Form.Control
                type="text"
                name="lastName"
                value={values.lastName}
                onChange={handleChange}
                onBlur={handleBlur}
                isInvalid={touched.lastName && !!errors.lastName}
                placeholder={t('lastName')}
              />
              <Form.Control.Feedback type="invalid">
                {errors.lastName}
              </Form.Control.Feedback>
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group
              md="4"
              controlId="email"
              className="form-group-border-center"
            >
              <Form.Control
                type="email"
                name="email"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                isInvalid={touched.email && !!errors.email}
                placeholder={t('email')}
              />
              <Form.Control.Feedback type="invalid">
                {errors.email}
              </Form.Control.Feedback>
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group
              md="4"
              controlId="password"
              className="form-group-border-bottom"
            >
              <Form.Control
                type="password"
                name="password"
                value={values.password}
                onChange={handleChange}
                onBlur={handleBlur}
                isInvalid={touched.password && !!errors.password}
                placeholder={t('password')}
              />
              <Form.Control.Feedback type="invalid">
                {errors.password}
              </Form.Control.Feedback>
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Check type="radio" id="agreeTerms" className={`text-agree ${isCheckoutPage && 'text-agree-checkout'}`}>
              <Form.Check.Input
                as={StyledRadioButton}
                color={primaryColor}
                name="agreeTerms"
                value
                checked={values.agreeTerms}
                onChange={() => {}}
                onClick={() => { setFieldValue('agreeTerms', !values.agreeTerms); }}
                className={`${isCheckoutPage && 'checkout__radio'}`}
                isInvalid={touched.agreeTerms && !!errors.agreeTerms}
              />
              <Form.Check.Label className={`${isCheckoutPage && 'ml-3'}`}>
                <Trans t={t} i18nKey="agreeTerms">
                  I agree to the
                  <Link
                    to={{ pathname: terms?.url }}
                    className={
                      touched.agreeTerms && !!errors.agreeTerms
                        ? 'text-danger'
                        : `${isCheckoutPage && 'checkout__link client-primary-color'}`
                    }
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Terms and conditions
                  </Link>
                  and the
                  <Link
                    to={{ pathname: terms?.url }}
                    className={
                      touched.agreeTerms && !!errors.agreeTerms
                        ? 'text-danger'
                        : `${isCheckoutPage && 'checkout__link client-primary-color'}`
                    }
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Privacy policy
                  </Link>
                  of Tripartie.
                </Trans>
              </Form.Check.Label>
            </Form.Check>
          </Form.Row>
          <Form.Row>
            <Button
              variant="primary"
              type="submit"
              disabled={!values.email || !values.firstName || !values.lastName || !values.password || !values.agreeTerms}
              className={`btn-block border-0 ${isCheckoutPage && 'checkout__btn client-primary-background-color'}`}
              isLoading={isSubmitting}
            >
              {t('submit')}
            </Button>
          </Form.Row>
          <p className="text-ask-account">
            <Trans t={t} i18nKey="login">
              Already have an account?
              {isCheckoutPage ? (
                <Button
                  className="p-0 checkout__link link client-primary-color"
                  onClick={() => setShowSignup(false)}
                  onKeyPress={() => setShowSignup(false)}
                >
                  login
                </Button>
              ) : (
                <Link
                  to="/login"
                  className={
                    touched.agreeTerms && !!errors.agreeTerms ? 'text-danger' : ''
                  }
                >
                  login
                </Link>
              )}
            </Trans>
          </p>
        </Form>
      )}
    </Formik>
  );
};

export default SignupForm;
