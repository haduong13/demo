import React from 'react';
import methodOne from 'assets/images/method1.svg';
import methodSecond from 'assets/images/method2.svg';
import methodThirst from 'assets/images/method3.svg';
import 'assets/style/parcel-delivery-modal.scss';

const ListDeliveryMethod = ({ onHandleConfirmMethod }) => (
  <>
    <div className="list-method-delivery">
      <div className="list-method-delivery__item">
        <input
          type="radio"
          id="pick-up"
          name="method-delivery-1"
          value=""
          defaultChecked
        />
        <label htmlFor="pick-up">
          <div className="content-method-delivery">
            <img src={methodOne} alt="" />
            <div>
              <p className="title">
                Mondial Relay:
                {' '}
                <span className="bold">€10.00</span>
              </p>
              <p>Pick-up delivery</p>
              <p className="p-last">Shipping label included</p>
            </div>
          </div>
        </label>
      </div>
      <div className="list-method-delivery__item">
        <input type="radio" id="home" name="method-delivery-1" value="" />
        <label htmlFor="home">
          <div className="content-method-delivery">
            <img src={methodSecond} alt="" />
            <div>
              <p className="title">
                Mondial Relay:
                {' '}
                <span className="bold">€10.00</span>
              </p>
              <p>Pick-up delivery</p>
              <p className="p-last">Shipping label included</p>
            </div>
          </div>
        </label>
      </div>
      <div className="list-method-delivery__item">
        <input
          type="radio"
          id="pick-up-home"
          name="method-delivery-1"
          value=""
        />
        <label htmlFor="pick-up-home">
          <div className="content-method-delivery">
            <img src={methodThirst} alt="" />
            <div>
              <p className="title">
                Mondial Relay:
                {' '}
                <span className="bold">€10.00</span>
              </p>
              <p>Pick-up delivery</p>
              <p className="p-last">Shipping label included</p>
            </div>
          </div>
        </label>
      </div>
    </div>
    <button
      onClick={onHandleConfirmMethod}
      type="button"
      className="btn parcel-delivery-modal__btn-confirm"
    >
      <span>Confirm shipping informations</span>
    </button>
  </>
);

export default ListDeliveryMethod;
