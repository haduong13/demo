import React, { useState } from 'react';
import LinesEllipsis from 'react-lines-ellipsis';
import responsiveHOC from 'react-lines-ellipsis/lib/responsiveHOC';
import { useTranslation } from 'react-i18next';

const ResponsiveReactLinesEllipsis = responsiveHOC()(LinesEllipsis);

const ResponsiveEllipsis = ({
  text,
  maxLine = 3,
  className = '',
  ...props
}) => {
  const { t } = useTranslation('responsiveEllipsis');
  const [useEllipsis, setUseEllipsis] = useState(true);

  const handleReflow = ({
    clamped,
  }) => {
    setUseEllipsis(clamped);
  };

  return (
    <div
      className="ellipsis"
      seeMoreLessText={useEllipsis ? t('seeMore') : t('seeLess')}
      onClick={() => { setUseEllipsis((useEllipsis) => (!useEllipsis)); }}
      onKeyPress={() => { setUseEllipsis((useEllipsis) => (!useEllipsis)); }}
      role="button"
      tabIndex={0}
    >
      {text && useEllipsis ? (
        <ResponsiveReactLinesEllipsis
          text={text}
          maxLine={maxLine}
          ellipsis="…"
          onReflow={handleReflow}
          className={`${className} ellipsis-content`}
          {...props}
        />
      ) : (
        <p
          className={`${className} ellipsis-content`}
          {...props}
        >
          {text}
        </p>
      )}
    </div>
  );
};

export default ResponsiveEllipsis;
