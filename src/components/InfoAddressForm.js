import React, { useState } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { geocodeByAddress, getLatLng } from 'react-places-autocomplete';
import BlockMap from 'components/BlockMap';
import BlockListAddress from 'components/BlockListAddress';
import InfoAddressFormLimit from 'components/InfoAddressFormLimit';
import 'assets/style/modal-payment.scss';

const InfoAddressForm = ({ confirmInfo }) => {
  const [lat, setLat] = useState(16.066708);
  const [lng, setLng] = useState(108.21312);
  const [address, setAddress] = useState('');
  const [loadMap, setLoadMap] = useState(false);
  const [accessConfirm, setAccessConfirm] = useState(false);

  const handleChangeAddress = (address) => {
    setAddress(address);
  };

  const handleSelectAddress = async (address) => {
    setAddress(address);
    setLoadMap(true);
    await geocodeByAddress(address)
      .then((results) => getLatLng(results[0]))
      .then((latLng) => {
        setLat(latLng.lat);
        setLng(latLng.lng);
      });
    setLoadMap(false);
  };

  const onHandleAccessConfirm = () => {
    setAccessConfirm(true);
  };

  const onHandleRemoveInput = () => {
    setAddress('');
    setLat(16.066708);
    setLng(108.21312);
  }

  return (
    <div className="form-info-address">
      <div className="modal-add-payment">
        <div className="form-content">
          <span className="modal-add-payment__text-holder-address">
            Recipient&apos;s name
          </span>
          <InfoAddressFormLimit
            address={address}
            handleSelectAddress={handleSelectAddress}
            handleChangeAddress={handleChangeAddress}
            onHandleRemoveInput={onHandleRemoveInput}
          />
          <Row>
            <Col>
              <div className="block-info-map">
                <BlockMap lat={lat} lng={lng} loadMap={loadMap} />
                <BlockListAddress
                  onHandleAccessConfirm={onHandleAccessConfirm}
                />
                <button
                  type="button"
                  className="btn parcel-delivery-modal__btn-confirm"
                  disabled={!accessConfirm}
                  onClick={confirmInfo}
                >
                  <span>Confirm</span>
                </button>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
};

export default InfoAddressForm;
