import React, {
  useState,
} from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

import CheckoutRadioButton from 'components/CheckoutRadioButton';
import Button from 'components/Button';
import PaymentCardForm from 'components/PaymentCardForm';

import { ReactComponent as IconPaymentMethodCard } from 'assets/images/icon-payment-method-card.svg';
import { ReactComponent as IconPaymentMethodCB } from 'assets/images/icon-payment-method-cb.svg';
import { ReactComponent as IconPaymentMethodVisa } from 'assets/images/icon-payment-method-visa.svg';
import { ReactComponent as IconPaymentMethodMastercard } from 'assets/images/icon-payment-method-mastercard.svg';
// import { ReactComponent as IconPaymentMethod3X } from 'assets/images/icon-payment-3x.svg';
// import { ReactComponent as IconPaymentMethod4X } from 'assets/images/icon-payment-4x.svg';
import IconInfo from 'assets/svg/IconInfo';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  tokenizeCard,
  deleteCard,
} from 'logic/actions/paymentCardsActions';
import {
  doPreauth,
  getTransaction,
  cancelTransaction,
} from 'logic/actions/transactionsActions';
import {
  createTransactionFromTemplate,
} from 'logic/actions/transactionTemplatesActions';

const NEW = 'new';

const CheckoutPaymentForm = ({
  transactionTemplate: {
    id,
    currency = 'EUR',
    clientTheme: {
      primary,
    } = {},
  } = {},
  transaction: {
    subTotal,
  } = {},
  paymentCards,
  tokenizeCard,
  deleteCard,
  createTransactionFromTemplate,
  doPreauth,
  getTransaction,
  cancelTransaction,
  showPaymentErrorModal,
}) => {
  const { t } = useTranslation(['checkoutPaymentForm', 'paymentCardForm']);
  const [paymentMethodId, setPaymentMethodId] = useState(paymentCards.length ? null : NEW);
  const [isSubmitting, setSubmitting] = useState(false);
  const history = useHistory();

  const getCardProviderLogo = (cardProvider) => {
    switch (cardProvider) {
      case 'VISA':
        return IconPaymentMethodVisa;

      case 'MASTERCARD':
        return IconPaymentMethodMastercard;

      case 'CB':
        return IconPaymentMethodCB;

      default:
        return (<div />);
    }
  };

  const rollBack = (cardId, deleteCardIfError, resetForm, transactionId = null) => {
    if (deleteCardIfError) {
      deleteCard(cardId);
    }

    if (transactionId !== null) {
      cancelTransaction(transactionId);
    }

    resetForm();
    setSubmitting(false);
    showPaymentErrorModal();
  };

  const createTransactionAndDoPreauth = (cardId, deleteCardIfError, resetForm = () => {}) => {
    createTransactionFromTemplate(id, { subTotal })
      .then(({ payload: { data: { id: transactionId } = {} } = {} }) => {
        doPreauth(transactionId, cardId)
          .then(({
            payload: {
              data: {
                status,
                secureModeIsNeeded,
                secureModeRedirectUrl,
              } = {},
            } = {},
          }) => {
            switch (status) {
              case 'CREATED':
                if (secureModeIsNeeded) {
                  window.location.assign(secureModeRedirectUrl);
                } else {
                  rollBack(cardId, deleteCardIfError, resetForm, transactionId);
                }
                break;
              case 'SUCCEEDED':
                getTransaction(transactionId)
                  .then(({ payload: { data: { isPreauthorized } = {} } = {} }) => {
                    if (isPreauthorized) {
                      history.push(`/transactions/${transactionId}/post-process-preauth`);
                    } else {
                      rollBack(cardId, deleteCardIfError, resetForm, transactionId);
                    }
                  });
                break;
              default:
                rollBack(cardId, deleteCardIfError, resetForm, transactionId);
            }
          })
          .catch(() => {
            rollBack(cardId, deleteCardIfError, resetForm, transactionId);
          });
      })
      .catch(() => {
        rollBack(cardId, deleteCardIfError, resetForm);
      });
  };

  return (
    <div>
      <div>
        {paymentCards.map(({
          id,
          obfuscatedNumber,
          expirationDate,
          cardProvider,
        }) => (
          <CheckoutRadioButton
            id="method-payment"
            name="method-payment"
            key={id}
            Icon={getCardProviderLogo(cardProvider)}
            title={obfuscatedNumber.replaceAll(/.*([0-9]{4})$/g, '•••• $1')}
            subTitle={t('expirationDate', { expirationDate: expirationDate.replaceAll(/(.{2})(.{2})/g, '$1/$2') })}
            defaultChecked={paymentMethodId === id}
            onChange={() => { setPaymentMethodId(id); }}
            color={primary}
            className="radio-payment-method"
          />
        ))}
        <CheckoutRadioButton
          id="method-payment"
          name="method-payment"
          Icon={IconPaymentMethodCard}
          title={paymentCards.length ? t('newCard') : t('card')}
          defaultChecked={paymentMethodId === NEW}
          onChange={() => { setPaymentMethodId(NEW); }}
          color={primary}
          className="radio-payment-method"
        />
      </div>

      <div className="modal-add-payment">
        {paymentMethodId === NEW ? (
          <div className="form-content">
            <PaymentCardForm
              isCheckoutPage
              primaryColor={primary}
              onSubmit={({
                cardNumber,
                cardExpirationDate,
                cardCvx,
              }, { resetForm }) => {
                tokenizeCard({
                  cardNumber,
                  cardExpirationDate,
                  cardCvx,
                  currency,
                })
                  .then(({ id: cardId }) => {
                    createTransactionAndDoPreauth(cardId, true, resetForm);
                  })
                  .catch(() => {
                    showPaymentErrorModal();
                    resetForm();
                  });
              }}
            />
          </div>
        ) : (
          <div>
            <div className="mt-3 d-flex align-items-start">
              <p className="mb-0 pl-2">
                <IconInfo className="mr-2 flex-shrink-0" width="14px" color={primary} />
              </p>
              <p className="mb-0 pl-2 keep-line-breaks">
                <small>{t('paymentCardForm:checkoutDisclaimer')}</small>
              </p>
            </div>

            <div>
              <Button
                className="w-100 mt-2 btn-block checkout__btn client-primary-background-color"
                type="submit"
                isLoading={isSubmitting}
                disabled={!paymentMethodId}
                onClick={() => {
                  setSubmitting(true);
                  createTransactionAndDoPreauth(paymentMethodId, false);
                }}
              >
                {t('paymentCardForm:checkoutSubmit')}
              </Button>
            </div>
          </div>
        )}
      </div>

      {/*
        <div className="mt-3">
          <CheckoutRadioButton
            id="method-payment-other1"
            name="method-payment-other"
            Icon={IconPaymentMethod3X}
            title={t('3xcb')}
          />

          <CheckoutRadioButton
            id="method-payment-other2"
            name="method-payment-other"
            Icon={IconPaymentMethod4X}
            title={t('4xcb')}
          />
        </div>
      */}
    </div>
  );
};

const mapStateToProps = (state) => ({
  paymentCards: state.persistent.paymentCardsReducer.paymentCards,
});

const mapDispatchToProps = (dispatch) => ({
  tokenizeCard: bindActionCreators(tokenizeCard, dispatch),
  deleteCard: bindActionCreators(deleteCard, dispatch),
  doPreauth: bindActionCreators(doPreauth, dispatch),
  getTransaction: bindActionCreators(getTransaction, dispatch),
  cancelTransaction: bindActionCreators(cancelTransaction, dispatch),
  createTransactionFromTemplate: bindActionCreators(createTransactionFromTemplate, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CheckoutPaymentForm);
