import React from 'react';
import { Formik } from 'formik';
import * as yup from 'yup';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'components/Button';
import { useTranslation, Trans } from 'react-i18next';
import 'assets/style/modal-payment.scss';
import IconInfo from 'assets/svg/IconInfo';

const PaymentCardForm = ({
  isCheckoutPage,
  onSubmit,
  primaryColor = '#004bb4',
}) => {
  const { t } = useTranslation('paymentCardForm');

  return (
    <Formik
      validateOnChange={false}
      validateOnBlur={false}
      validationSchema={yup.object({
        cardNumber: yup.string().required(t('emptyCardNumber')).matches(/^([0-9]{4} ?){4}$/, t('invalidCardNumber')),
        cardExpirationDate: yup.string().required(t('emptyCardExpirationDate')).matches(/^[0-9]{2}\/[0-9]{2}$/, t('invalidCardExpirationDate')),
        cardCvx: yup.string().required(t('emptyCardCvx')).matches(/^[0-9]{3}$/, t('invalidCardCvx')),
      })}
      onSubmit={({
        cardNumber,
        cardExpirationDate,
        ...rest
      }, formikBag) => {
        onSubmit({
          cardNumber: cardNumber.replaceAll(' ', ''),
          cardExpirationDate: cardExpirationDate.replaceAll('/', ''),
          ...rest,
        }, formikBag);
      }}
      initialValues={{
        cardNumber: '',
        cardExpirationDate: '',
        cardCvx: '',
      }}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        touched,
        errors,
        isSubmitting,
      }) => (
        <Form noValidate onSubmit={handleSubmit}>
          <span className="modal-add-payment__text-holder-address">
            {t('title')}
          </span>
          <Row>
            <Col className="form-group-border-top">
              <Form.Group controlId="cardNumber">
                <Form.Control
                  type="text"
                  maxLength={19}
                  name="cardNumber"
                  value={values.cardNumber}
                  onChange={(e) => {
                    e.target.value = e.target.value.replaceAll(/([0-9]{4}) *(?=[0-9])/g, '$1 ');
                    handleChange(e);
                  }}
                  onBlur={handleBlur}
                  isInvalid={touched.cardNumber && !!errors.cardNumber}
                  placeholder="1234 1234 1234 1234"
                />
                <Form.Control.Feedback type="invalid">
                  {errors.cardNumber}
                </Form.Control.Feedback>
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col className="form-group-border-bottom-left">
              <Form.Group controlId="cardExpirationDate">
                <Form.Control
                  type="text"
                  maxLength={5}
                  name="cardExpirationDate"
                  value={values.cardExpirationDate}
                  onChange={(e) => {
                    e.target.value = e.target.value.replaceAll(/([0-9]{2})\/?(?=[0-9])/g, '$1/');
                    handleChange(e);
                  }}
                  onBlur={handleBlur}
                  isInvalid={touched.cardExpirationDate && !!errors.cardExpirationDate}
                  placeholder="MM/AA"
                />
                <Form.Control.Feedback type="invalid">
                  {errors.cardExpirationDate}
                </Form.Control.Feedback>
              </Form.Group>
            </Col>
            <Col className="form-group-border-bottom-right">
              <Form.Group controlId="cardCvx">
                <Form.Control
                  type="text"
                  maxLength={3}
                  name="cardCvx"
                  value={values.cardCvx}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isInvalid={touched.cardCvx && !!errors.cardCvx}
                  placeholder="CVC"
                />
                <Form.Control.Feedback type="invalid">
                  {errors.cardCvx}
                </Form.Control.Feedback>
              </Form.Group>
            </Col>
          </Row>
          {isCheckoutPage ? (
            <div>
              <div className="d-flex align-items-start mt-3">
                <p className="mb-0 pl-2">
                  <IconInfo className="mr-2" width="14px" color={primaryColor} />
                  <small><Trans t={t} i18nKey="checkoutDisclaimer" /></small>
                </p>
              </div>

              <div>
                <Button
                  className="w-100 mt-2 btn-block checkout__btn client-primary-background-color"
                  type="submit"
                  disabled={!values.cardNumber || !values.cardExpirationDate || !values.cardCvx}
                  isLoading={isSubmitting}
                >
                  {t('checkoutSubmit')}
                </Button>
              </div>
            </div>
          ) : (
            <Row>
              <Col className="form-group-border-bottom">
                <Button
                  variant="primary"
                  type="submit"
                  disabled={!values.cardNumber || !values.cardExpirationDate || !values.cardCvx}
                  className="modal-add-payment__button-submit"
                  isLoading={isSubmitting}
                >
                  {t('submit')}
                </Button>
              </Col>
            </Row>
          )}
        </Form>
      )}
    </Formik>
  );
};

export default PaymentCardForm;
