import React, {
  useState,
  useCallback,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation, Trans } from 'react-i18next';
import { useIntl } from 'react-intl';
import { toast } from 'react-toastify';
import useWindowSize from 'helpers/useWindowSize';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Modal from 'react-bootstrap/Modal';
import Button from 'components/Button';
import CompleteTransactionModal from 'components/CompleteTransactionModal';
import EvaluationFormModal from 'components/EvaluationFormModal';
import ResponsiveEllipsis from 'components/ResponsiveEllipsis';
import {
  acceptTransaction,
  rejectTransaction,
  completeTransaction,
  rejectCancelTransaction,
  cancelTransaction,
  evaluateTransaction,
} from 'logic/actions/transactionsActions';

const BUYER = 'buyer';
const SELLER = 'seller';

const EVENT_TRANSACTION_CANCEL_REQUESTED = 'event_transaction_cancel_requested';

const TransactionActionPanel = ({
  transaction: {
    id,
    title,
    isPreauthorized,
    isAccepted,
    isRejected,
    isCanceledByAdmin,
    isCanceledByBuyer,
    isCanceledBySeller,
    isCanceled,
    isCompleted,
    isExpired,
    canBeCanceled,
    buyerId,
    sellerReview,
    buyerReview,
    buyerDisplayName,
    sellerTotal,
    buyerTotal,
    client,
    currency = 'EUR',
    templatePermalink,
    lastEventSlug,
    verificationCode,
  } = {},
}) => {
  const { t, i18n } = useTranslation('transactionActionPanel');
  const intl = useIntl();
  const windowSize = useWindowSize();

  const me = useSelector((state) => (state.persistent.meReducer.me));
  const dispatch = useDispatch();
  const doAcceptTransaction = useCallback(() => dispatch(acceptTransaction(id)), [dispatch, id]);
  const doRejectTransaction = useCallback(() => dispatch(rejectTransaction(id)), [dispatch, id]);
  const doCompleteTransaction = useCallback((verificationCode = null) => dispatch(completeTransaction(id, verificationCode)), [dispatch, id]);
  const doRejectCancelTransaction = useCallback(() => dispatch(rejectCancelTransaction(id)), [dispatch, id]);
  const doCancelTransaction = useCallback(() => dispatch(cancelTransaction(id)), [dispatch, id]);
  const doEvaluateTransaction = useCallback((values) => dispatch(evaluateTransaction(id, values)), [dispatch, id]);
  const errorToast = useCallback(() => toast.error(t('anErrorOccured')), [t]);

  const [btn1Submitting, setBtn1Submitting] = useState(false);
  const [btn2Submitting, setBtn2Submitting] = useState(false);
  const [isCanceling, setCanceling] = useState(false);
  const [showConfirmCancelModal, setShowConfirmCancelModal] = useState(false);
  const [isRejecting, setRejecting] = useState(false);
  const [showConfirmRejectModal, setShowConfirmRejectModal] = useState(false);
  const [showEvaluationForm, setShowEvaluationForm] = useState(false);
  const [showCompleteModal, setShowCompleteModal] = useState(false);

  const isPurchase = me.id === buyerId;
  const role = isPurchase ? BUYER : SELLER;
  const needReview = (isPurchase && !buyerReview) || (!isPurchase && !sellerReview);
  const showVerificationCode = isPurchase && isAccepted && !isCanceled && !isCompleted;

  let status = null;
  let btn1Action = null;
  let btn1ClassName = 'btn1';
  let btn1Variant = 'primary';
  let btn2Action = null;

  if (isPreauthorized) {
    status = 'preauthorized';

    if (role === SELLER) {
      btn1Action = doAcceptTransaction;
      btn2Action = () => {
        setShowConfirmRejectModal(true);
        return Promise.resolve();
      };
    }
  }

  if (isAccepted) {
    status = 'accepted';

    if (role === SELLER) {
      btn1Action = () => {
        setShowCompleteModal(true);
        return Promise.resolve();
      };
    }
  }

  if (isCompleted) {
    if (needReview) {
      status = 'needReview';

      btn1Action = () => {
        setShowEvaluationForm(true);
        return Promise.resolve();
      };
    } else {
      status = 'completed';
    }
  }

  if (isRejected) {
    status = 'rejected';
  }

  if (isExpired) {
    status = 'expired';
  }

  if (isCanceledByBuyer) {
    if (!isAccepted) {
      status = 'canceledByBuyerBeforePayment';
    } else {
      status = 'canceledByBuyerAfterPayment';
    }
  }

  if (isCanceledBySeller) {
    if (!isAccepted) {
      status = 'canceledBySellerBeforePayment';
    } else {
      status = 'canceledBySellerAfterPayment';
    }
  }

  if (isCanceledByAdmin) {
    if (!isAccepted) {
      status = 'canceledByAdminBeforePayment';
    } else {
      status = 'canceledByAdminAfterPayment';
    }
  }

  if (lastEventSlug === EVENT_TRANSACTION_CANCEL_REQUESTED) {
    status = 'cancelRequested';

    btn1Action = () => {
      setShowConfirmCancelModal(true);
      return Promise.resolve();
    };
    btn1ClassName = 'cancelBtn';
    btn1Variant = 'danger';

    btn2Action = doRejectCancelTransaction;
  }

  if (status === null) {
    return null;
  }

  const translationKey = `status.${role}.${status}`;

  const showBtn1 = i18n.exists(`transactionActionPanel:${translationKey}.btn1`);
  const showBtn2 = i18n.exists(`transactionActionPanel:${translationKey}.btn2`);
  const showSupportBtn = i18n.exists(`transactionActionPanel:${translationKey}.supportBtn`);
  const showNewPropositionBtn = i18n.exists(`transactionActionPanel:${translationKey}.newPropositionBtn`);

  // Cancel is only visible by seller, or buyer if transaction has not been accepted
  const showCancelBtn = !(isPurchase && isAccepted) && i18n.exists(`transactionActionPanel:${translationKey}.cancelBtn`);

  const showBtnCol = showBtn1 || showBtn2 || showSupportBtn || showCancelBtn || showNewPropositionBtn;

  const confirmCancelModal = (
    <Modal
      show={showConfirmCancelModal}
      onHide={() => { setShowConfirmCancelModal(false); }}
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>{t('confirmCancelModal.title')}</Modal.Title>
      </Modal.Header>

      <Modal.Body>{t('confirmCancelModal.areYouSure')}</Modal.Body>

      <Modal.Footer>
        <Button variant="secondary" onClick={() => { setShowConfirmCancelModal(false); }}>
          {t('confirmCancelModal.close')}
        </Button>
        <Button
          variant="danger"
          isLoading={isCanceling}
          disabled={!canBeCanceled}
          onClick={() => {
            setCanceling(true);
            doCancelTransaction()
              .finally(() => {
                setShowConfirmCancelModal(false);
                setCanceling(false);
              });
          }}
        >
          {t('confirmCancelModal.confirm')}
        </Button>
      </Modal.Footer>
    </Modal>
  );

  const confirmRejectModal = (
    <Modal
      show={showConfirmRejectModal}
      onHide={() => { setShowConfirmRejectModal(false); }}
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>{t('confirmRejectModal.title')}</Modal.Title>
      </Modal.Header>

      <Modal.Body>{t('confirmRejectModal.areYouSure')}</Modal.Body>

      <Modal.Footer>
        <Button variant="secondary" onClick={() => { setShowConfirmRejectModal(false); }}>
          {t('confirmRejectModal.close')}
        </Button>
        <Button
          variant="danger"
          isLoading={isRejecting}
          onClick={() => {
            setRejecting(true);
            doRejectTransaction()
              .finally(() => {
                setShowConfirmRejectModal(false);
                setRejecting(false);
              });
          }}
        >
          {t('confirmRejectModal.confirm')}
        </Button>
      </Modal.Footer>
    </Modal>
  );

  const translationValues = {
    buyerDisplayName,
    title,
    client,
    sellerTotal: intl.formatNumber(sellerTotal / 100, { style: 'currency', currency }),
    buyerTotal: intl.formatNumber(buyerTotal / 100, { style: 'currency', currency }),
  };

  return (
    <div className="transaction__suggest p-3 border rounded">
      <CompleteTransactionModal
        visible={showCompleteModal}
        setVisible={setShowCompleteModal}
        onSubmit={({ verificationCode }, { resetForm }) => {
          doCompleteTransaction(verificationCode)
            .catch(() => {
              errorToast();
            })
            .finally(() => {
              resetForm(true);
              setShowCompleteModal(false);
            });
        }}
      />
      {confirmCancelModal}
      {confirmRejectModal}
      <EvaluationFormModal
        show={showEvaluationForm}
        onHide={() => { setShowEvaluationForm(false); }}
        onSubmit={(values, { resetForm }) => {
          doEvaluateTransaction(values)
            .finally(() => {
              setShowEvaluationForm(false);
              resetForm();
            });
        }}
        doEvaluateTransaction
      />
      <Row>
        <Col className="mb-3 mb-lg-0">
          <h5 className="mb-2 transaction__title-main">{t(`${translationKey}.title`)}</h5>
          <div className="m-0 transaction__description keep-line-breaks">
            {windowSize.isNarrow ? (
              <ResponsiveEllipsis text={t(`${translationKey}.content`, translationValues)} />
            ) : (
              <p>{t(`${translationKey}.content`, translationValues)}</p>
            )}
          </div>
          { showVerificationCode && (
            <Trans className="transaction__verification-code" t={t} i18nKey="verificationCode" values={{ verificationCode }}>
              Verification code:
              <strong />
            </Trans>
          )}
        </Col>
        {showBtnCol && (
          <Col lg={3}>
            <div className="transaction__button-wrap">
              {showBtn1 && (
                <Button
                  className={`${btn1ClassName} mb-2`}
                  isLoading={btn1Submitting}
                  variant={btn1Variant}
                  onClick={() => {
                    setBtn1Submitting(true);
                    btn1Action()
                      .catch(() => {
                        errorToast();
                      })
                      .finally(() => {
                        setBtn1Submitting(false);
                      });
                  }}
                >
                  {t(`${translationKey}.btn1`)}
                </Button>
              )}
              {showBtn2 && (
                <Button
                  variant="light"
                  className="btn2 mb-2"
                  isLoading={btn2Submitting}
                  onClick={() => {
                    setBtn2Submitting(true);
                    btn2Action()
                      .catch(() => {
                        errorToast();
                      })
                      .finally(() => {
                        setBtn2Submitting(false);
                      });
                  }}
                >
                  {t(`${translationKey}.btn2`)}
                </Button>
              )}
              {showNewPropositionBtn && (
                <Button
                  as="a"
                  variant="light"
                  className="supportBtn mb-2"
                  href={templatePermalink}
                >
                  {t(`${translationKey}.newPropositionBtn`)}
                </Button>
              )}
              {showSupportBtn && (
                <Button
                  as="a"
                  variant="light"
                  className="supportBtn mb-2"
                  href="mailto:support@tripartie.com"
                >
                  {t(`${translationKey}.supportBtn`)}
                </Button>
              )}
              {showCancelBtn && (
                <Button
                  variant="danger"
                  className="cancelBtn mb-2"
                  disabled={!canBeCanceled}
                  onClick={() => { setShowConfirmCancelModal(true); }}
                >
                  {t(`${translationKey}.cancelBtn`)}
                </Button>
              )}
            </div>
          </Col>
        )}
      </Row>
    </div>
  );
};

export default TransactionActionPanel;
