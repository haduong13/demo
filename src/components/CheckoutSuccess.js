import React from 'react';
import { useTranslation } from 'react-i18next';
import useWindowSize from 'helpers/useWindowSize';
import { Link } from 'react-router-dom';

import Button from 'components/Button';
import IconFly from 'assets/images/icon-fly.svg';

const CheckoutSuccess = ({
  adUrl,
  client,
}) => {
  const { t } = useTranslation('checkoutSuccess');
  return (
    <div className="text-center">
      <div className="mt-5">
        <img className="ml-auto mr-auto" src={IconFly} alt="" width="160px" />
      </div>

      <h4 className="m-0 mt-3 mb-3 checkout__title">{t('title')}</h4>
      <p className="mb-3">{t('subTitle')}</p>
      <Button
        type="button"
        className={`btn-block ml-auto mr-auto checkout__btn client-primary-background-color ${
          useWindowSize().isNarrow ? 'w-100' : 'w-75'
        }`}
        onClick={() => { window.location = adUrl; }}
      >
        {t('returnToAd', { client })}
      </Button>
      <Link
        to="/"
        className="btn-block checkout__link mt-3 w-100 client-primary-color"
      >
        {t('goToMyTripartieAccount')}
      </Link>
    </div>
  );
};

export default CheckoutSuccess;
