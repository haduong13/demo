import React from 'react';
import { Formik } from 'formik';
import * as yup from 'yup';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Button from 'components/Button';
import { useTranslation } from 'react-i18next';
import ReactStars from 'react-rating-stars-component';
import iconClose from 'assets/images/icon-close.svg';

const EvaluationFormModal = ({
  show,
  onHide,
  onSubmit,
}) => {
  const { t } = useTranslation('evaluationForm');

  return (
    <Formik
      validateOnChange={false}
      validateOnBlur={false}
      validationSchema={yup.object({
        score: yup.number().min(1, t('invalidScore')).max(5, t('invalidScore')),
        comment: yup.string(),
      })}
      onSubmit={onSubmit}
      initialValues={{
        score: 0,
        comment: '',
      }}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        touched,
        errors,
        isSubmitting,
        setFieldValue,
      }) => (
        <Modal
          show={show}
          onHide={onHide}
          centered
          className="modal"
        >
          <Form noValidate onSubmit={handleSubmit}>
            <Modal.Header>
              <p className="modal__title">{t('title')}</p>
              <div
                onClick={onHide}
                onKeyPress={onHide}
                className="modal__icon-close"
                role="button"
                tabIndex={0}
              >
                <img
                  src={iconClose}
                  alt=""
                />
              </div>
            </Modal.Header>

            <Modal.Body>
              <Form.Group md="4" controlId="score">
                <Form.Label>{t('overall')}</Form.Label>
                <ReactStars
                  count={5}
                  onChange={(score) => { setFieldValue('score', score); }}
                  size={32}
                  activeColor="#ffd700"
                />
                <Form.Control.Feedback type="invalid">
                  {errors.score}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group md="4" controlId="comment">
                <Form.Label>{t('comment')}</Form.Label>
                <Form.Control
                  type="text"
                  as="textarea"
                  name="comment"
                  value={values.comment}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isInvalid={touched.comment && !!errors.comment}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.comment}
                </Form.Control.Feedback>
              </Form.Group>
            </Modal.Body>

            <Modal.Footer>
              <Button
                className="mb-2"
                type="submit"
                disabled={!values.score}
                isLoading={isSubmitting}
              >
                {t('submit')}
              </Button>
            </Modal.Footer>
          </Form>
        </Modal>
      )}
    </Formik>
  );
};

export default EvaluationFormModal;
