import React from 'react';
import Modal from 'react-bootstrap/Modal';
import { useTranslation } from 'react-i18next';
import PaymentCardForm from 'components/PaymentCardForm';
import iconClose from 'assets/images/icon-close.svg';
import 'assets/style/modal-payment.scss';

const AddPaymentCardModal = ({
  show,
  onHide,
}) => {
  const { t } = useTranslation('addPaymentCardModal');
  return (
    <Modal
      show={show}
      onHide={onHide}
      centered
      className="modal-add-payment"
    >
      <Modal.Header>
        <p className="modal-add-payment__title">
          {t('title')}
        </p>
        <div
          onClick={onHide}
          onKeyPress={onHide}
          className="modal-add-payment__icon-close"
          role="button"
          tabIndex={0}
        >
          <img
            className="modal-add-payment__icon-close"
            src={iconClose}
            alt=""
          />
        </div>
      </Modal.Header>

      <Modal.Body>
        <div className="form-content">
          <PaymentCardForm />
        </div>
      </Modal.Body>
    </Modal>
  );
};

export default AddPaymentCardModal;
