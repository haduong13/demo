import React from 'react';
import styled from 'styled-components';

const StyledRadioButton = styled.input`
  &:checked {
    background-color: ${(props) => props.color || '#004bb4'} !important;
  }
`;

export default (props) => (<StyledRadioButton type="radio" {...props} />); // eslint-disable-line import/no-anonymous-default-export
