import React from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import { useTranslation } from 'react-i18next';
import { FormattedNumber } from 'react-intl';

import IconInfo from 'assets/svg/IconInfo';

const CheckoutRecapDesktop = ({
  transactionTemplate: {
    title,
    description,
    client,
    subTotal = 0,
    currency = 'EUR',
    clientTheme: {
      primary,
    } = {},
  } = {},
  transaction: {
    subTotal: newPrice = 0,
    buyerFees = 0,
    buyerTotal = 0,
  } = {},
  showCounterOfferModal,
  noCounterOffer,
}) => {
  const { t } = useTranslation('checkoutRecap');
  const hasCounterOffer = newPrice !== subTotal;

  return (
    <div className="checkout-detail">
      <Row className="mt-5 pb-3">
        <Col>
          <div className="checkout-detail-image">
            <div className="checkout-detail-image__inner" />
          </div>
        </Col>
        <Col>
          <h5 className="mb-2 checkout__title checkout__title--small">
            {title}
          </h5>
          <p className="m-0 checkout-detail__desc">
            {description}
          </p>
        </Col>
      </Row>
      <div className="border-top pt-3 pb-3 checkout-detail__info">
        <div className="d-flex justify-content-between mb-0">
          <p className="m-0">{t('subTotal')}</p>
          <p className="m-0 text-right">
            {hasCounterOffer ? (
              <>
                <del><FormattedNumber value={subTotal / 100} style="currency" currency={currency} /></del>
                {' '}
                <FormattedNumber value={newPrice / 100} style="currency" currency={currency} />
              </>
            ) : (
              <FormattedNumber value={subTotal / 100} style="currency" currency={currency} />
            )}
          </p>
        </div>
        {!noCounterOffer && (
          <div
            onClick={showCounterOfferModal}
            onKeyPress={showCounterOfferModal}
            role="button"
            tabIndex={0}
          >
            <p className="m-0 checkout-detail__other">
              <small className="m-0 client-primary-color">
                {t('counterPrice')}
              </small>
            </p>
          </div>
        )}
        <div className="d-flex justify-content-between mt-2">
          <p className="m-0">{t('fees')}</p>
          <OverlayTrigger
            placement="right"
            overlay={
              <Tooltip>{t('feesTooltip', { client })}</Tooltip>
            }
          >
            <IconInfo className="ml-2 mr-auto" width="14px" color={primary} />
          </OverlayTrigger>
          <p className="m-0 text-right"><FormattedNumber value={buyerFees / 100} style="currency" currency={currency} /></p>
        </div>
      </div>

      {/* <div className="border-top pt-3 pb-3 checkout-detail__info">
        <div className="d-flex mb-0">
          <IconAdd className="ml-2" width="16px" color={primary} />
          <p className="m-0 text-right">{t('promocode')}</p>
        </div>
      </div> */}

      <div className="border-top pt-3 pb-3 checkout-detail__info">
        <div className="d-flex justify-content-between mb-0">
          <p className="m-0">{t('total')}</p>
          <p className="m-0 text-right checkout__title-total"><FormattedNumber value={buyerTotal / 100} style="currency" currency={currency} /></p>
        </div>
      </div>
    </div>
  );
};

export default CheckoutRecapDesktop;
