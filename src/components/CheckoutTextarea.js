import React from 'react';

const CheckoutTextarea = (props) => {
  const { onChange, className, ...input } = props;
  return (
    <div className={className}>
      {input.label && <label className="input-label">{input.label}</label>}
      <textarea {...input} onChange={onChange} />
    </div>
  );
};

export default CheckoutTextarea;
