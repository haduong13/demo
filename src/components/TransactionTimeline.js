import React from 'react';
import { useTranslation } from 'react-i18next';
import { FormattedDate } from 'react-intl';
import iconChecked from 'assets/images/icon-checked.svg';
import iconNotChecked from 'assets/images/icon-not-checked.svg';

const defaultSteps = [
  { slug: 'event_transaction_sent' },
  { slug: 'event_transaction_accept' },
  { slug: 'event_transaction_pay' },
  { slug: 'event_transaction_complete' },
];

const TransactionTimeline = ({
  transaction: {
    isRejected,
    isExpired,
    isCanceled,
    events = [],
  } = {},
}) => {
  const { t } = useTranslation('transactionTimeline');
  const badlyEnded = isRejected || isExpired || isCanceled;
  let eventsToDisplay = [...events];

  if (badlyEnded && eventsToDisplay.length < 2) {
    return null;
  }

  if (!badlyEnded && eventsToDisplay.length < 4) {
    eventsToDisplay = eventsToDisplay.concat(defaultSteps.slice(eventsToDisplay.length));
  }

  return (
    <div className="transaction-reference-progress d-flex justify-content-between mt-3">
      {eventsToDisplay.map(({ slug, date }) => (
        <div key={slug}>
          <img width="22px" src={date ? iconChecked : iconNotChecked} alt="" />
          <p className="mb-0 transaction-reference-progress__title">{t(slug)}</p>
          {date && (<p className="mb-0 text-secondary"><FormattedDate value={new Date(date)} /></p>)}
        </div>
      ))}
    </div>
  );
};

export default TransactionTimeline;
