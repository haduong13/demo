import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Alert from 'react-bootstrap/Alert';
import useWindowSize from 'helpers/useWindowSize';
import IconDelivery1 from 'assets/svg/IconDelivery1';
import IconDelivery2 from 'assets/svg/IconDelivery2';
import IconDeliveryMobile1 from 'assets/svg/IconDeliveryMobile1';
import IconDeliveryMobile2 from 'assets/svg/IconDeliveryMobile2';
import methodOne from 'assets/images/method1.svg';
import Button from 'components/Button';
import CheckoutRadioButton from 'components/CheckoutRadioButton';
import ParcelDeliveryModal from 'components/ParcelDeliveryModal';

const CheckoutDeliveryMethod = ({
  // deliveryMethod,
  setDeliveryMethod,
  setActiveKey,
  transactionTemplate: { clientTheme: { primary } = {} } = {},
  showBlockChoiceMethodRespon,
  blockStepMethod,
  checkParcelDelivery,
  onHandleCheckParcelDelivery,
  onHandleOver,
}) => {
  const { t } = useTranslation('checkoutDeliveryMethod');

  const [openParcelDelivery, setOpenParcelDelivery] = useState(false);

  const confirmInfo = () => {
    onHandleCheckParcelDelivery();
    setOpenParcelDelivery(!openParcelDelivery);
  };

  const onHandleOpenMethodDelivery = () => {
    setOpenParcelDelivery(true);
    if (window.innerWidth <= 768) {
      showBlockChoiceMethodRespon();
    }
  };

  return (
    <div className="delivery-method">
      <ParcelDeliveryModal
        show={window.innerWidth > 768 && openParcelDelivery}
        onHide={() => {
          setOpenParcelDelivery(false);
        }}
        confirmInfo={confirmInfo}
      />
      <CheckoutRadioButton
        id="method-delivery1"
        name="method-delivery"
        Icon={useWindowSize().isNarrow ? IconDeliveryMobile1 : IconDelivery1}
        alt=""
        color={primary}
        title={t('handover')}
        checked={!checkParcelDelivery}
        onChange={() => {
          onHandleOver();
        }}
      />
      <div className={`${checkParcelDelivery || blockStepMethod === 3 ? 'parcel-delivery' : ''}`}>
        <CheckoutRadioButton
          id="method-delivery2"
          name="method-delivery"
          Icon={useWindowSize().isNarrow ? IconDeliveryMobile2 : IconDelivery2}
          alt=""
          color={primary}
          title={t('parcelDelivery')}
          onChange={onHandleOpenMethodDelivery}
          checked={checkParcelDelivery || blockStepMethod === 3}
        />
        <div className="parcel-delivery__info-more">
          <img src={methodOne} alt="" />
          <div className="content-left">
            <p>
              Mondial Relay
              {' '}
              <span className="edit">Edit</span>
              {' '}
              <span className="price">€10.00</span>
            </p>
            <p className="description">
              Available at the pick-up point within 4 to 7 days
            </p>
            <p className="address">
              <span className="bold">Recipient’s name:</span>
              Jane Doe
            </p>
            <p className="address">
              <span className="bold">Pick-up point:</span>
              Cerise & Potiron 5,
              parvis Alan Turing, 75013 Paris, France
            </p>
          </div>
        </div>
      </div>
      <Alert>{t('handoverNotice')}</Alert>
      <div>
        <Button
          className="w-100 btn-block checkout__btn client-primary-background-color"
          onClick={() => {
            setDeliveryMethod('handover');
            setActiveKey('note');
          }}
        >
          {t('title')}
        </Button>
      </div>
    </div>
  );
};

export default CheckoutDeliveryMethod;
