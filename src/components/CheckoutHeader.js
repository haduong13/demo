import React from 'react';
import LogoTripartie from 'assets/svg/LogoTripartie';
import { useTranslation } from 'react-i18next';

const CheckoutHeader = () => {
  const { t } = useTranslation('checkoutHeader');

  return (
    <div className="checkout-container__header p-3 fixed-top d-flex justify-content-center align-items-center client-primary-background-color">
      <div>
        <LogoTripartie width="111px" />
      </div>
      <p className="m-0 ml-lg-3 d-none d-lg-block">{t('title')}</p>
    </div>
  );
};

export default CheckoutHeader;
