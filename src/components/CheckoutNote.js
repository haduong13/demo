import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Button from './Button';
import CheckoutTextarea from './CheckoutTextarea';

const CheckoutNote = ({
  setActiveKey,
}) => {
  const { t } = useTranslation('checkout');

  const [valueMessage, setValueMessage] = useState('');

  const onChange = (e) => {
    setValueMessage(e.target.value);
  };

  return (
    <div>
      <CheckoutTextarea
        className="checkout__text-area"
        label={t('checkoutNote.yourMessage')}
        placeholder={t('checkoutNote.placeholderMessage')}
        onChange={onChange}
      />
      <div>
        {valueMessage && (
          <Button
            className="w-100 btn-block checkout__btn client-primary-background-color"
            onClick={() => {
              setActiveKey('payment');
            }}
          >
            {t('checkoutNote.addNoteBtn')}
          </Button>
        )}

        <Button
          className="w-100 btn-block checkout__btn checkout__btn--next"
          onClick={() => {
            setActiveKey('payment');
          }}
        >
          {t('checkoutNote.addNoteDescription')}
        </Button>
      </div>
    </div>
  );
};

export default CheckoutNote;
