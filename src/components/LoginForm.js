import React from 'react';
import { Formik } from 'formik';
import * as yup from 'yup';
import Form from 'react-bootstrap/Form';
import Button from 'components/Button';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import 'assets/style/login-register.scss';

const LoginForm = ({
  onSubmit,
  // lastUsedLogin,
  isCheckoutPage,
  setShowSignup,
}) => {
  const { t } = useTranslation('loginForm');

  return (
    <Formik
      validateOnChange={false}
      validateOnBlur={false}
      validationSchema={yup.object({
        email: yup.string().required(t('emptyEmail')).email(t('invalidEmail')),
        password: yup.string().required(t('emptyPassword')),
      })}
      onSubmit={onSubmit}
      initialValues={{
        email: '',
        password: '',
      }}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        touched,
        errors,
        isSubmitting,
      }) => (
        <Form className="form-login" noValidate onSubmit={handleSubmit}>
          <Form.Row>
            <Form.Group
              md="4"
              controlId="email"
              className="form-group-border-top"
            >
              <Form.Control
                type="email"
                name="email"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                isInvalid={touched.email && !!errors.email}
                placeholder={t('email')}
              />
              <Form.Control.Feedback type="invalid">
                {errors.email}
              </Form.Control.Feedback>
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group
              md="4"
              controlId="password"
              className="form-group-border-bottom"
            >
              <Form.Control
                type="password"
                name="password"
                value={values.password}
                onChange={handleChange}
                onBlur={handleBlur}
                isInvalid={touched.password && !!errors.password}
                placeholder={t('password')}
              />
              <Form.Control.Feedback type="invalid">
                {errors.password}
              </Form.Control.Feedback>
            </Form.Group>
          </Form.Row>
          <Link
            to="/reset-password/request"
            className="text-forget-password"
          >
            {t('forgotPassword')}
          </Link>
          <Form.Row>
            <Button
              type="submit"
              disabled={!values.email || !values.password}
              className={`btn-block ${isCheckoutPage && 'checkout__btn border-0 client-primary-background-color'}`}
              isLoading={isSubmitting}
            >
              {t('submit')}
            </Button>
          </Form.Row>
          <div className="text-not-have-account">
            <span />
            <span className="text">{t('dontHaveAccount')}</span>
            <span />
          </div>
          <Form.Row>
            {isCheckoutPage ? (
              <Button
                className="border-0 create-account"
                onClick={() => setShowSignup(true)}
              >
                {t('signup')}
              </Button>
            ) : (
              <Link to="/signup" className="create-account">
                {t('signup')}
              </Link>
            )}
          </Form.Row>
        </Form>
      )}
    </Formik>
  );
};

export default LoginForm;
