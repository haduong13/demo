import React from 'react';
import iconCheck from 'assets/images/icon-check.svg';
import 'assets/style/modal-payment.scss';

const listAddressMockup = [
  {
    key: 1,
    id: 'A',
    address1: 'Cerise & Potiron',
    address2: '5 parvis Alan Turing, 75013 Paris, France',
  },
  {
    key: 2,
    id: 'B',
    address1: 'STATION F - Pick up',
    address2: '5 parvis Alan Turing, 75013 Paris, France',
  },
  {
    key: 3,
    id: 'C',
    address1: 'STATION F - Pick up',
    address2: '5 parvis Alan Turing, 75013 Paris, France',
  },
  {
    key: 4,
    id: 'D',
    address1: 'STATION F - Pick up',
    address2: '5 parvis Alan Turing, 75013 Paris, France',
  },
];

const BlockListAddress = ({ onHandleAccessConfirm }) => (
  <div className="list-address">
    {listAddressMockup?.map((item) => (
      <div key={item.key} className="list-address__item">
        <input
          type="radio"
          id={item.id}
          name="address-list"
          value=""
          onClick={onHandleAccessConfirm}
        />
        <img src={iconCheck} alt="" />
        <label htmlFor={item.id}>
          <div className="content">
            <span className="stt">{item.id}</span>
            <div className="text-left">
              <p className="title">Cerise & Potiron</p>
              <p>5 parvis Alan Turing, 75013 Paris, France</p>
              <div className="time">
                <span>Monday</span>
                <span>9am-6am</span>
              </div>
              <div className="time">
                <span>Tuesday</span>
                <span>9am-6am</span>
              </div>
              <div className="time">
                <span>Wednesday</span>
                <span>9am-6am</span>
              </div>
              <div className="time">
                <span>Thursday</span>
                <span>9am-6am</span>
              </div>
              <div className="time">
                <span>Friday</span>
                <span>9am-6am</span>
              </div>
              <div className="time">
                <span>Sunday</span>
                <span>9am-6am</span>
              </div>
            </div>
          </div>
        </label>
      </div>
    ))}
  </div>
);

export default BlockListAddress;
