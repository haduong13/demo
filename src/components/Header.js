import React, {
  useState,
  useEffect,
  useCallback,
} from 'react';
import { useTranslation } from 'react-i18next';
import { logout } from 'logic/actions/authActions';
import Dropdown from 'react-bootstrap/Dropdown';
import { Link } from 'react-router-dom';
import LanguageSelector from 'components/LanguageSelector';
import HeaderResponsive from 'components/HeaderResponsive';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { useLoadingScreen } from 'helpers/LoadingScreenContext';
import logo from 'assets/images/logo.svg';
import avatar from 'assets/images/avatar.png';
import downArrow from 'assets/images/down-arrow.svg';
import iconWallet from 'assets/images/icon-wallet.svg';
import iconSignOut from 'assets/images/icon-sign-out.svg';
import iconSetting from 'assets/images/icon-setting.svg';
import iconMenuOn from 'assets/images/icon-menu-on.svg';
import iconMenuOff from 'assets/images/icon-menu-off.svg';
import iconHelpCenter from 'assets/images/icon-help-center.svg';
import 'assets/style/header.scss';

const Header = ({
  me: {
    userProfile: {
      firstName,
      lastName,
    } = {},
  } = {},
  logout,
}) => {
  const { t } = useTranslation(['header', '_links']);
  const setLoadingScreen = useLoadingScreen();
  const [showMenu, setShowMenu] = useState(false);

  useEffect(() => {
    const menu = document.getElementById('menu-responsive');
    if (!menu) {
      return;
    }

    if (showMenu) {
      menu.style.display = 'block';
    } else {
      menu.style.display = 'none';
    }
  }, [showMenu]);

  const handleLogout = useCallback(() => {
    setLoadingScreen(true);
    logout()
      .finally(() => {
        setLoadingScreen(false);
      });
  }, [setLoadingScreen, logout]);

  return (
    <>
      <div className="header-main">
        <Link to="/">
          <img src={logo} alt="" />
        </Link>
        <div className="header-main__block-info">
          <div className="my-transactions">
            <Link to="/">
              <span className="my-transactions__text-info">
                {t('home')}
                {/* <span className="my-transactions__number">2</span> */}
              </span>
            </Link>
          </div>
          <div className="my-info">
            <Dropdown className="custom-profile-dropdown">
              <Dropdown.Toggle variant="none">
                <img className="img-avatar" src={avatar} alt="" />
                {`${firstName} ${lastName}`}
                <img className="icon-down" src={downArrow} alt="" />
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item href="/settings/wallets">
                  <span className="text-dropdown">
                    <img className="icon-dropdown" src={iconWallet} alt="" />
                    {t('wallet')}
                    {/* <span className="money">0,00 €</span> */}
                  </span>
                </Dropdown.Item>
                <Dropdown.Item href="/settings">
                  <span className="text-dropdown">
                    <img className="icon-dropdown" src={iconSetting} alt="" />
                    {t('settings')}
                  </span>
                </Dropdown.Item>
                <Dropdown.Item href={t('_links:supportCenter')} target="_blank" rel="noopener noreferrer">
                  <span className="text-dropdown">
                    <img className="icon-dropdown" src={iconHelpCenter} alt="" />
                    {t('helpCenter')}
                  </span>
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={handleLogout}
                  onKeyPress={handleLogout}
                >
                  <span className="text-dropdown">
                    <img className="icon-dropdown" src={iconSignOut} alt="" />
                    {t('logout')}
                  </span>
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
          <div className="flag-language">
            <LanguageSelector align="right" />
          </div>
        </div>
        <div className="header-main__block-icon-menu">
          {showMenu ? (
            <div
              onClick={() => { setShowMenu(false); }}
              onKeyPress={() => { setShowMenu(false); }}
              role="button"
              tabIndex={0}
            >
              <img
                className="header-main__icon-menu-off"
                src={iconMenuOff}
                alt=""
              />
            </div>
          ) : (
            <div
              onClick={() => { setShowMenu(true); }}
              onKeyPress={() => { setShowMenu(true); }}
              role="button"
              tabIndex={0}
            >
              <img
                className="header-main__icon-menu-on"
                src={iconMenuOn}
                alt=""
              />
            </div>
          )}
        </div>
      </div>
      <HeaderResponsive hideMenu={() => { setShowMenu(false); }} />
    </>
  );
};

const mapStateToProps = (state) => ({
  me: state.persistent.meReducer.me,
});

const mapDispatchToProps = (dispatch) => ({
  logout: bindActionCreators(logout, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Header);
