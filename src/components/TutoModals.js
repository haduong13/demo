import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import Button from 'components/Button';
import LanguageSelector from 'components/LanguageSelector';

import 'assets/style/tuto-modals.scss';

import TutoModalRadioChecked from 'assets/svg/TutoModalRadioChecked';
import TutoModalRadioUnchecked from 'assets/svg/TutoModalRadioUnchecked';

import tutoModalBuyer1 from 'assets/images/tuto_modal_buyer_1.svg';
import tutoModalBuyer2 from 'assets/images/tuto_modal_buyer_2.svg';
import tutoModalBuyer3 from 'assets/images/tuto_modal_buyer_3.svg';

const BUYER_IMAGES = [
  tutoModalBuyer1,
  tutoModalBuyer2,
  tutoModalBuyer3,
];

const IMAGES = {
  buyer: BUYER_IMAGES,
};

const TutoModals = ({
  show,
  onHide,
  primaryColor = '#004bb4',
  tuto,
}) => {
  const { t } = useTranslation('tutoModals');
  const [currentIndex, setCurrentIndex] = useState(0);

  const images = IMAGES[tuto] || null;
  const tBase = `${tuto}.${currentIndex}`;

  let largestTitle = '';
  let largestText = '';
  for (let i = 0; i < images.length; i++) {
    if (t(`${tuto}.${i}.title`).length > largestTitle.length) {
      largestTitle = t(`${tuto}.${i}.title`);
    }
    if (t(`${tuto}.${i}.text`).length > largestText.length) {
      largestText = t(`${tuto}.${i}.text`);
    }
  }

  if (!images) {
    return null;
  }

  return (
    <Modal
      show={show}
      backdrop="static"
      centered
      className="tuto-modal"
    >
      <Modal.Body>
        <div>
          <img src={images[currentIndex] || null} alt="" />
        </div>
        <div className="tuto-modal__body">
          <div>
            {images.map((path, index) => (
              index === currentIndex ? (
                <TutoModalRadioChecked key={path} onClick={() => { setCurrentIndex(index); }} className="tuto-modal__radio" color={primaryColor} />
              ) : (
                <TutoModalRadioUnchecked key={path} onClick={() => { setCurrentIndex(index); }} className="tuto-modal__radio" />
              )
            ))}
          </div>
          <p className="tuto-modal__title-placeholder">
            <p className="tuto-modal__title">{t(`${tBase}.title`)}</p>
            <p>{largestTitle}</p>
          </p>
          <p className="tuto-modal__text-placeholder">
            <p className="tuto-modal__text">{t(`${tBase}.text`)}</p>
            <p>{largestText}</p>
          </p>
          <Row>
            {currentIndex !== images.length - 1 ? (
              <>
                <Col className="d-flex justify-content-end">
                  <Button
                    variant="secondary"
                    onClick={() => { setCurrentIndex((currentIndex) => (currentIndex - 1)); }}
                    disabled={currentIndex === 0}
                    className="tuto-modal__button"
                  >
                    {t('previous')}
                  </Button>
                </Col>
                <Col className="d-flex justify-content-start">
                  <Button
                    onClick={() => { setCurrentIndex((currentIndex) => (currentIndex + 1)); }}
                    disabled={currentIndex === images.length - 1}
                    className="tuto-modal__button client-primary-background-color"
                  >
                    {t('next')}
                  </Button>
                </Col>
              </>
            ) : (
              <Col className="d-flex justify-content-center">
                <Button
                  onClick={onHide}
                  className="tuto-modal__button--end client-primary-background-color"
                >
                  {t('end')}
                </Button>
              </Col>
            )}
          </Row>
          <Button
            variant="link"
            onClick={onHide}
            className="tuto-modal__hide-button"
          >
            {t('ignore')}
          </Button>
          <div className="d-flex justify-content-center"><LanguageSelector small align="right" /></div>
        </div>
      </Modal.Body>
    </Modal>
  );
};

export default TutoModals;
