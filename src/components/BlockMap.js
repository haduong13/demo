import React from 'react';
import GoogleMapReact from 'google-map-react';
import marker from 'assets/images/marker.png';
import 'assets/style/modal-payment.scss';

const AnyReactComponent = () => (
  <div>
    <img src={marker} alt="" />
  </div>
);

const BlockMap = ({ lat, lng, loadMap }) => (
  <div className="google-map-show">
    {!loadMap && (
    <GoogleMapReact
      bootstrapURLKeys={{
        key: 'AIzaSyAiQX1yfcvHCeM98e6asi-Wi-Y_H4-V1Qw',
      }}
      defaultCenter={{
        lat,
        lng,
      }}
      defaultZoom={17}
    >
      <AnyReactComponent lat={lat} lng={lng} />
    </GoogleMapReact>
    )}
  </div>
);

export default BlockMap;
