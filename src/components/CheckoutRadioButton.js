import React from 'react';
import StyledRadioButton from 'components/StyledRadioButton';

const CheckoutRadioButton = ({
  title,
  subTitle,
  Icon,
  color = '#004bb4',
  className = '',
  ...props
}) => (
  <label className={`radio-btn-custom d-flex align-items-center pt-2 pb-2 pl-3 pr-3 border ${className}`}>
    <StyledRadioButton
      color={color}
      className="checkout__radio"
      {...props}
    />
    <span className="pl-2 pr-1">{title}</span>
    {subTitle && (
      <span className="pl-2 pr-1 text-secondary">{subTitle}</span>
    )}
    {Icon && (
      <Icon className="ml-auto checkout-svg" color={color} />
    )}
  </label>
);

export default CheckoutRadioButton;
