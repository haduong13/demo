import React, {
  useCallback,
} from 'react';
import {
  NavLink,
} from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { logout } from 'logic/actions/authActions';
import Button from 'components/Button';
import { useLoadingScreen } from 'helpers/LoadingScreenContext';
import LanguageSelector from 'components/LanguageSelector';
import iconWallet from 'assets/images/icon-wallet.svg';
import iconPersonalInfo from 'assets/images/icon-personal-information.svg';
import iconPayment from 'assets/images/icon-payment.svg';
import iconProfile from 'assets/images/icon-verified-profile.svg';
import iconMyTransaction from 'assets/images/icon-my-transaction.svg';
import iconSignOut from 'assets/images/icon-sign-out.svg';
import iconHelpCenter from 'assets/images/icon-help-center.svg';
import 'assets/style/menu-responsive.scss';

const HeaderResponsive = ({
  hideMenu,
}) => {
  const { t } = useTranslation(['headerResponsive', '_links']);
  const setLoadingScreen = useLoadingScreen();
  const dispatch = useDispatch();

  const handleLogout = useCallback(() => {
    setLoadingScreen(true);
    dispatch(logout())
      .finally(() => {
        setLoadingScreen(false);
      });
  }, [dispatch, setLoadingScreen]);

  return (
    <div className="block-menu-responsive" id="menu-responsive">
      <div
        className="block-menu-responsive__opacity"
        onClick={hideMenu}
        onKeyPress={hideMenu}
        role="button"
        tabIndex={0}
        aria-label="Close menu"
      />
      <ul className="block-menu-responsive__list-bar">
        <li>
          <NavLink to="/" onClick={hideMenu} onKeyPress={hideMenu}>
            <span className="text-list">
              <img className="icon-list" src={iconMyTransaction} alt="" />
              {t('home')}
            </span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/settings/wallets" onClick={hideMenu} onKeyPress={hideMenu}>
            <span className="text-list">
              <img className="icon-list" src={iconWallet} alt="" />
              {t('wallet')}
            </span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/settings/profile" onClick={hideMenu} onKeyPress={hideMenu}>
            <span className="text-list">
              <img className="icon-list" src={iconPersonalInfo} alt="" />
              {t('profile')}
            </span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/settings/payments" onClick={hideMenu} onKeyPress={hideMenu}>
            <span className="text-list">
              <img className="icon-list" src={iconPayment} alt="" />
              {t('payments')}
            </span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/settings/profile-verification" onClick={hideMenu} onKeyPress={hideMenu}>
            <span className="text-list">
              <img className="icon-list" src={iconProfile} alt="" />
              {t('kyc')}
            </span>
          </NavLink>
        </li>
        <li className="margin-top-list">
          <a href={t('_links:supportCenter')} target="_blank" rel="noopener noreferrer" onClick={hideMenu} onKeyPress={hideMenu}>
            <span className="text-list">
              <img className="icon-list" src={iconHelpCenter} alt="" />
              {t('helpCenter')}
            </span>
          </a>
        </li>
        <li>
          <Button
            as="a"
            variant="link"
            onClick={handleLogout}
            onKeyPress={handleLogout}
          >
            <span className="text-list">
              <img className="icon-list" src={iconSignOut} alt="" />
              {t('logout')}
            </span>
          </Button>
        </li>
        <li className="mt-auto">
          <LanguageSelector up />
        </li>
      </ul>
    </div>
  );
};

export default HeaderResponsive;
