import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import iconClose from "assets/images/icon-close.svg";
import iconLeft from "assets/images/icon-left.svg";
import InfoAddressForm from "components/InfoAddressForm";
import ListDeliveryMethod from "components/ListDeliveryMethod";
import "assets/style/parcel-delivery-modal.scss";

const ParcelDeliveryModal = ({ show, onHide, confirmInfo }) => {
  const [stepForm, setStepForm] = useState(1);

  const onHandleConfirmMethod = () => {
    setStepForm(2);
  };

  const onBackToMethod = () => {
    setStepForm(1);
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      centered
      className="parcel-delivery-modal"
    >
      <Modal.Body>
        {stepForm === 1 ? (
          <div id="method-delivery" className="parcel-delivery-modal__content">
            <div
              onClick={onHide}
              onKeyDown={onHide}
              className="parcel-delivery-modal__icon-close"
              role="button"
              tabIndex={0}
            >
              <img src={iconClose} alt="" />
            </div>
            <p className="parcel-delivery-modal__title">
              Select your delivery method
            </p>
            <ListDeliveryMethod onHandleConfirmMethod={onHandleConfirmMethod} />
          </div>
        ) : (
          <div id="address-delivery" className="parcel-delivery-modal__content">
            <div
              className="parcel-delivery-modal__icon-preview"
              onClick={onBackToMethod}
              onKeyDown={onBackToMethod}
              role="button"
              tabIndex={0}
            >
              <img src={iconLeft} alt="" />
            </div>
            <div
              onClick={onHide}
              onKeyDown={onBackToMethod}
              role="button"
              tabIndex={0}
              className="parcel-delivery-modal__icon-close"
            >
              <img src={iconClose} alt="" />
            </div>
            <p className="parcel-delivery-modal__title">
              Enter your shipping address and select a pick-up location
            </p>
            <InfoAddressForm confirmInfo={confirmInfo} />
          </div>
        )}
      </Modal.Body>
    </Modal>
  );
};

export default ParcelDeliveryModal;
