import React from 'react';
import { useTranslation } from 'react-i18next';
import LanguageSelector from 'components/LanguageSelector';
import { useSelector } from 'react-redux';
import 'assets/style/footer.scss';

const Footer = () => {
  const { t } = useTranslation(['footer', '_links']);
  const terms = useSelector((state) => (state.persistent.termsReducer.terms));

  return (
    <ul className="footer">
      <li className="footer__link">
        <a href={t('_links:tripartieWebsite')} target="_blank" rel="noopener noreferrer">{t('poweredByTripartie')}</a>
      </li>
      <li className="footer__link">
        <a href={t('_links:supportCenter')} target="_blank" rel="noopener noreferrer">{t('helpCenter')}</a>
      </li>
      <li className="footer__link">
        <a href={terms?.url} target="_blank" rel="noopener noreferrer">{t('terms')}</a>
      </li>
      <li>
        <LanguageSelector up small />
      </li>
    </ul>
  );
};

export default Footer;
