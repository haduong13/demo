import React from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { FormattedNumber } from 'react-intl';
import { useHistory } from 'react-router-dom';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

const TransactionListItem = ({
  me,
  transaction = {},
}) => {
  const { t } = useTranslation('transactionListItem');
  const history = useHistory();

  const {
    id,
    title,
    sellerDisplayName = 'Non inscrit',
    buyerId,
    buyerDisplayName = 'Non inscrit',
    client,
    sellerTotal,
    buyerTotal,
    currency = 'EUR',
  } = transaction;

  const isPurchase = me.id === buyerId;
  const total = isPurchase ? -buyerTotal : sellerTotal;

  return (
    <Row
      className="home-card mb-3 pb-3"
      onClick={() => history.push(`/transactions/${id}`, { transaction })}
    >
      <Col className="home-card__left">
        {/* <div className="d-none d-lg-block">
          <img alt="" src={avatar} width="50px" />
        </div> */}
        <div className="pl-lg-1">
          <h5 className="mb-2 home-card__title">{title}</h5>
          <div className="home-card__details">
            <div>
              <p className="m-0">
                <span className="key">
                  {isPurchase ? t('purchase') : t('sale')}
                  :
                </span>
                <span className="value">{isPurchase ? sellerDisplayName : buyerDisplayName}</span>
                {/* <span className="d-none d-lg-inline-block home-card__check" /> */}
              </p>
            </div>
            <div>
              <p className="m-0">
                <span className="key">
                  {t('platform')}
                  :
                </span>
                <span className="value">{client}</span>
              </p>
            </div>
          </div>
        </div>
      </Col>
      <Col
        md="2"
        className={
          `home-card__price m-0 pl-lg-3 ${isPurchase ? 'losses' : ''}`
        }
      >
        <h5 className="m-0 ml-auto">
          <FormattedNumber value={total / 100} style="currency" currency={currency} />
        </h5>
      </Col>
    </Row>
  );
};

const mapStateToProps = (state) => ({
  me: state.persistent.meReducer.me,
});

export default connect(
  mapStateToProps,
)(TransactionListItem);
