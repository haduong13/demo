import React, {
  useEffect,
  useState,
  useCallback,
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import Spinner from 'react-bootstrap/Spinner';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Modal from 'react-bootstrap/Modal';
import Button from 'components/Button';
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';
import AddBankAccountModal from 'components/AddBankAccountModal';
import useWindowSize from 'helpers/useWindowSize';

import { refreshMe } from 'logic/actions/meActions';
import {
  getOwnBankAccounts,
  deleteBankAccount,
  createBankAccount,
} from 'logic/actions/bankAccountsActions';
import {
  getOwnPaymentCards,
  deleteCard,
} from 'logic/actions/paymentCardsActions';
import { getCountries } from 'logic/actions/countriesActions';
import 'assets/style/setting-wallet.scss';
import BankAccountForm from 'components/BankAccountForm';
import iconAddPlus from 'assets/images/icon-add-plus.svg';
import iconInfo from 'assets/images/icon-info.svg';
import iconLeft from 'assets/images/icon-left.svg';
import IconPaymentMethodCB from 'assets/images/icon-payment-method-cb.svg';
import IconPaymentMethodVisa from 'assets/images/icon-payment-method-visa.svg';
import IconPaymentMethodMastercard from 'assets/images/icon-payment-method-mastercard.svg';
import 'assets/style/modal-payment.scss';

const Payments = ({
  me: { userProfile: { residency } = {} } = {},
  bankAccounts = [],
  paymentCards = [],
  countries,
  refreshMe,
  getOwnBankAccounts,
  deleteBankAccount,
  createBankAccount,
  getOwnPaymentCards,
  deleteCard,
  getCountries,
}) => {
  const { t } = useTranslation('payments');
  const windowSize = useWindowSize();
  const deleteBankAccountSuccessToast = useCallback(() => toast.success(t('deleteBankAccountSuccess')), [t]);
  const deleteBankAccountErrorToast = useCallback(() => toast.error(t('deleteBankAccountError')), [t]);
  const deleteCardSuccessToast = useCallback(() => toast.success(t('deleteCardSuccess')), [t]);
  const deleteCardErrorToast = useCallback(() => toast.error(t('deleteCardError')), [t]);
  const addBankAccountSuccessToast = useCallback(() => toast.success(t('addBankAccountSuccess')), [t]);
  const addBankAccountErrorToast = useCallback(() => toast.error(t('addBankAccountError')), [t]);

  const [loading, setLoading] = useState(false);
  const [deleting, setDeleting] = useState(false);

  const [showNewBankAccountForm, setShowNewBankAccountForm] = useState(false);
  const [showNewPaymentCardForm, setShowNewPaymentCardForm] = useState(false); // eslint-disable-line no-unused-vars
  const showMain = !windowSize.isNarrow || (!showNewBankAccountForm && !showNewPaymentCardForm);

  const [showDeleteBankAccountModal, setShowDeleteBankAccountModal] = useState(false);
  const [bankAccountId, setBankAccountId] = useState(0);
  const openDeleteBankAccountModal = (bankAccountId) => {
    setShowDeleteBankAccountModal(true);
    setBankAccountId(bankAccountId);
  };
  const closeDeleteBankAccountModal = () => {
    setShowDeleteBankAccountModal(false);
    setBankAccountId(0);
  };

  const [showDeletePaymentCardModal, setShowDeletePaymentCardModal] = useState(false);
  const [paymentCardId, setPaymentCardId] = useState(0);
  const openDeletePaymentCardModal = (paymentCardId) => {
    setShowDeletePaymentCardModal(true);
    setPaymentCardId(paymentCardId);
  };
  const closeDeletePaymentCardModal = () => {
    setShowDeletePaymentCardModal(false);
    setPaymentCardId(0);
  };

  useEffect(() => {
    setLoading(!bankAccounts.length && !paymentCards.length);
    refreshMe();
    getCountries();
    Promise.allSettled([
      getOwnBankAccounts(),
      getOwnPaymentCards(),
    ]).finally(() => {
      setLoading(false);
    });
  }, []);

  const deleteBankAccountModal = (
    <Modal
      show={showDeleteBankAccountModal}
      onHide={closeDeleteBankAccountModal}
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>{t('deleteModal.title')}</Modal.Title>
      </Modal.Header>

      <Modal.Body>{t('deleteModal.areYouSure')}</Modal.Body>

      <Modal.Footer>
        <Button variant="secondary" onClick={closeDeleteBankAccountModal}>
          {t('deleteModal.close')}
        </Button>
        <Button
          variant="danger"
          disabled={bankAccountId === 0}
          isLoading={deleting}
          onClick={() => {
            setDeleting(true);
            deleteBankAccount(bankAccountId)
              .then(() => {
                getOwnBankAccounts().finally(() => {
                  deleteBankAccountSuccessToast();
                  closeDeleteBankAccountModal();
                  setDeleting(false);
                });
              })
              .catch(() => {
                deleteBankAccountErrorToast();
                closeDeleteBankAccountModal();
                setDeleting(false);
              });
          }}
        >
          {t('deleteModal.ok')}
        </Button>
      </Modal.Footer>
    </Modal>
  );

  const deletePaymentCardModal = (
    <Modal
      show={showDeletePaymentCardModal}
      onHide={closeDeletePaymentCardModal}
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>{t('deleteModal.title')}</Modal.Title>
      </Modal.Header>

      <Modal.Body>{t('deleteModal.areYouSure')}</Modal.Body>

      <Modal.Footer>
        <Button variant="secondary" onClick={closeDeletePaymentCardModal}>
          {t('deleteModal.close')}
        </Button>
        <Button
          variant="danger"
          disabled={paymentCardId === 0}
          isLoading={deleting}
          onClick={() => {
            setDeleting(true);
            deleteCard(paymentCardId)
              .then(() => {
                getOwnPaymentCards().finally(() => {
                  deleteCardSuccessToast();
                  closeDeletePaymentCardModal();
                  setDeleting(false);
                });
              })
              .catch(() => {
                deleteCardErrorToast();
                closeDeletePaymentCardModal();
                setDeleting(false);
              });
          }}
        >
          {t('deleteModal.ok')}
        </Button>
      </Modal.Footer>
    </Modal>
  );

  const getCardProviderLogo = (cardProvider) => {
    switch (cardProvider) {
      case 'VISA':
        return IconPaymentMethodVisa;

      case 'MASTERCARD':
        return IconPaymentMethodMastercard;

      case 'CB':
        return IconPaymentMethodCB;

      default:
        return (<div />);
    }
  };

  return (
    <>
      <Helmet><title>{t('pageTitle')}</title></Helmet>
      {loading ? (
        <Spinner
          className="mx-auto d-block color-blue-tripartie"
          animation="border"
        />
      ) : (
        <>
          {deleteBankAccountModal}
          {deletePaymentCardModal}

          <AddBankAccountModal
            show={!windowSize.isNarrow && showNewBankAccountForm}
            onHide={() => {
              setShowNewBankAccountForm(false);
            }}
            onSubmit={(data, { setSubmitting }) => {
              createBankAccount(data)
                .then(() => {
                  getOwnBankAccounts().finally(() => {
                    addBankAccountSuccessToast();
                    setShowNewBankAccountForm(false);
                  });
                })
                .catch(() => {
                  addBankAccountErrorToast();
                  setSubmitting(false);
                });
            }}
            countries={countries}
            initialValues={{ country: residency }}
          />
          {windowSize.isNarrow && showNewBankAccountForm && (
            <div className="form-add-payment-responsive">
              <div
                onClick={() => { setShowNewBankAccountForm(false); }}
                onKeyPress={() => { setShowNewBankAccountForm(false); }}
                role="button"
                tabIndex={0}
              >
                <p className="form-add-payment-responsive__title">
                  <img
                    className="form-add-payment-responsive__icon-left"
                    src={iconLeft}
                    alt=""
                  />
                  {t('bankAccounts.add')}
                </p>
              </div>
              <div className="modal-add-payment">
                <div className="form-content">
                  <BankAccountForm
                    countries={countries}
                    initialValues={{ country: residency }}
                    onSubmit={(data, { setSubmitting }) => {
                      createBankAccount(data)
                        .then(() => {
                          getOwnBankAccounts().finally(() => {
                            addBankAccountSuccessToast();
                            setShowNewBankAccountForm(false);
                          });
                        })
                        .catch(() => {
                          addBankAccountErrorToast();
                          setSubmitting(false);
                        });
                    }}
                  />
                </div>
              </div>
            </div>
          )}

          {/* <AddPaymentCardModal
            show={!windowSize.isNarrow && showNewPaymentCardForm}
            onHide={() => {
              setShowNewPaymentCardForm(false);
            }}
            countries={countries}
            initialValues={{ country: residency }}
          />
          {windowSize.isNarrow && showNewPaymentCardForm && (
            <div className="form-add-payment-responsive">
              <div
                onClick={() => {setShowNewPaymentCardForm(false);}}
                onKeyPress={() => {setShowNewPaymentCardForm(false);}}
                role="button"
                tabIndex={0}
              >
                <p className="form-add-payment-responsive__title">
                  <img
                    className="form-add-payment-responsive__icon-left"
                    src={iconLeft}
                    alt=""
                  />
                  {t('paymentCards.add')}
                </p>
              </div>
              <div className="modal-add-payment">
                <div className="form-content">
                  <PaymentCardForm />
                </div>
              </div>
            </div>
          )} */}

          {showMain && (
            <>
              <div className="block-payment">
                <p className="block-payment__title">{t('bankAccounts.title')}</p>
                <p className="block-payment__description">
                  {t('bankAccounts.description')}
                </p>
                {bankAccounts.map(({
                  id,
                  iban,
                }) => (
                  <Row key={id} className="block-payment-item">
                    <Col>
                      <p>
                        {iban.replaceAll(/(.{4})/g, '$1 ')}
                      </p>
                    </Col>
                    <Col className="col-auto">
                      <Button
                        className="block-payment-item__button"
                        onClick={() => {
                          openDeleteBankAccountModal(id);
                        }}
                      >
                        {t('bankAccounts.delete')}
                      </Button>
                    </Col>
                  </Row>
                ))}
                <div
                  className="block-payment__bottom"
                  onClick={() => { setShowNewBankAccountForm(true); }}
                  onKeyPress={() => { setShowNewBankAccountForm(true); }}
                  role="button"
                  tabIndex={0}
                >
                  <span className="block-payment__text-add">
                    <img
                      className="block-payment__icon-add-plus"
                      src={iconAddPlus}
                      alt=""
                    />
                    {t('bankAccounts.add')}
                  </span>
                </div>
              </div>
              <div className="block-payment">
                <div className="block-payment__items">
                  <p className="block-payment__title">{t('paymentCards.title')}</p>
                  {paymentCards.map(({
                    id,
                    obfuscatedNumber,
                    expirationDate,
                    cardProvider,
                    valid = true,
                  }) => (
                    <Row key={id} className="block-payment-item">
                      <Col>
                        <p className="text-nowrap">
                          {obfuscatedNumber.replaceAll(/.*([0-9]{4})$/g, '•••• $1')}
                        </p>
                      </Col>
                      <Col>
                        <p className="text-nowrap block-payment-item__expire">
                          {t('paymentCards.expire', { expirationDate: expirationDate.replace(/(.{2})(.{2})/, '$1/$2') })}
                        </p>
                      </Col>
                      <Col>
                        <p className="d-flex align-items-baseline">
                          <img
                            className="card-provider-logo ml-3"
                            src={getCardProviderLogo(cardProvider)}
                            alt=""
                          />
                        </p>
                      </Col>
                      {!valid && (
                        <Col>
                          <img
                            className="block-payment-item__icon-invalid"
                            src={iconInfo}
                            alt=""
                          />
                        </Col>
                      )}
                      <Col className="col-auto">
                        <Button
                          className="block-payment-item__button"
                          onClick={() => { openDeletePaymentCardModal(id); }}
                        >
                          {t('paymentCards.delete')}
                        </Button>
                      </Col>
                    </Row>
                  ))}
                </div>
                {/* <div
                  className="block-payment__bottom"
                  onClick={() => { setShowNewPaymentCardForm(true); }}
                  onKeyPress={() => { setShowNewPaymentCardForm(true); }}
                  role="button"
                  tabIndex={0}
                >
                  <span className="block-payment__text-add">
                    <img
                      className="block-payment__icon-add-plus"
                      src={iconAddPlus}
                      alt=""
                    />
                    {t('paymentCards.add')}
                  </span>
                </div> */}
              </div>
            </>
          )}
        </>
      )}
    </>
  );
};

const mapStateToProps = (state) => ({
  me: state.persistent.meReducer.me,
  bankAccounts: state.persistent.bankAccountsReducer.bankAccounts,
  paymentCards: state.persistent.paymentCardsReducer.paymentCards,
  countries: state.persistent.countriesReducer.countries,
});

const mapDispatchToProps = (dispatch) => ({
  refreshMe: bindActionCreators(refreshMe, dispatch),
  getOwnBankAccounts: bindActionCreators(getOwnBankAccounts, dispatch),
  deleteBankAccount: bindActionCreators(deleteBankAccount, dispatch),
  createBankAccount: bindActionCreators(createBankAccount, dispatch),
  getOwnPaymentCards: bindActionCreators(getOwnPaymentCards, dispatch),
  deleteCard: bindActionCreators(deleteCard, dispatch),
  getCountries: bindActionCreators(getCountries, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Payments);
