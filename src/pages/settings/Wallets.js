import React, {
  useEffect,
  useState,
  useCallback,
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import Spinner from 'react-bootstrap/Spinner';
import Alert from 'react-bootstrap/Alert';
import Modal from 'react-bootstrap/Modal';
import Button from 'components/Button';
import Form from 'react-bootstrap/Form';
import { useTranslation, Trans } from 'react-i18next';
import { toast } from 'react-toastify';
import { FormattedNumber } from 'react-intl';
import { refreshMe } from 'logic/actions/meActions';
import { getOwnBankAccounts } from 'logic/actions/bankAccountsActions';
import { getOwnWallets, cashout } from 'logic/actions/walletsActions';
import 'assets/style/setting-wallet.scss';

const DEFAULT_WALLET = {
  balance: 0,
  currency: 'EUR',
};

const Wallets = ({
  me: { canCashout } = {},
  wallets = [],
  bankAccounts = [],
  refreshMe,
  getOwnBankAccounts,
  getOwnWallets,
  cashout,
}) => {
  const { t } = useTranslation('wallets');
  const cashoutSuccessToast = useCallback(() => toast.success(t('cashoutSuccess')), [t]);
  const cashoutErrorToast = useCallback(() => toast.error(t('cashoutError')), [t]);
  const [cashingOut, setCashingOut] = useState(false);
  const [loading, setLoading] = useState(false);
  const [modalIsVisible, setModalIsVisible] = useState(false);
  const [walletId, setWalletId] = useState(0);
  const [bankAccountId, setBankAccountId] = useState(0);
  const showModal = (walletId) => {
    setModalIsVisible(true);
    setWalletId(walletId);
    setBankAccountId(0);
  };
  const hideModal = () => {
    setModalIsVisible(false);
    setWalletId(0);
    setBankAccountId(0);
  };

  useEffect(() => {
    setLoading(!wallets.length);
    refreshMe();
    getOwnBankAccounts();
    getOwnWallets().finally(() => {
      setLoading(false);
    });
  }, []);

  const cashoutModal = (
    <Modal show={modalIsVisible} onHide={hideModal} centered>
      <Modal.Header closeButton>
        <Modal.Title>{t('modal.title')}</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Form.Control
          as="select"
          custom
          onChange={({ target: { value } = {} }) => {
            setBankAccountId(value);
          }}
        >
          <option value={0}>{t('modal.chooseBankAccount')}</option>
          {bankAccounts?.map(({ id, iban }) => (
            <option key={id} value={id}>
              {iban.replace(/([^ ]{4})/g, '$1 ')}
            </option>
          ))}
        </Form.Control>
      </Modal.Body>

      <Modal.Footer>
        <Button variant="secondary" onClick={hideModal}>
          {t('modal.close')}
        </Button>
        <Button
          variant="primary"
          disabled={bankAccountId === 0}
          isLoading={cashingOut}
          onClick={() => {
            setCashingOut(true);
            cashout(walletId, bankAccountId)
              .then(() => {
                cashoutSuccessToast();
                setLoading(true);
                getOwnWallets().finally(() => {
                  setLoading(false);
                });
              })
              .catch(() => {
                cashoutErrorToast();
              })
              .finally(() => {
                hideModal();
                setCashingOut(false);
              });
          }}
        >
          {t('modal.ok')}
        </Button>
      </Modal.Footer>
    </Modal>
  );

  const positiveWallets = wallets.filter(({ balance }) => balance);
  const walletsToDisplay = positiveWallets.length ? positiveWallets : [DEFAULT_WALLET];

  return (
    <>
      <Helmet><title>{t('pageTitle')}</title></Helmet>
      {loading ? (
        <Spinner
          className="mx-auto d-block color-blue-tripartie"
          animation="border"
        />
      ) : (
        <div className="block-wallet">
          {cashoutModal}

          {!canCashout && positiveWallets.length > 0 && (
            <Alert variant="danger">
              <Trans
                t={t}
                i18nKey="cannotCashout"
              >
                To be able to withdraw, you must
                <Link to="/settings/profile-verification">verify your profile</Link>
                and
                <Link to="/settings/payments">add a bank account</Link>
                .
              </Trans>
            </Alert>
          )}

          {walletsToDisplay.map(({
            id, balance, currency, hasInProgessCashout,
          }) => (
            <div key={id} className="block-wallet__item">
              <p className="block-wallet__title-main">{t('walletTitle', { currency })}</p>
              {hasInProgessCashout ? (
                <Alert variant="info">{t('cashoutInProgress')}</Alert>
              ) : (
                <>
                  <p className="block-wallet__money">
                    <FormattedNumber value={balance / 100} style="currency" currency={currency} />
                  </p>
                  <Button
                    className="block-wallet__button-move"
                    disabled={!balance || !canCashout || hasInProgessCashout}
                    onClick={() => {
                      showModal(id);
                    }}
                  >
                    {t('cashout')}
                  </Button>
                </>
              )}
            </div>
          ))}
        </div>
      )}
    </>
  );
};

const mapStateToProps = (state) => ({
  me: state.persistent.meReducer.me,
  bankAccounts: state.persistent.bankAccountsReducer.bankAccounts,
  wallets: state.persistent.walletsReducer.wallets,
});

const mapDispatchToProps = (dispatch) => ({
  refreshMe: bindActionCreators(refreshMe, dispatch),
  getOwnBankAccounts: bindActionCreators(getOwnBankAccounts, dispatch),
  getOwnWallets: bindActionCreators(getOwnWallets, dispatch),
  cashout: bindActionCreators(cashout, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Wallets);
