import React, {
  useEffect,
  useState,
  useCallback,
} from 'react';
import _ from 'lodash';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import { Helmet } from 'react-helmet';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import Alert from 'react-bootstrap/Alert';
import Button from 'components/Button';
import * as yup from 'yup';
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';
import { sort } from 'helpers/stringHelpers';

import {
  refreshMe, updateMe, deleteMyAccount,
} from 'logic/actions/meActions';
import { logout } from 'logic/actions/authActions';
import { checkEmail } from 'logic/actions/usersActions';
import { getCountries } from 'logic/actions/countriesActions';

const ERROR_EMAIL_ALREADY_EXISTS = 'email_already_exists';
const STATUS_VERIFIED = 'status_verified';

const Profile = ({
  me: {
    email,
    identityVerifyStatus,
    canBeDeleted,
    userProfile: {
      firstName,
      lastName,
      birthday,
      nationality,
      residency,
    } = {},
  } = {},
  countries = [],
  refreshMe,
  updateMe,
  deleteMyAccount,
  checkEmail,
  getCountries,
  logout,
}) => {
  const { t, i18n } = useTranslation(['profile', '_countries']);
  const updateSuccessToast = useCallback(() => toast.success(t('form.updateSuccess')), [t]);
  const updateErrorToast = useCallback(() => toast.error(t('form.updateError')), [t]);
  const deleteErrorToast = useCallback(() => toast.error(t('deleteError')), [t]);
  const [previousEmail, setPreviousEmail] = useState(email);
  const [previousEmailTestResult, setPreviousEmailTestResult] = useState(true);
  const [deleting, setDeleting] = useState(false);
  const [nationalities, setNationalities] = useState(false);
  const [showConfirmDeleteModal, setShowConfirmDeleteModal] = useState(false);

  useEffect(() => {
    refreshMe();
    getCountries();
  }, []);

  useEffect(() => {
    setNationalities(countries.filter(({ slug }) => (i18n.exists(`_countries:nationality.${slug}`))));
  }, [countries, i18n]);

  const confirmDeleteModal = (
    <Modal
      show={showConfirmDeleteModal}
      onHide={() => { setShowConfirmDeleteModal(false); }}
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>{t('confirmDeleteModal.title')}</Modal.Title>
      </Modal.Header>

      <Modal.Body>{t('confirmDeleteModal.areYouSure')}</Modal.Body>

      <Modal.Footer>
        <Button variant="secondary" onClick={() => { setShowConfirmDeleteModal(false); }}>
          {t('confirmDeleteModal.close')}
        </Button>
        <Button
          variant="danger"
          isLoading={deleting}
          disabled={!canBeDeleted}
          onClick={() => {
            setDeleting(true);
            deleteMyAccount()
              .then(() => {
                logout();
              })
              .catch(() => {
                deleteErrorToast();
              })
              .finally(() => {
                setDeleting(false);
              });
          }}
        >
          {t('confirmDeleteModal.confirm')}
        </Button>
      </Modal.Footer>
    </Modal>
  );

  return (
    <>
      <Helmet><title>{t('pageTitle')}</title></Helmet>
      <div>
        {confirmDeleteModal}
        <Formik
          validateOnChange={false}
          validateOnBlur={false}
          validationSchema={yup.object({
            firstName: yup.string().required(t('form.emptyFirstName')),
            lastName: yup.string().required(t('form.emptyLastName')),
            residency: yup.string().required(t('form.emptyResidency')),
            nationality: yup.string().required(t('form.emptyNationality')),
            email: yup
              .string()
              .required(t('form.emptyEmail'))
              .email(t('form.invalidEmail'))
              .test({
                name: 'emailValid',
                test: (newEmail, { createError }) => {
                  if (!newEmail || newEmail === previousEmail) {
                    return previousEmailTestResult;
                  }
                  setPreviousEmail(newEmail);
                  return checkEmail(newEmail)
                    .then(() => {
                      setPreviousEmailTestResult(true);
                      return true;
                    })
                    .catch(
                      ({ error: { response: { data: { errors } = {} } = {} } = {} }) => {
                        let result = createError({ message: t('form.invalidEmail') });
                        if (errors === ERROR_EMAIL_ALREADY_EXISTS) {
                          result = createError({ message: t('form.alreadyUsedEmail') });
                        }
                        setPreviousEmailTestResult(result);
                        return result;
                      },
                    );
                },
              }),
            birthday: yup
              .date()
              .required(t('form.emptyBirthday')),
          })}
          onSubmit={(
            {
              email,
              firstName,
              lastName,
              birthday,
              nationality,
              residency,
            },
            { resetForm },
          ) => {
            updateMe({
              email,
              userProfile: {
                firstName,
                lastName,
                birthday,
                nationality,
                residency,
              },
            })
              .then(() => {
                updateSuccessToast();
              })
              .catch(() => {
                updateErrorToast();
              })
              .finally(() => {
                resetForm({
                  values: {
                    email,
                    firstName,
                    lastName,
                    birthday,
                    nationality,
                    residency,
                  },
                });
              });
          }}
          initialValues={{
            email,
            firstName,
            lastName,
            birthday,
            nationality,
            residency,
          }}
        >
          {({
            handleSubmit,
            handleChange,
            handleBlur,
            values,
            touched,
            errors,
            isSubmitting,
            dirty,
          }) => (
            <Form noValidate onSubmit={handleSubmit}>
              <div className="block-profile">
                <p className="block-profile__title">{t('personalInformation')}</p>
                {identityVerifyStatus === STATUS_VERIFIED && (
                  <Alert variant="warning">{t('warnKycRevocation')}</Alert>
                )}
                <Form.Group controlId="email">
                  <Form.Label>{t('form.email')}</Form.Label>
                  <Form.Control
                    type="email"
                    name="email"
                    value={values.email}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    isInvalid={touched.email && !!errors.email}
                    placeholder={t('form.email')}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.email}
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group controlId="firstName">
                  <Form.Label>{t('form.firstName')}</Form.Label>
                  <Form.Control
                    type="text"
                    name="firstName"
                    value={values.firstName}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    isInvalid={touched.firstName && !!errors.firstName}
                    placeholder={t('form.firstName')}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.firstName}
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group md="6" controlId="lastName">
                  <Form.Label>{t('form.lastName')}</Form.Label>
                  <Form.Control
                    className=""
                    type="text"
                    name="lastName"
                    value={values.lastName}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    isInvalid={touched.lastName && !!errors.lastName}
                    placeholder={t('form.lastName')}
                  />
                  {/* <span className="message-success">Enregistré</span> */}
                  <Form.Control.Feedback type="invalid">
                    {errors.lastName}
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group md="6" controlId="birthday">
                  <Form.Label>{t('form.birthday')}</Form.Label>
                  <Form.Control
                    className={`form-control ${!values.birthday ? 'placeholder' : ''}`}
                    type="date"
                    name="birthday"
                    value={values.birthday}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    isInvalid={touched.birthday && !!errors.birthday}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.birthday}
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group controlId="nationality">
                  <Form.Label>{t('form.nationality')}</Form.Label>
                  <Form.Control
                    as="select"
                    className={`form-control ${!values.nationality ? 'placeholder' : ''}`}
                    custom
                    name="nationality"
                    value={values.nationality}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    isInvalid={touched.nationality && !!errors.nationality}
                  >
                    <option value="" className="placeholder">{t('form.nationality')}</option>
                    {sort(nationalities, ({ slug }) => (t(`_countries:nationality.${slug}`))).map(({ slug }) => (
                      <option key={slug} value={slug} className="color-input">{t(`_countries:nationality.${slug}`)}</option>
                    ))}
                  </Form.Control>
                  <Form.Control.Feedback type="invalid">
                    {errors.nationality}
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group controlId="residency">
                  <Form.Label>{t('form.residency')}</Form.Label>
                  <Form.Control
                    as="select"
                    className={`form-control ${!values.residency ? 'placeholder' : ''}`}
                    custom
                    name="residency"
                    value={values.residency}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    isInvalid={touched.residency && !!errors.residency}
                  >
                    <option value="" className="placeholder">{t('form.residency')}</option>
                    {_.sortBy(countries, ({ slug }) => (t(`_countries:country.${slug}`))).map(({ slug }) => (
                      <option key={slug} value={slug} className="color-input">{t(`_countries:country.${slug}`)}</option>
                    ))}
                  </Form.Control>
                  <Form.Control.Feedback type="invalid">
                    {errors.residency}
                  </Form.Control.Feedback>
                </Form.Group>
                {dirty && (
                  <Button
                    isLoading={isSubmitting}
                    type="submit"
                    disabled={!values.email || !values.firstName || !values.lastName || !values.birthday || !values.nationality || !values.residency}
                  >
                    {t('form.submit')}
                  </Button>
                )}
              </div>
            </Form>
          )}
        </Formik>
        <div className="block-profile">
          <p className="block-profile__title block-profile__hidden">{t('deleteMyAccount')}</p>
          {!canBeDeleted && (
            <Alert variant="danger">{t('cannotBeDeleted')}</Alert>
          )}
          <Button
            className="block-profile__delete-profile block-profile__hidden"
            variant="danger"
            isLoading={deleting}
            disabled={!canBeDeleted}
            onClick={() => { setShowConfirmDeleteModal(true); }}
          >
            {t('deleteMyAccount')}
          </Button>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  me: state.persistent.meReducer.me,
  countries: state.persistent.countriesReducer.countries,
});

const mapDispatchToProps = (dispatch) => ({
  refreshMe: bindActionCreators(refreshMe, dispatch),
  updateMe: bindActionCreators(updateMe, dispatch),
  deleteMyAccount: bindActionCreators(deleteMyAccount, dispatch),
  checkEmail: bindActionCreators(checkEmail, dispatch),
  getCountries: bindActionCreators(getCountries, dispatch),
  logout: bindActionCreators(logout, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
