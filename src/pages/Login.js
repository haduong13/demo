import React, { useCallback } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';
import { Redirect } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { getToken } from 'logic/actions/authActions';
import LoginForm from 'components/LoginForm';
import Footer from 'components/Footer';
import logo from 'assets/images/logo.svg';
import 'assets/style/login-register.scss';

const Login = ({
  getToken,
  loggedIn,
  lastUsedLogin,
  location,
}) => {
  const { t } = useTranslation('login');
  const loginErrorToast = useCallback(() => toast.error(t('form.loginError')), [t]);
  const referer = (location.state && location.state.referer) || '/';

  if (loggedIn) {
    return <Redirect to={referer} />;
  }

  return (
    <>
      <Helmet><title>{t('pageTitle')}</title></Helmet>
      <div className="login-container">
        <div className="form-login-main">
          <div className="form-login-main__content">
            <img src={logo} alt="" />
            <p className="title-main">{t('title')}</p>
            <LoginForm
              onSubmit={(values, { resetForm }) => {
                getToken(values).catch(() => {
                  loginErrorToast();
                  resetForm({
                    values: {
                      ...values,
                      password: '',
                    },
                  });
                });
              }}
              lastUsedLogin={lastUsedLogin}
            />
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  lastUsedLogin: state.persistent.authReducer.lastUsedLogin,
  loggedIn: state.persistent.authReducer.loggedIn,
});

const mapDispatchToProps = (dispatch) => ({ getToken: bindActionCreators(getToken, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(Login);
