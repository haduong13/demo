import React, {
  useEffect,
  useState,
} from 'react';
import _ from 'lodash';
import useWindowSize from 'helpers/useWindowSize';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { useTranslation, Trans } from 'react-i18next';
import Modal from 'react-bootstrap/Modal';
import { useLoadingScreen } from 'helpers/LoadingScreenContext';

import CheckoutDesktop from 'pages/checkout/CheckoutDesktop';
import CheckoutMobile from 'pages/checkout/CheckoutMobile';

import CheckoutHeader from 'components/CheckoutHeader';
import CheckoutCounterOfferModal from 'components/CheckoutCounterOfferModal';
import TutoModals from 'components/TutoModals';
import Button from 'components/Button';

import defaultTransactionImage from 'assets/images/defaultTransactionImage.svg';

import { getTerms } from 'logic/actions/termsActions';
import { fetchClientSquareLogo } from 'logic/actions/clientsActions';
import {
  refreshTransactionTemplate,
  dryRunTransactionFromTemplate,
  fetchTransactionTemplatePicture,
} from 'logic/actions/transactionTemplatesActions';

import {
  getOwnPaymentCards,
} from 'logic/actions/paymentCardsActions';

import 'assets/style/checkout.scss';

const CheckoutPage = ({
  loggedIn,
  getTerms,
  refreshTransactionTemplate,
  fetchTransactionTemplatePicture,
  fetchClientSquareLogo,
  dryRunTransactionFromTemplate,
  getOwnPaymentCards,
  location,
  match,
}) => {
  const setLoadingScreen = useLoadingScreen();
  const { t, i18n } = useTranslation('checkout');
  const currentLang = i18n.language?.replace(/^([a-z]+)-?.*/, '$1') || 'en';
  const windowSize = useWindowSize();

  const retryTransaction = (location.state && location.state.retryTransaction) || {};
  const { templateUuid } = match.params;
  const [transactionTemplate, setTransactionTemplate] = useState({});
  const [transaction, setTransaction] = useState(retryTransaction);

  const [picture, setPicture] = useState();
  const [clientSquareLogo, setClientSquareLogo] = useState();

  const [counterOfferModalIsVisible, setCounterOfferModalIsVisible] = useState(false);
  const [showPaymentErrorModal, setShowPaymentErrorModal] = useState(!_.isEmpty(retryTransaction));
  const [showTuto, setShowTuto] = useState(!loggedIn && !showPaymentErrorModal);

  const [showMobileRecap, setShowMobileRecap] = useState(_.isEmpty(retryTransaction) && windowSize.isNarrow);
  const [showSignup, setShowSignup] = useState(true);
  const [deliveryMethod, setDeliveryMethod] = useState(!_.isEmpty(retryTransaction) && 'handover');

  const [activeKey, setActiveKey] = useState(deliveryMethod ? 'payment' : 'delivery');

  useEffect(() => {
    if (!loggedIn) {
      setActiveKey('sign');
    }
  }, [loggedIn]);

  useEffect(() => {
    if (undefined === showMobileRecap) {
      setShowMobileRecap(windowSize.isNarrow);
    }
  }, [windowSize, showMobileRecap]);

  useEffect(() => {
    getTerms(currentLang);
  }, [currentLang]);

  useEffect(() => {
    setLoadingScreen(_.isEmpty(transactionTemplate));
    refreshTransactionTemplate(templateUuid)
      .then(({ payload: { data } = {} }) => {
        setTransactionTemplate(data);
        if (_.isEmpty(transaction)) {
          setTransaction(data);
        }

        const promises = [];
        promises.push(fetchClientSquareLogo(data.clientId)
          .then(({ payload: { data } = {} }) => {
            setClientSquareLogo(data);
          }));

        if (data.hasPicture) {
          promises.push(fetchTransactionTemplatePicture(data.id)
            .then(({ payload: { data } = {} }) => {
              setPicture(data);
            }));
        }

        if (loggedIn) {
          promises.push(getOwnPaymentCards());
        }

        Promise.allSettled(promises).then(() => {
          setLoadingScreen(false);
        });
      });
  }, []);

  const {
    id,
    clientTheme: {
      primary = '#004bb4',
    } = {},
  } = transactionTemplate;

  const paymentErrorModal = (
    <Modal
      show={showPaymentErrorModal}
      onHide={() => { setShowPaymentErrorModal(false); }}
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>{t('paymentErrorModal.title')}</Modal.Title>
      </Modal.Header>

      <Modal.Body className="keep-line-breaks">
        <Trans t={t} i18nKey="paymentErrorModal.text">
          An error occurred during payment. Please check your banking information and try again.\nIf the problem persists, contact
          {' '}
          <a href="support@tripartie.com">our support team</a>
          .
        </Trans>
      </Modal.Body>

      <Modal.Footer>
        <Button variant="secondary" onClick={() => { setShowPaymentErrorModal(false); }}>
          {t('paymentErrorModal.close')}
        </Button>
      </Modal.Footer>
    </Modal>
  );

  return (
    <>
      <Helmet>
        <title>{t('pageTitle')}</title>
        <style>
          {`
            .client-primary-color {
              color: ${primary} !important;
            }
            
            .client-primary-background-color {
              background-color: ${primary} !important;
            }
            
            .checkout-detail-image__inner {
              background-image: url(${picture || defaultTransactionImage});
            }
          `}
        </style>
      </Helmet>
      {paymentErrorModal}
      <TutoModals
        show={showTuto}
        onHide={() => { setShowTuto(false); }}
        tuto="buyer"
        primaryColor={primary}
      />
      <CheckoutCounterOfferModal
        transactionTemplate={transactionTemplate}
        visible={counterOfferModalIsVisible}
        setVisible={setCounterOfferModalIsVisible}
        onSubmit={({ subTotal }, { resetForm }) => {
          dryRunTransactionFromTemplate(id, { subTotal: subTotal * 100 })
            .then(({ payload: { data } }) => {
              setTransaction(data);
            })
            .finally(() => {
              setCounterOfferModalIsVisible(false);
              resetForm();
            });
        }}
      />
      <div className="w-100 checkout-container">
        <CheckoutHeader primaryColor={primary} />

        {windowSize.isNarrow ? (
          <CheckoutMobile
            showPaymentErrorModal={() => { setShowPaymentErrorModal(true); }}
            transactionTemplate={transactionTemplate}
            transaction={transaction}
            picture={picture}
            clientSquareLogo={clientSquareLogo}
            showCounterOfferModal={() => { setCounterOfferModalIsVisible(true); }}
            showSignup={showSignup}
            setShowSignup={setShowSignup}
            deliveryMethod={deliveryMethod}
            setDeliveryMethod={setDeliveryMethod}
            activeKey={activeKey}
            setActiveKey={setActiveKey}
            showMobileRecap={showMobileRecap}
            setShowMobileRecap={setShowMobileRecap}
          />
        ) : (
          <CheckoutDesktop
            showPaymentErrorModal={() => { setShowPaymentErrorModal(true); }}
            transactionTemplate={transactionTemplate}
            transaction={transaction}
            picture={picture}
            clientSquareLogo={clientSquareLogo}
            showCounterOfferModal={() => { setCounterOfferModalIsVisible(true); }}
            showSignup={showSignup}
            setShowSignup={setShowSignup}
            deliveryMethod={deliveryMethod}
            setDeliveryMethod={setDeliveryMethod}
            activeKey={activeKey}
            setActiveKey={setActiveKey}
          />
        )}
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  loggedIn: state.persistent.authReducer.loggedIn,
});

const mapDispatchToProps = (dispatch) => ({
  getTerms: bindActionCreators(getTerms, dispatch),
  refreshTransactionTemplate: bindActionCreators(refreshTransactionTemplate, dispatch),
  fetchTransactionTemplatePicture: bindActionCreators(fetchTransactionTemplatePicture, dispatch),
  dryRunTransactionFromTemplate: bindActionCreators(dryRunTransactionFromTemplate, dispatch),
  fetchClientSquareLogo: bindActionCreators(fetchClientSquareLogo, dispatch),
  getOwnPaymentCards: bindActionCreators(getOwnPaymentCards, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CheckoutPage);
