import React, { useCallback, useState } from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import { useTranslation } from 'react-i18next';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';

import { getToken, logout } from 'logic/actions/authActions';
import {
  createUser,
  checkEmail,
} from 'logic/actions/usersActions';
import { getOwnPaymentCards } from 'logic/actions/paymentCardsActions';

import CheckoutRecapDesktop from 'components/CheckoutRecapDesktop';
import CheckoutBreadcrumbBar from 'components/CheckoutBreadcrumbBar';
import Button from 'components/Button';
import LoginForm from 'components/LoginForm';
import SignupForm from 'components/SignupForm';
import Footer from 'components/Footer';

import IconNum1 from 'assets/svg/IconNum1';
import IconNum2 from 'assets/svg/IconNum2';
import IconNum3 from 'assets/svg/IconNum3';
import IconNum4 from 'assets/svg/IconNum4';
import CheckoutDeliveryMethod from 'components/CheckoutDeliveryMethod';
import CheckoutPaymentForm from 'components/CheckoutPaymentForm';
import CheckoutNote from "components/CheckoutNote";

const CheckoutDesktop = ({
  transactionTemplate = {},
  transaction,
  picture,
  clientSquareLogo,
  loggedIn,
  me: {
    userProfile: {
      firstName,
    } = {},
  } = {},
  lastUsedLogin,
  terms,
  showCounterOfferModal,
  showPaymentErrorModal,
  showSignup,
  setShowSignup,
  deliveryMethod,
  setDeliveryMethod,
  activeKey,
  setActiveKey,
  getToken,
  createUser,
  checkEmail,
  getOwnPaymentCards,
  logout,
}) => {
  const { t, i18n } = useTranslation('checkout');
  const currentLang = i18n.language?.replace(/^([a-z]+)-?.*/, '$1') || 'en';
  const loginErrorToast = useCallback(() => toast.error(t('loginError')), [t]);
  const signupErrorToast = useCallback(() => toast.error(t('signupError')), [t]);

  const [checkParcelDelivery, setCheckParcelDelivery] = useState(false);

  const onHandleCheckParcelDelivery = () => {
    setCheckParcelDelivery(true)
  }
  
  const onHandleOver = () => {
    setCheckParcelDelivery(false)
  }

  const {
    client,
    adUrl,
    clientTheme: {
      primary,
    } = {},
  } = transactionTemplate;

  return (
    <div className="checkout">
      <Row className="checkout__content">
        <Col lg="5">
          <CheckoutBreadcrumbBar
            client={client}
            adUrl={adUrl}
            clientSquareLogo={clientSquareLogo}
          />
          <CheckoutRecapDesktop
            transactionTemplate={transactionTemplate}
            transaction={transaction}
            picture={picture}
            showCounterOfferModal={showCounterOfferModal}
          />
        </Col>
        <Col lg="2">
          <div
            className="h-100 m-auto checkout__separator"
          />
        </Col>
        <Col lg="5">
          <Accordion activeKey={activeKey}>
            <Card className="border-0">
              <Card.Header className="bg-white border-0 pl-0 pr-0">
                <Accordion.Toggle
                  onClick={(e) => { e.preventDefault(); }}
                  eventKey="sign"
                  className={`pl-0 pr-0 checkout-btn-header-accordion ${loggedIn && 'disable'}`}
                >
                  <div className="m-0 d-flex align-items-center">
                    <IconNum1 className="mr-2" width="24px" color={primary} />
                    <p className="m-0">
                      <span className="checkout__title">
                        {loggedIn && firstName && (t('hello', { name: firstName }))}
                        {!(loggedIn && firstName) && showSignup && t('signup')}
                        {!(loggedIn && firstName) && !showSignup && t('login')}
                      </span>
                      {loggedIn && firstName && (
                        <Button
                          variant="link"
                          className="checkout__logout"
                          onClick={() => { logout(); }}
                          onKeyPress={() => { logout(); }}
                        >
                          {t('switchAccount')}
                        </Button>
                      )}
                    </p>
                  </div>
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="sign">
                <Card.Body className="p-0 pb-1">
                  <div className="py-0 px-2">
                    {showSignup ? (
                      <SignupForm
                        primaryColor={primary}
                        isCheckoutPage
                        setShowSignup={setShowSignup}
                        checkEmail={checkEmail}
                        terms={terms}
                        onSubmit={({
                          email,
                          firstName,
                          lastName,
                          password,
                          agreeTerms,
                        }, { setSubmitting }) => {
                          createUser({
                            email,
                            plainPassword: password,
                            userProfile: {
                              firstName,
                              lastName,
                            },
                            termsVersion: agreeTerms && terms.version,
                            lang: currentLang,
                          })
                            .then(() => {
                              getToken({
                                email,
                                password,
                              })
                                .then(() => {
                                  getOwnPaymentCards();
                                  setActiveKey('delivery');
                                })
                                .catch(() => {
                                  setShowSignup(false);
                                });
                            })
                            .catch(() => {
                              signupErrorToast();
                              setSubmitting(false);
                            });
                        }}
                      />
                    ) : (
                      <LoginForm
                        primaryColor={primary}
                        isCheckoutPage
                        lastUsedLogin={lastUsedLogin}
                        setShowSignup={setShowSignup}
                        onSubmit={(values, { resetForm }) => {
                          getToken(values)
                            .then(() => {
                              getOwnPaymentCards();
                              setActiveKey('delivery');
                            })
                            .catch(() => {
                              loginErrorToast();
                              resetForm({
                                values: {
                                  ...values,
                                  password: '',
                                },
                              });
                            });
                        }}
                      />
                    )}
                  </div>
                </Card.Body>
              </Accordion.Collapse>
            </Card>

            <Card className="border-0">
              <Card.Header className="bg-white border-0 pl-0 pr-0">
                <Accordion.Toggle
                  onClick={(e) => {
                    if (loggedIn) {
                      setActiveKey('delivery');
                    } else {
                      e.preventDefault();
                    }
                  }}
                  eventKey="delivery"
                  className={`pl-0 pr-0 checkout-btn-header-accordion ${!loggedIn && 'disable'}`}
                >
                  <p className="m-0 d-flex align-items-center">
                    <IconNum2 className="mr-2" width="24px" color={primary} />
                    <span className="checkout__title">
                      {t('chooseDeliveryMethod.desktop')}
                    </span>
                  </p>
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="delivery">
                <Card.Body className="p-0 pb-1">
                  <CheckoutDeliveryMethod
                    transactionTemplate={transactionTemplate}
                    deliveryMethod={deliveryMethod}
                    setDeliveryMethod={setDeliveryMethod}
                    setActiveKey={setActiveKey}
                    checkParcelDelivery={checkParcelDelivery}
                    onHandleCheckParcelDelivery={onHandleCheckParcelDelivery}
                    onHandleOver={onHandleOver}
                  />
                </Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card className="border-0">
              <Card.Header className="bg-white border-0 pl-0 pr-0">
                <Accordion.Toggle
                  onClick={(e) => {
                    if (deliveryMethod) {
                      setActiveKey('note');
                    } else {
                      e.preventDefault();
                    }
                  }}
                  eventKey="note"
                  className={`pl-0 pr-0 checkout-btn-header-accordion ${!deliveryMethod && 'disable'}`}
                >
                  <p className="m-0 d-flex align-items-center">
                  <IconNum3 className="mr-2" width="24px" color={primary} />
                    <span className="checkout__title">
                    {t("checkoutNote.addNote")}
                    </span>
                  </p>
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="note">
                <Card.Body className="p-0 pb-1">
                  <CheckoutNote
                    transactionTemplate={transactionTemplate}
                    deliveryMethod={deliveryMethod}
                    setActiveKey={setActiveKey}
                  />
                </Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card className="border-0">
              <Card.Header className="bg-white border-0 pl-0 pr-0">
                <Accordion.Toggle
                  onClick={(e) => {
                    if (deliveryMethod) {
                      setActiveKey('payment');
                    } else {
                      e.preventDefault();
                    }
                  }}
                  eventKey="payment"
                  className={`pl-0 pr-0 checkout-btn-header-accordion ${!deliveryMethod && 'disable'}`}
                >
                  <p className="m-0 d-flex align-items-center">
                    <IconNum4 className="mr-2" width="24px" color={primary} />
                    <span className="checkout__title">
                      {t('choosePaymentMethod.desktop')}
                    </span>
                  </p>
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="payment">
                <Card.Body className="p-0 pb-1">
                  <CheckoutPaymentForm
                    transactionTemplate={transactionTemplate}
                    transaction={transaction}
                    showPaymentErrorModal={showPaymentErrorModal}
                  />
                </Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        </Col>
      </Row>
      <Footer />
    </div>
  );
};

const mapStateToProps = (state) => ({
  loggedIn: state.persistent.authReducer.loggedIn,
  lastUsedLogin: state.persistent.authReducer.lastUsedLogin,
  me: state.persistent.meReducer.me,
  terms: state.persistent.termsReducer.terms,
});

const mapDispatchToProps = (dispatch) => ({
  getToken: bindActionCreators(getToken, dispatch),
  createUser: bindActionCreators(createUser, dispatch),
  checkEmail: bindActionCreators(checkEmail, dispatch),
  getOwnPaymentCards: bindActionCreators(getOwnPaymentCards, dispatch),
  logout: bindActionCreators(logout, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CheckoutDesktop);
