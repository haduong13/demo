import React, {
  useState,
  useCallback,
} from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import { useTranslation } from 'react-i18next';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import { FormattedNumber } from 'react-intl';

import { getToken, logout } from 'logic/actions/authActions';
import {
  createUser,
  checkEmail,
} from 'logic/actions/usersActions';
import { getOwnPaymentCards } from 'logic/actions/paymentCardsActions';

import Button from 'components/Button';
import CheckoutRecapMobile from 'components/CheckoutRecapMobile';
import CheckoutBreadcrumbBar from 'components/CheckoutBreadcrumbBar';
import LoginForm from 'components/LoginForm';
import SignupForm from 'components/SignupForm';
import CheckoutDeliveryMethod from 'components/CheckoutDeliveryMethod';
import CheckoutPaymentForm from 'components/CheckoutPaymentForm';
import InfoAddressFormLimit from 'components/InfoAddressFormLimit';
import ListDeliveryMethod from 'components/ListDeliveryMethod';
import BlockMap from 'components/BlockMap';
import BlockListAddress from 'components/BlockListAddress';
import {
  geocodeByAddress,
  getLatLng,
} from "react-places-autocomplete";
import Footer from 'components/Footer';

import IconArrowDown from 'assets/images/down-arrow.svg';
import IconBag from 'assets/images/icon-bag.svg';
import CheckoutNote from "components/CheckoutNote";

const CheckoutMobile = ({
  me: {
    userProfile: {
      firstName,
    } = {},
  } = {},
  transactionTemplate,
  transaction,
  picture,
  clientSquareLogo,
  loggedIn,
  lastUsedLogin,
  terms,
  showCounterOfferModal,
  showPaymentErrorModal,
  showSignup,
  setShowSignup,
  deliveryMethod,
  setDeliveryMethod,
  activeKey,
  setActiveKey,
  showMobileRecap,
  setShowMobileRecap,
  getToken,
  createUser,
  checkEmail,
  getOwnPaymentCards,
  logout,
}) => {
  const { t, i18n } = useTranslation('checkout');
  const currentLang = i18n.language?.replace(/^([a-z]+)-?.*/, '$1') || 'en';
  const loginErrorToast = useCallback(() => toast.error(t('loginError')), [t]);
  const signupErrorToast = useCallback(() => toast.error(t('signupError')), [t]);
  const [visibleDropdownBoxDetail, setVisibleDropdownBoxDetail] = useState(false);
  const [blockStepMethod, setBlockStepMethod] = useState(1);
  const [lat, setLat] = useState(16.066708);
  const [lng, setLng] = useState(108.21312);
  const [loadMap, setLoadMap] = useState(false);
  const [accessConfirm, setAccessConfirm] = useState(false);

  const [checkParcelDelivery, setCheckParcelDelivery] = useState(false);
  const [address, setAddress] = useState("");

  const {
    client,
    adUrl,
    clientTheme: {
      primary,
    } = {},
    subTotal = 0,
    currency = 'EUR',
  } = transactionTemplate;

  const {
    subTotal: newPrice = 0,
  } = transaction;

  const hasCounterOffer = newPrice !== subTotal;

  const showBlockChoiceMethodRespon = () => {
    setBlockStepMethod(2);
  }

  const onHandleConfirmMethod = () => {
    setBlockStepMethod(3);
  }

  const onHandleAccessConfirm = () => {
    setAccessConfirm(true);
  };

  const onHandleConfirmShippingInfo = () => {
    setBlockStepMethod(1);
    setCheckParcelDelivery(true);
  }

  const onHandleOver = () => {
    setCheckParcelDelivery(false)
  }

  const handleChangeAddress = (address) => {
    setAddress(address);
  };

  const handleSelectAddress = async (address) => {
    setAddress(address);
    setLoadMap(true);
    await geocodeByAddress(address)
      .then((results) => getLatLng(results[0]))
      .then((latLng) => {
        setLat(latLng["lat"]);
        setLng(latLng["lng"]);
      })
      .catch((error) => console.error("Error", error));
    setLoadMap(false);
  };

  const onHandleRemoveInput = () => {
    setAddress('');
    setLat(16.066708);
    setLng(108.21312);
  }

  return (
    <div className="checkout">
      <Row className={`d-lg-none align-item-center checkout-header-mobile ${visibleDropdownBoxDetail && 'bg-white'}`}>
        <Col>
          <CheckoutBreadcrumbBar
            client={client}
            adUrl={adUrl}
            clientSquareLogo={clientSquareLogo}
          />
        </Col>
        <Col
          className="m-0 d-flex align-items-center justify-content-end"
          onClick={() => setVisibleDropdownBoxDetail((visibleDropdownBoxDetail) => (!visibleDropdownBoxDetail))}
          onKeyPress={() => setVisibleDropdownBoxDetail((visibleDropdownBoxDetail) => (!visibleDropdownBoxDetail))}
          role="button"
          tabIndex={0}
        >
          <p className="m-0 d-flex align-items-center justify-content-end">
            {visibleDropdownBoxDetail ? (
              <span className="mr-2">
                {' '}
                {t('close')}
              </span>
            ) : (
              <>
                <img src={IconBag} width="15px" alt="icon" />
                {hasCounterOffer ? (
                  <span className="ml-2 mr-2 checkout-header-mobile__price">
                    <del><FormattedNumber value={subTotal / 100} style="currency" currency={currency} /></del>
                    {' '}
                    <FormattedNumber value={newPrice / 100} style="currency" currency={currency} />
                  </span>
                ) : (
                  <span className="ml-2 mr-2 checkout-header-mobile__price">
                    <FormattedNumber value={subTotal / 100} style="currency" currency={currency} />
                  </span>
                )}
              </>
            )}
            {!showMobileRecap && (
              <img
                className="checkout-header-mobile__icon-down"
                style={{
                  transition: '0.3s',
                  transform: `rotate(${visibleDropdownBoxDetail ? '180deg' : 0})`,
                }}
                src={IconArrowDown}
                width="15px"
                alt="icon"
              />
            )}
          </p>
        </Col>
        <div
          className={`position-fixed bg-white pl-3 pr-3 w-100 checkout-box-dropdown ${
            visibleDropdownBoxDetail && 'active'
          }`}
        >
          <CheckoutRecapMobile
            isDropdown
            transactionTemplate={transactionTemplate}
            transaction={transaction}
            picture={picture}
          />
          <Button
            className="w-100 btn-block checkout__btn checkout__btn--ghost client-primary-color"
            onClick={showCounterOfferModal}
          >
            {t('counterPrice')}
          </Button>
        </div>
      </Row>

      {showMobileRecap ? (
        <div className="checkout__content">
          <CheckoutRecapMobile
            transactionTemplate={transactionTemplate}
            transaction={transaction}
            picture={picture}
          />
          <div className="mt-3 pt-3 mb-5">
            <Button
              variant="link"
              className="mb-3 w-100 client-primary-color"
              onClick={showCounterOfferModal}
            >
              {t('counterPrice')}
            </Button>
            <div className="pt-2">
              <Button
                className="w-100 btn-block checkout__btn client-primary-background-color border-0"
                onClick={() => setShowMobileRecap(false)}
              >
                {t('continue')}
              </Button>
            </div>
          </div>
        </div>
      ) : (
        <div className="checkout__content">
          <Tabs
            activeKey={activeKey}
            onSelect={(key) => { setActiveKey(key); }}
            id="checkout-tab-mobile"
            className="checkout__tab-mobile"
          >
            <Tab
              eventKey="sign"
              title={(
                <p className="m-0 h-100 active client-primary-background-color" />
              )}
              disabled={loggedIn}
            >
              <div className="checkout__mt-50">
                {showSignup ? (
                  <>
                    <h4 className="checkout__mb-25 checkout__title">
                      {t('signup')}
                    </h4>
                    <SignupForm
                      primaryColor={primary}
                      isCheckoutPage
                      setShowSignup={setShowSignup}
                      checkEmail={checkEmail}
                      terms={terms}
                      onSubmit={({
                        email,
                        firstName,
                        lastName,
                        password,
                        agreeTerms,
                      }, { setSubmitting }) => {
                        createUser({
                          email,
                          plainPassword: password,
                          userProfile: {
                            firstName,
                            lastName,
                          },
                          termsVersion: agreeTerms && terms.version,
                          lang: currentLang,
                        })
                          .then(() => {
                            getToken({
                              email,
                              password,
                            })
                              .then(() => {
                                getOwnPaymentCards();
                                setActiveKey('delivery');
                              })
                              .catch(() => {
                                loginErrorToast();
                                setShowSignup(false);
                              });
                          })
                          .catch(() => {
                            signupErrorToast();
                            setSubmitting(false);
                          });
                      }}
                    />
                  </>
                ) : (
                  <>
                    <h4 className="mb-3 checkout__title">
                      {t('login')}
                    </h4>
                    <LoginForm
                      primaryColor={primary}
                      isCheckoutPage
                      lastUsedLogin={lastUsedLogin}
                      setShowSignup={setShowSignup}
                      onSubmit={(values, { resetForm }) => {
                        getToken(values)
                          .then(() => {
                            getOwnPaymentCards();
                            setActiveKey('delivery');
                          })
                          .catch(() => {
                            loginErrorToast();
                            resetForm({
                              values: {
                                ...values,
                                password: '',
                              },
                            });
                          });
                      }}
                    />
                  </>
                )}
              </div>
            </Tab>
            <Tab
              eventKey="delivery"
              title={(
                <p className={`m-0 h-100 ${['delivery', 'note', 'payment'].includes(activeKey) && 'active client-primary-background-color'}`} />
              )}
              disabled={!loggedIn}
            >
              <div className="my-3">
                {blockStepMethod === 1 && (
                  <>
                    <p>
                      <span className="checkout__title">
                        {t("chooseDeliveryMethod.mobile", { name: firstName })}
                      </span>
                      <Button
                        variant="link"
                        className="checkout__logout"
                        onClick={() => {
                          logout();
                        }}
                        onKeyPress={() => {
                          logout();
                        }}
                      >
                        {t("switchAccount")}
                      </Button>
                    </p>
                    <CheckoutDeliveryMethod
                      transactionTemplate={transactionTemplate}
                      deliveryMethod={deliveryMethod}
                      setDeliveryMethod={setDeliveryMethod}
                      setActiveKey={setActiveKey}
                      showBlockChoiceMethodRespon={showBlockChoiceMethodRespon}
                      blockStepMethod={blockStepMethod}
                      checkParcelDelivery={checkParcelDelivery}
                      onHandleOver={onHandleOver}
                    />
                  </>
                )}
                {blockStepMethod === 2 && (
                  <div className="block-method-delivery-respon">
                    <p className="title-main">Select your delivery method</p>
                    <div className="modal-add-payment">
                      <div className="form-content">
                        <span className="description-block">
                          Recipient’s delivery informations
                        </span>
                        <InfoAddressFormLimit
                          address={address}
                          handleSelectAddress={handleSelectAddress}
                          handleChangeAddress={handleChangeAddress}
                          onHandleRemoveInput={onHandleRemoveInput}
                        />
                      </div>
                    </div>
                    <span className="description-block">
                      Select your delivery method
                    </span>
                    <ListDeliveryMethod
                      onHandleConfirmMethod={onHandleConfirmMethod}
                    />
                  </div>
                )}
                {blockStepMethod === 3 && (
                  <div className="block-method-delivery-respon">
                    <p className="title-main">Select a pick-up location</p>
                    <span className="description-block">
                      Select your pick-up point
                    </span>
                    <Tabs
                      id="address-tab-mobi"
                      className="tab-list"
                    >
                      <Tab eventKey="map" title="Map">
                        <div className="tab-item">
                          <BlockMap lat={lat} lng={lng} loadMap={loadMap} />
                        </div>
                      </Tab>
                      <Tab eventKey="list-address-map" title="Pick-up point">
                        <div className="tab-item tab-item--last">
                          <BlockListAddress onHandleAccessConfirm={onHandleAccessConfirm} />
                        </div>
                      </Tab>
                    </Tabs>
                    <button
                      type="button"
                      class="btn parcel-delivery-modal__btn-confirm mt-3"
                      onClick={onHandleConfirmShippingInfo}
                      disabled={!accessConfirm ? true : false}
                    >
                      <span>Confirm</span>
                    </button>
                  </div>
                )}
              </div>
            </Tab>
            <Tab
              eventKey="note"
              title={(
                <p className={`m-0 h-100 ${['note', 'payment'].includes(activeKey) && 'active client-primary-background-color'}`} />
              )}
              disabled={!loggedIn}
            >
              <div className="my-3">
                <p>
                  <span className="checkout__title">
                    {t("checkoutNote.addNote")}
                  </span>
                </p>
                <CheckoutNote
                  transactionTemplate={transactionTemplate}
                  deliveryMethod={deliveryMethod}
                  setActiveKey={setActiveKey}
                />
              </div>
            </Tab>
            <Tab
              eventKey="payment"
              title={(
                <p className={`m-0 h-100 ${activeKey === 'payment' && 'active client-primary-background-color'}`} />
              )}
              disabled={!deliveryMethod}
            >
              <div className="my-3">
                <p>
                  <span className="checkout__title">
                    {t('choosePaymentMethod.mobile', { name: firstName })}
                  </span>
                  <Button
                    variant="link"
                    className="checkout__logout"
                    onClick={() => { logout(); }}
                    onKeyPress={() => { logout(); }}
                  >
                    {t('switchAccount')}
                  </Button>
                </p>
                <CheckoutPaymentForm
                  transactionTemplate={transactionTemplate}
                  transaction={transaction}
                  showPaymentErrorModal={showPaymentErrorModal}
                />
              </div>
            </Tab>
          </Tabs>
        </div>
      )}
      <Footer />
    </div>
  );
};

const mapStateToProps = (state) => ({
  loggedIn: state.persistent.authReducer.loggedIn,
  lastUsedLogin: state.persistent.authReducer.lastUsedLogin,
  me: state.persistent.meReducer.me,
  terms: state.persistent.termsReducer.terms,
});

const mapDispatchToProps = (dispatch) => ({
  getToken: bindActionCreators(getToken, dispatch),
  createUser: bindActionCreators(createUser, dispatch),
  checkEmail: bindActionCreators(checkEmail, dispatch),
  getOwnPaymentCards: bindActionCreators(getOwnPaymentCards, dispatch),
  logout: bindActionCreators(logout, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CheckoutMobile);
