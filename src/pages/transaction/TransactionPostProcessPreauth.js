import React, {
  useEffect,
  useState,
  useCallback,
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { useTranslation } from 'react-i18next';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { toast } from 'react-toastify';
import { useLoadingScreen } from 'helpers/LoadingScreenContext';
import queryString from 'query-string';
import useWindowSize from 'helpers/useWindowSize';
import CheckoutBreadcrumbBar from 'components/CheckoutBreadcrumbBar';
import CheckoutRecapDesktop from 'components/CheckoutRecapDesktop';
import CheckoutSuccess from 'components/CheckoutSuccess';
import CheckoutHeader from 'components/CheckoutHeader';
import Footer from 'components/Footer';
import {
  getTransaction,
  cancelTransaction,
  postProcessPreauth,
  fetchTransactionPicture,
} from 'logic/actions/transactionsActions';
import { fetchClientSquareLogo } from 'logic/actions/clientsActions';
import defaultTransactionImage from 'assets/images/defaultTransactionImage.svg';

const TransactionPostProcessPreauth = ({
  getTransaction,
  cancelTransaction,
  postProcessPreauth,
  fetchTransactionPicture,
  fetchClientSquareLogo,
  transactions,
  match,
  location,
  history,
}) => {
  const setLoadingScreen = useLoadingScreen();
  const { t } = useTranslation('checkout');
  const { transactionId } = match.params;
  const transaction = transactions[transactionId] || { id: transactionId };
  const { preAuthorizationId } = queryString.parse(location.search);
  const fetchTransactionErrorToast = useCallback(() => toast.error(t('fetchTransactionError')), [t]);
  const windowSize = useWindowSize();
  const [picture, setPicture] = useState();
  const [clientSquareLogo, setClientSquareLogo] = useState();

  const {
    client,
    adUrl,
    clientTheme: {
      primary,
    } = {},
  } = transaction;

  const loadPictures = useCallback(({
    id,
    hasPicture,
    clientId,
  }) => {
    const promises = [];
    promises.push(fetchClientSquareLogo(clientId)
      .then(({ payload: { data } = {} }) => {
        setClientSquareLogo(data);
      }));

    if (hasPicture) {
      promises.push(fetchTransactionPicture(id)
        .then(({ payload: { data } = {} }) => {
          setPicture(data);
        }));
    }

    Promise.allSettled(promises).then(() => {
      setLoadingScreen(false);
    });
  }, [fetchClientSquareLogo, fetchTransactionPicture, setLoadingScreen]);

  useEffect(() => {
    setLoadingScreen(true);
    getTransaction(transactionId)
      .then(({ payload: { data: transaction } = {} }) => {
        if (!transaction.isPreauthorized && preAuthorizationId) {
          postProcessPreauth(transactionId, parseInt(preAuthorizationId))
            .then(() => {
              loadPictures(transaction);
            })
            .catch(() => {
              cancelTransaction(transaction.id);
              const { pathname } = new URL(transaction.templatePermalink);
              history.push(pathname, { retryTransaction: transaction });
            });
        } else {
          loadPictures(transaction);
        }
      })
      .catch(() => {
        fetchTransactionErrorToast();
        setLoadingScreen(false);
      });
  }, [transactionId]);

  return (
    <>
      <Helmet>
        <title>{t('pageTitle')}</title>
        <style>
          {`
            .client-primary-color {
              color: ${primary} !important;
            }
            
            .client-primary-background-color {
              background-color: ${primary} !important;
            }
            
            .checkout-detail-image__inner {
              background-image: url(${picture || defaultTransactionImage});
            }
          `}
        </style>
      </Helmet>
      <div className="checkout-container">
        <CheckoutHeader primaryColor={primary} />
        <div className="checkout">
          {windowSize.isNarrow ? (
            <CheckoutSuccess
              client={client}
              adUrl={adUrl}
              primaryColor={primary}
            />
          ) : (
            <Row className="checkout__content">
              <Col lg="5">
                <CheckoutBreadcrumbBar
                  client={transaction.client}
                  adUrl={adUrl}
                  clientSquareLogo={clientSquareLogo}
                />
                <CheckoutRecapDesktop
                  transactionTemplate={transaction}
                  transaction={transaction}
                  picture={picture}
                  isPostProcess
                  noCounterOffer
                />
              </Col>
              <Col lg="2">
                <div
                  className="h-100 m-auto checkout__separator"
                />
              </Col>
              <Col lg="5">
                <CheckoutSuccess
                  client={client}
                  adUrl={adUrl}
                  primaryColor={primary}
                />
              </Col>
            </Row>
          )}
          <Footer />
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  me: state.persistent.meReducer.me,
  transactions: state.persistent.transactionsReducer.transactions,
});

const mapDispatchToProps = (dispatch) => ({
  getTransaction: bindActionCreators(getTransaction, dispatch),
  cancelTransaction: bindActionCreators(cancelTransaction, dispatch),
  postProcessPreauth: bindActionCreators(postProcessPreauth, dispatch),
  fetchTransactionPicture: bindActionCreators(fetchTransactionPicture, dispatch),
  fetchClientSquareLogo: bindActionCreators(fetchClientSquareLogo, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TransactionPostProcessPreauth);
