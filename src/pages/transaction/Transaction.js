import React, {
  useEffect,
  useState,
  useCallback,
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Helmet } from 'react-helmet';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Modal from 'react-bootstrap/Modal';
import Button from 'components/Button';
import {
  FormattedNumber,
  FormattedDate,
} from 'react-intl';

import { useLoadingScreen } from 'helpers/LoadingScreenContext';

import { toast } from 'react-toastify';
import TransactionActionPanel from 'components/TransactionActionPanel';
import TransactionTimeline from 'components/TransactionTimeline';
import {
  getTransaction,
  requestCancelTransaction,
  cancelTransaction,
  fetchTransactionPicture,
  fetchTransactionVerificationCode,
} from 'logic/actions/transactionsActions';
import Header from 'components/Header';

import iconArrowLeft from 'assets/images/iconmonstr-angel-left-thin.svg';
import defaultTransactionImage from 'assets/images/defaultTransactionImage.svg';
import 'assets/style/transaction.scss';

const EVENT_TRANSACTION_CANCEL_REQUESTED = 'event_transaction_cancel_requested';

const Transaction = ({
  getTransaction,
  requestCancelTransaction,
  cancelTransaction,
  fetchTransactionPicture,
  fetchTransactionVerificationCode,
  me,
  transactions,
  match,
  history,
}) => {
  const setLoadingScreen = useLoadingScreen();
  const { t } = useTranslation('transaction');
  const { transactionId } = match.params;
  const transaction = transactions[transactionId] || { id: transactionId };
  const [canceling, setCanceling] = useState(false);
  const [showConfirmCancelModal, setShowConfirmCancelModal] = useState(false);
  const [picture, setPicture] = useState(false);
  const fetchTransactionErrorToast = useCallback(() => toast.error(t('fetchTransactionError')), [t]);

  const {
    id,
    title,
    canBeCanceled,
    isAccepted,
    isRejected,
    isExpired,
    isCanceled,
    isCompleted,
    buyerId,
    buyerDisplayName,
    sellerDisplayName,
    sellerTotal,
    buyerTotal,
    client,
    currency = 'EUR',
    subTotal,
    buyerFees,
    sellerFees,
    events = [],
    adUrl,
    lastEventSlug,
  } = transaction;

  const isPurchase = me.id === buyerId;
  const ownFees = isPurchase ? buyerFees : sellerFees;
  const ownTotal = isPurchase ? buyerTotal : sellerTotal;
  const showCancelButton = !isRejected && !isCanceled && !isCompleted && !isExpired
    && !(isPurchase && lastEventSlug === EVENT_TRANSACTION_CANCEL_REQUESTED);

  useEffect(() => {
    setLoadingScreen(!title);

    getTransaction(transactionId)
      .then(({ payload: { data: transaction } = {} }) => {
        const promises = [];

        if (transaction.buyerId === me.id) {
          promises.push(fetchTransactionVerificationCode(transaction.id));
        }

        if (transaction.hasPicture) {
          promises.push(fetchTransactionPicture(id)
            .then(({ payload: { data } = {} }) => {
              setPicture(data);
            }));
        }

        Promise.allSettled(promises).then(() => {
          setLoadingScreen(false);
        });
      })
      .catch(() => {
        fetchTransactionErrorToast();
      });
  }, [transactionId]);

  const confirmCancelModal = () => {
    const modalName = isPurchase && isAccepted ? 'confirmRequestCancelModal' : 'confirmCancelModal';
    const action = isPurchase && isAccepted ? requestCancelTransaction : cancelTransaction;

    return (
      <Modal
        show={showConfirmCancelModal}
        onHide={() => { setShowConfirmCancelModal(false); }}
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>{t(`${modalName}.title`)}</Modal.Title>
        </Modal.Header>

        <Modal.Body className="keep-line-breaks">{t(`${modalName}.areYouSure`)}</Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={() => { setShowConfirmCancelModal(false); }}>
            {t(`${modalName}.close`)}
          </Button>
          <Button
            variant="danger"
            isLoading={canceling}
            disabled={!canBeCanceled}
            onClick={() => {
              setCanceling(true);
              action(id)
                .finally(() => {
                  setShowConfirmCancelModal(false);
                  setCanceling(false);
                });
            }}
          >
            {t(`${modalName}.confirm`)}
          </Button>
        </Modal.Footer>
      </Modal>
    );
  };

  return (
    <>
      <Helmet><title>{t('pageTitle', { id })}</title></Helmet>
      {confirmCancelModal()}
      <Header />
      <div className="w-100 transaction mb-3">
        <div className="justify-content-between mb-3 transaction__breadcrumb">
          <div
            onClick={() => history.push('/')}
            onKeyPress={() => history.push('/')}
            role="link"
            tabIndex={0}
          >
            <p
              className="m-0 d-flex align-items-center"
            >
              <img src={iconArrowLeft} alt="" width="12px" />
              <span className="pl-1">{t('goBack')}</span>
            </p>
          </div>
          {/* <p className="m-0 text-right help">Aide</p> */}
        </div>

        <TransactionActionPanel transaction={transaction} />

        <div className="transaction__reference transaction-reference transaction__mt-30 p-3 border rounded  d-lg-block d-none">
          <p className="text-secondary">{t('transactionRef', { id })}</p>
          <Row className="pb-3">
            <Col>
              <h3 className="m-0 transaction-reference__title">
                {title}
              </h3>
            </Col>
            <Col>
              <h3 className="m-0 text-right transaction-reference__price">
                <FormattedNumber value={(isPurchase ? buyerTotal : sellerTotal) / 100} style="currency" currency={currency} />
              </h3>
            </Col>
          </Row>

          <TransactionTimeline transaction={transaction} />
        </div>

        <Row className="transaction__mt-30">
          <Col sm={12} lg={4} className="mb-3 mt-lg-0 d-lg-none">
            <div className="transaction-image">
              <div
                className="transaction-image__inner"
                style={{ backgroundImage: `url(${picture || defaultTransactionImage})` }}
              />
            </div>
          </Col>
          <Col sm={12} lg={8}>
            <div className="transaction-detail p-3 border rounded">
              <h5 className="mb-2 transaction-detail__title">{t('details.title')}</h5>
              <div className="transaction-detail__list">
                {ownFees !== 0 && (
                  <>
                    <div className="d-flex justify-content-between pb-3 pt-3 border-bottom">
                      <p className="m-0">
                        {t('details.amount')}
                        :
                      </p>
                      <p className="m-0 text-right"><FormattedNumber value={subTotal / 100} style="currency" currency={currency} /></p>
                    </div>

                    <div className="d-flex justify-content-between pb-3 pt-3 border-bottom">
                      <p className="m-0">
                        {t('details.fees')}
                        :
                      </p>
                      <p className="m-0 text-right"><FormattedNumber value={ownFees / 100} style="currency" currency={currency} /></p>
                    </div>
                  </>
                )}

                <div className="d-flex justify-content-between pb-3 pt-3 border-bottom">
                  <p className="m-0">
                    {t('details.total')}
                    :
                  </p>
                  <p className="m-0 text-right"><FormattedNumber value={ownTotal / 100} style="currency" currency={currency} /></p>
                </div>

                <div className="d-flex justify-content-between pb-3 pt-3 border-bottom">
                  <p className="m-0">
                    {t('details.deliveryMode')}
                    :
                  </p>
                  <p className="m-0 text-right">{t('details.handover')}</p>
                </div>

                <div className="d-flex justify-content-between pb-3 pt-3 border-bottom">
                  <p className="m-0">
                    {t('details.sentOn')}
                    :
                  </p>
                  <p className="m-0 text-right"><FormattedDate value={new Date(events[0]?.date)} /></p>
                </div>

                {isPurchase ? (
                  <div className="d-flex justify-content-between pb-3 pt-3 border-bottom">
                    <p className="m-0">
                      {t('details.seller')}
                      :
                    </p>
                    <p className="m-0 text-right">{sellerDisplayName}</p>
                  </div>
                ) : (
                  <div className="d-flex justify-content-between pb-3 pt-3 border-bottom">
                    <p className="m-0">
                      {t('details.buyer')}
                      :
                    </p>
                    <p className="m-0 text-right">{buyerDisplayName}</p>
                  </div>
                )}

                <div className="d-flex justify-content-between pb-3 pt-3">
                  <p className="m-0">
                    {t('details.client')}
                    :
                  </p>
                  <p className="m-0 text-right">
                    {client}
                    <br />
                    <small><a href={adUrl}>{t('details.adUrl')}</a></small>
                  </p>
                </div>
              </div>
            </div>
          </Col>
          <Col sm={12} lg={4} className="mt-3 mt-lg-0">
            {/* <div className="transaction-sold-by p-3 border rounded">
              <h5 className="mb-2 transaction-sold-by__title">
                {t('soldBy')} :
              </h5>
              <div className="d-flex">
                <div>
                <p className="transaction-sold-by__name">{isPurchase ? sellerDisplayName : buyerDisplayName}</p>
                <p className="m-0 transaction-sold-by__tag">
                    <span />
                    <span>PROFIL CERTIFIE</span>
                  </p>
                </div>
              </div>
            </div> */}

            <div className="transaction-image d-none d-lg-block">
              <div
                className="transaction-image__inner"
                style={{ backgroundImage: `url(${picture || defaultTransactionImage})` }}
              />
            </div>
          </Col>
        </Row>

        {showCancelButton && (
          <div className="mt-3">
            <Button
              variant="danger"
              className="cancelBtn"
              disabled={!canBeCanceled}
              onClick={() => { setShowConfirmCancelModal(true); }}
            >
              {isPurchase && isAccepted ? t('requestCancel') : t('cancel')}
            </Button>
          </div>
        )}
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  me: state.persistent.meReducer.me,
  transactions: state.persistent.transactionsReducer.transactions,
});

const mapDispatchToProps = (dispatch) => ({
  getTransaction: bindActionCreators(getTransaction, dispatch),
  requestCancelTransaction: bindActionCreators(requestCancelTransaction, dispatch),
  cancelTransaction: bindActionCreators(cancelTransaction, dispatch),
  fetchTransactionPicture: bindActionCreators(fetchTransactionPicture, dispatch),
  fetchTransactionVerificationCode: bindActionCreators(fetchTransactionVerificationCode, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Transaction);
