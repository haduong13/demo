import React, {
  useEffect, useState,
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Helmet } from 'react-helmet';
import { useLoadingScreen } from 'helpers/LoadingScreenContext';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';

import TransactionListItem from 'components/TransactionListItem';
import Header from 'components/Header';

import { refreshMe } from 'logic/actions/meActions';
import {
  getOwnActiveTransactions,
  getOwnEndedTransactions,
} from 'logic/actions/transactionsActions';
import 'assets/style/home.scss';

const Home = ({
  refreshMe,
  getOwnActiveTransactions,
  getOwnEndedTransactions,
  activeTransactions: {
    pages: activeTransactionsPages = [],
  } = {},
  endedTransactions: {
    pages: endedTransactionsPages = [],
  } = {},
}) => {
  const { t } = useTranslation('home');
  const setLoadingScreen = useLoadingScreen();
  const [key, setKey] = useState('inProgress');
  const [currentPageActive, setCurrentPageActive] = useState(1); // eslint-disable-line no-unused-vars
  const [currentPageEnded, setCurrentPageEnded] = useState(1); // eslint-disable-line no-unused-vars

  const currentActiveTransactions = activeTransactionsPages[currentPageActive] || [];
  const currentEndedTransactions = endedTransactionsPages[currentPageActive] || [];

  useEffect(() => {
    refreshMe();
  }, []);

  useEffect(() => {
    setLoadingScreen(!currentActiveTransactions.length);
    Promise.allSettled([
      getOwnActiveTransactions(currentPageActive),
      getOwnEndedTransactions(currentPageActive),
    ]).then(() => {
      setLoadingScreen(false);
    });
  }, [
    currentPageActive,
  ]);

  return (
    <>
      <Helmet><title>{t('pageTitle')}</title></Helmet>
      <Header />
      <div className="w-100 home">
        <h2 className="home__title">{t('title')}11</h2>
        <Tabs
          activeKey={key}
          onSelect={(k) => setKey(k)}
          className="home-tab mb-5 mt-3"
        >
          <Tab eventKey="inProgress" title={t('inProgress')}>
            <div>
              {currentActiveTransactions.map((transaction) => (
                <TransactionListItem transaction={transaction} key={transaction.id} />
              ))}
            </div>
          </Tab>
          <Tab eventKey="finished" title={t('finished')}>
            <div>
              {currentEndedTransactions.map((transaction) => (
                <TransactionListItem transaction={transaction} key={transaction.id} />
              ))}
            </div>
          </Tab>
        </Tabs>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  me: state.persistent.meReducer.me,
  activeTransactions: state.persistent.transactionsReducer.activeTransactions,
  endedTransactions: state.persistent.transactionsReducer.endedTransactions,
  nbPages: state.persistent.transactionsReducer.activeTransactions.nbPages,
});

const mapDispatchToProps = (dispatch) => ({
  refreshMe: bindActionCreators(refreshMe, dispatch),
  getOwnActiveTransactions: bindActionCreators(getOwnActiveTransactions, dispatch),
  getOwnEndedTransactions: bindActionCreators(getOwnEndedTransactions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
