import React, {
  useEffect,
  useCallback,
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';
import { Redirect } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { getToken } from 'logic/actions/authActions';
import { getTerms } from 'logic/actions/termsActions';
import {
  createUser,
  checkEmail,
} from 'logic/actions/usersActions';
import SignupForm from 'components/SignupForm';
import Footer from 'components/Footer';
import logo from 'assets/images/logo.svg';
import 'assets/style/login-register.scss';

const Signup = ({
  getToken,
  createUser,
  checkEmail,
  getTerms,
  loggedIn,
  terms = {},
  location,
  history,
}) => {
  const { t, i18n } = useTranslation('signup');
  const currentLang = i18n.language?.replace(/^([a-z]+)-?.*/, '$1') || 'en';
  const signupErrorToast = useCallback(() => toast.error(t('form.signupError')), [t]);
  const loginErrorToast = useCallback(() => toast.error(t('form.loginError')), [t]);
  const referer = (location.state && location.state.referer) || '/';

  useEffect(() => {
    getTerms(currentLang);
  }, [currentLang]);

  if (loggedIn) {
    return <Redirect to={referer} />;
  }

  return (
    <>
      <Helmet><title>{t('pageTitle')}</title></Helmet>
      <div className="login-container">
        <div className="form-login-main">
          <div className="form-login-main__content">
            <img src={logo} alt="" />
            <p className="title-main">{t('title')}</p>
            <SignupForm
              onSubmit={(
                {
                  email, firstName, lastName, password, agreeTerms,
                },
                { setSubmitting },
              ) => {
                createUser({
                  email,
                  plainPassword: password,
                  userProfile: {
                    firstName,
                    lastName,
                  },
                  termsVersion: agreeTerms && terms.version,
                  lang: currentLang,
                })
                  .then(() => {
                    getToken({
                      email, password,
                    }).catch(() => {
                      loginErrorToast();
                      history.push('/login', { referer });
                    });
                  })
                  .catch(() => {
                    signupErrorToast();
                    setSubmitting(false);
                  });
              }}
              checkEmail={checkEmail}
              terms={terms}
            />
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  loggedIn: state.persistent.authReducer.loggedIn,
  terms: state.persistent.termsReducer.terms,
});

const mapDispatchToProps = (dispatch) => ({
  getToken: bindActionCreators(getToken, dispatch),
  createUser: bindActionCreators(createUser, dispatch),
  checkEmail: bindActionCreators(checkEmail, dispatch),
  getTerms: bindActionCreators(getTerms, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
