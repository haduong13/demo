import React, { useEffect } from 'react';
import {
  NavLink, Route, Switch,
} from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { useTranslation } from 'react-i18next';
import { Helmet } from 'react-helmet';
import Profile from 'pages/settings/Profile';
import Wallets from 'pages/settings/Wallets';
import Payments from 'pages/settings/Payments';
import ProfileVerification from 'pages/settings/ProfileVerification';
import { refreshMe } from 'logic/actions/meActions';
import iconWallet from 'assets/images/icon-wallet.svg';
import iconPersonalInfo from 'assets/images/icon-personal-information.svg';
import iconPayment from 'assets/images/icon-payment.svg';
import iconProfile from 'assets/images/icon-verified-profile.svg';
import Header from 'components/Header';

const Settings = ({
  match: {
    path,
    url,
  }, refreshMe,
}) => {
  const { t } = useTranslation('settings');

  useEffect(() => {
    refreshMe();
  }, []);

  return (
    <>
      <Helmet><title>{t('pageTitle')}</title></Helmet>
      <Header />
      <Row className="d-flex block-settings margin-header">
        <Col md={4}>
          <ul className="list-menu-bar">
            <li>
              <NavLink to={`${url}/wallets`}>
                <span className="text-list">
                  <img className="icon-list" src={iconWallet} alt="" />
                  {t('links.wallets')}
                </span>
              </NavLink>
            </li>
            <li>
              <NavLink
                to={`${url}/profile`}
                isActive={(match, location) => ([`${url}`, `${url}/profile`].includes(location.pathname))}
              >
                <span className="text-list">
                  <img className="icon-list" src={iconPersonalInfo} alt="" />
                  {t('links.profile')}
                </span>
              </NavLink>
            </li>
            <li>
              <NavLink to={`${url}/payments`}>
                <span className="text-list">
                  <img className="icon-list" src={iconPayment} alt="" />
                  {t('links.payments')}
                </span>
              </NavLink>
            </li>
            <li>
              <NavLink to={`${url}/profile-verification`}>
                <span className="text-list">
                  <img className="icon-list" src={iconProfile} alt="" />
                  {t('links.profileVerification')}
                </span>
              </NavLink>
            </li>
          </ul>
        </Col>
        <Col md={8}>
          <Switch>
            <Route exact path={`${path}/wallets`} component={Wallets} />
            <Route exact path={[`${path}`, `${path}/profile`]} component={Profile} />
            <Route exact path={`${path}/payments`} component={Payments} />
            <Route exact path={`${path}/profile-verification`} component={ProfileVerification} />
          </Switch>
        </Col>
      </Row>
    </>
  );
};

const mapStateToProps = (state) => ({ me: state.persistent.meReducer.me });

const mapDispatchToProps = (dispatch) => ({ refreshMe: bindActionCreators(refreshMe, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
