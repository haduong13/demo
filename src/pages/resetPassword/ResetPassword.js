import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { Formik } from 'formik';
import * as yup from 'yup';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import Form from 'react-bootstrap/Form';
import Button from 'components/Button';
import { toast } from 'react-toastify';
import { Redirect } from 'react-router-dom';
import Footer from 'components/Footer';
import logo from 'assets/images/logo.svg';

import { resetPassword } from 'logic/actions/resetPasswordActions';

const ResetPassword = ({
  loggedIn,
  resetPassword,
  match,
  history,
  location,
}) => {
  const { t } = useTranslation('resetPassword');
  const { token } = match.params;
  const resetPasswordErrorToast = useCallback(() => toast.error(t('resetPasswordError')), [t]);
  const resetPasswordSuccessToast = useCallback(() => toast.success(t('resetPasswordSuccess')), [t]);
  const referer = (location.state && location.state.referer) || '/';

  if (loggedIn) {
    return <Redirect to={referer} />;
  }

  return (
    <>
      <Helmet><title>{t('pageTitle')}</title></Helmet>
      <div className="login-container">
        <div className="form-login-main">
          <div className="form-login-main__content">
            <img src={logo} alt="" />
            <p className="title-main">{t('title')}</p>
            <span className="description">{t('description')}</span>
            <Formik
              validateOnChange={false}
              validateOnBlur={false}
              validationSchema={yup.object({
                password: yup.string().required(t('emptyPassword')).min(10, t('invalidPassword')),
                confirmPassword: yup.string().required(t('emptyConfirmPassword')).oneOf([yup.ref('password')], t('passwordsDontMatch')),
              })}
              onSubmit={({ password }, { resetForm }) => {
                resetPassword(token, password)
                  .then(() => {
                    resetPasswordSuccessToast();
                    history.push('/login');
                  })
                  .catch(() => {
                    resetPasswordErrorToast();
                    resetForm();
                  });
              }}
              initialValues={{
                password: '',
                confirmPassword: '',
              }}
            >
              {({
                handleSubmit,
                handleChange,
                handleBlur,
                values,
                touched,
                errors,
                isSubmitting,
              }) => (
                <Form className="form-login" noValidate onSubmit={handleSubmit}>
                  <Form.Row>
                    <Form.Group
                      md="4"
                      controlId="password"
                      className="form-group-border-top"
                    >
                      <Form.Control
                        type="password"
                        name="password"
                        value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isInvalid={touched.password && !!errors.password}
                        placeholder={t('password')}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.password}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Group
                      md="4"
                      controlId="confirmPassword"
                      className="form-group-border-bottom"
                    >
                      <Form.Control
                        type="password"
                        name="confirmPassword"
                        value={values.confirmPassword}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isInvalid={touched.confirmPassword && !!errors.confirmPassword}
                        placeholder={t('confirmPassword')}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.confirmPassword}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Button
                      type="submit"
                      disabled={!values.password || !values.confirmPassword}
                      className="btn-block mt-4"
                      isLoading={isSubmitting}
                    >
                      {t('submit')}
                    </Button>
                  </Form.Row>
                </Form>
              )}
            </Formik>
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({ loggedIn: state.persistent.authReducer.loggedIn });

const mapDispatchToProps = (dispatch) => ({ resetPassword: bindActionCreators(resetPassword, dispatch) });

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ResetPassword);
