import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { Formik } from 'formik';
import * as yup from 'yup';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import Form from 'react-bootstrap/Form';
import Button from 'components/Button';
import { toast } from 'react-toastify';
import { Redirect } from 'react-router-dom';
import Footer from 'components/Footer';
import logo from 'assets/images/logo.svg';

import { requestResetPassword } from 'logic/actions/resetPasswordActions';

const RequestResetPassword = ({
  loggedIn,
  requestResetPassword,
  history,
  location,
}) => {
  const { t } = useTranslation('requestResetPassword');
  const submittedToast = useCallback(() => toast.success(t('submitted')), [t]);
  const errorToast = useCallback(() => toast.error(t('error')), [t]);
  const referer = (location.state && location.state.referer) || '/';

  if (loggedIn) {
    return <Redirect to={referer} />;
  }

  return (
    <>
      <Helmet><title>{t('pageTitle')}</title></Helmet>
      <div className="login-container">
        <div className="form-login-main">
          <div className="form-login-main__content">
            <img src={logo} alt="" />
            <p className="title-main">{t('title')}</p>
            <span className="description">{t('description')}</span>
            <Formik
              validateOnChange={false}
              validateOnBlur={false}
              validationSchema={yup.object({ email: yup.string().required(t('emptyEmail')).email(t('invalidEmail')) })}
              onSubmit={({ email }, { setSubmitting }) => {
                requestResetPassword(email)
                  .then(() => {
                    submittedToast();
                    history.goBack();
                  })
                  .catch(() => {
                    errorToast();
                    setSubmitting(false);
                  });
              }}
              initialValues={{ email: '' }}
            >
              {({
                handleSubmit,
                handleChange,
                handleBlur,
                values,
                touched,
                errors,
                isSubmitting,
              }) => (
                <Form className="form-login" noValidate onSubmit={handleSubmit}>
                  <Form.Row>
                    <Form.Group md="4" controlId="email">
                      <Form.Control
                        type="email"
                        name="email"
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isInvalid={touched.email && !!errors.email}
                        placeholder={t('email')}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.email}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Button
                      type="submit"
                      disabled={!values.email}
                      className="btn-block mt-4"
                      isLoading={isSubmitting}
                    >
                      {t('submit')}
                    </Button>
                  </Form.Row>
                </Form>
              )}
            </Formik>
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({ loggedIn: state.persistent.authReducer.loggedIn });

const mapDispatchToProps = (dispatch) => ({ requestResetPassword: bindActionCreators(requestResetPassword, dispatch) });

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RequestResetPassword);
