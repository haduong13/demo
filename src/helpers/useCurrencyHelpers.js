import { useTranslation } from 'react-i18next';

const useCurrencyHelpers = (currency) => {
  const { i18n } = useTranslation();

  const parts = new Intl.NumberFormat(i18n.language, {
    style: 'currency',
    currency,
  }).formatToParts(1);

  let symbol = '?';
  const before = parts.findIndex((item) => {
    if (item.type === 'currency') {
      symbol = item.value;
      return true;
    }
    return false;
  }) === 0;

  return {
    symbol,
    before,
  };
};

export default useCurrencyHelpers;
