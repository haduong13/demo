const {
  NODE_ENV,
  REACT_APP_ENV_NAME,
  REACT_APP_API_URL,
  REACT_APP_CLIENT_ID,
  REACT_APP_CLIENT_SECRET,
  REACT_APP_SENTRY_DSN,
  REACT_APP_DEBUG,
  REACT_APP_HASH,
} = process.env;

const isProd = NODE_ENV === 'production';
const envName = REACT_APP_ENV_NAME;
const apiUrl = REACT_APP_API_URL;
const clientId = REACT_APP_CLIENT_ID;
const clientSecret = REACT_APP_CLIENT_SECRET;
const sentryDSN = REACT_APP_SENTRY_DSN || 'https =//c035321391594731aae9e2fd3f0618e2@o204930.ingest.sentry.io/5653358';
const debug = REACT_APP_DEBUG === 'true';
const hash = REACT_APP_HASH || '';

export {
  isProd,
  envName,
  apiUrl,
  clientId,
  clientSecret,
  sentryDSN,
  debug,
  hash,
};
