import _ from 'lodash';

export const removeAccents = (str) => (
  str
    .replace(/[áàãâä]/gi, 'a')
    .replace(/[éè¨ê]/gi, 'e')
    .replace(/[íìïî]/gi, 'i')
    .replace(/[óòöôõ]/gi, 'o')
    .replace(/[úùüû]/gi, 'u')
    .replace(/[ç]/gi, 'c')
    .replace(/[ñ]/gi, 'n')
    .replace(/[^a-zA-Z0-9]/g, ' ')
);

export const sort = (array, fn) => (
  _.sortBy(array, (item) => (removeAccents(fn(item)).toLowerCase()))
);
