import authReducer from 'logic/reducers/authReducer';
import meReducer from 'logic/reducers/meReducer';
import countriesReducer from 'logic/reducers/countriesReducer';
import currenciesReducer from 'logic/reducers/currenciesReducer';
import termsReducer from 'logic/reducers/termsReducer';
import transactionsReducer from 'logic/reducers/transactionsReducer';
import walletsReducer from 'logic/reducers/walletsReducer';
import bankAccountsReducer from 'logic/reducers/bankAccountsReducer';
import paymentCardsReducer from 'logic/reducers/paymentCardsReducer';

export const persistentReducers = {
  authReducer,
  meReducer,
  countriesReducer,
  currenciesReducer,
  termsReducer,
  transactionsReducer,
  walletsReducer,
  bankAccountsReducer,
  paymentCardsReducer,
};

export const volatileReducers = {
};
