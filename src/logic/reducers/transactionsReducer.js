import { createReducer } from '@reduxjs/toolkit';

import {
  success,
  shouldResetState,
} from 'logic/reducers/helpers';

import {
  GET_OWN_ACTIVE_TRANSACTIONS,
  GET_OWN_ENDED_TRANSACTIONS,
  GET_TRANSACTION,
  POST_PROCESS_PREAUTH,
  ACCEPT_TRANSACTION,
  REJECT_TRANSACTION,
  COMPLETE_TRANSACTION,
  REQUEST_CANCEL_TRANSACTION,
  REJECT_CANCEL_TRANSACTION,
  CANCEL_TRANSACTION,
  EVALUATE_TRANSACTION,
  FETCH_TRANSACTION_VERIFICATION_CODE,
} from 'logic/actions/transactionsActions';

const initialState = {
  activeTransactions: {
    pages: [],
    nbPages: 1,
  },
  endedTransactions: {
    pages: [],
    nbPages: 1,
  },
  transactions: [],
};

const handleGetOwnActiveTransactions = (
  state,
  {
    payload: {
      data: {
        page,
        nbPages = 1,
        items = [],
      } = {},
    } = {},
  },
) => {
  const { activeTransactions } = state;
  if (page) {
    activeTransactions.pages[page] = items;
  }
  activeTransactions.nbPages = nbPages;
};

const handleGetOwnEndedTransactions = (
  state,
  {
    payload: {
      data: {
        page,
        nbPages = 1,
        items = [],
      } = {},
    } = {},
  },
) => {
  const { endedTransactions } = state;
  if (page) {
    endedTransactions.pages[page] = items;
  }
  endedTransactions.nbPages = nbPages;
};

const handleGetTransaction = (
  state,
  {
    payload: { data = {} } = {},
    meta: { previousAction: { transactionId = 0 } = {} } = {},
  },
) => {
  const { transactions } = state;
  if (transactionId) {
    transactions[transactionId] = {
      ...transactions[transactionId], ...data,
    };
  }
};

export default createReducer(initialState, {
  [success(GET_OWN_ACTIVE_TRANSACTIONS)]: handleGetOwnActiveTransactions,
  [success(GET_OWN_ENDED_TRANSACTIONS)]: handleGetOwnEndedTransactions,
  [success(GET_TRANSACTION)]: handleGetTransaction,
  [success(POST_PROCESS_PREAUTH)]: handleGetTransaction,
  [success(ACCEPT_TRANSACTION)]: handleGetTransaction,
  [success(REJECT_TRANSACTION)]: handleGetTransaction,
  [success(COMPLETE_TRANSACTION)]: handleGetTransaction,
  [success(REQUEST_CANCEL_TRANSACTION)]: handleGetTransaction,
  [success(REJECT_CANCEL_TRANSACTION)]: handleGetTransaction,
  [success(CANCEL_TRANSACTION)]: handleGetTransaction,
  [success(EVALUATE_TRANSACTION)]: handleGetTransaction,
  [success(FETCH_TRANSACTION_VERIFICATION_CODE)]: handleGetTransaction,
}, [{
  matcher: (action) => (shouldResetState(action)),
  reducer: () => (initialState),
}]);
