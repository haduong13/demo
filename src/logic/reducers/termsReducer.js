import { createReducer } from '@reduxjs/toolkit';

import {
  success,
} from 'logic/reducers/helpers';

import {
  GET_TERMS,
} from 'logic/actions/termsActions';

const initialState = { terms: {} };

const handleGetTerms = (
  state,
  { payload: { data: terms = {} } = {} },
) => {
  state.terms = terms;
};

export default createReducer(initialState, { [success(GET_TERMS)]: handleGetTerms });
