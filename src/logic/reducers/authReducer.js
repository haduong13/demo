import { createReducer } from '@reduxjs/toolkit';

import {
  success,
  shouldResetState,
} from 'logic/reducers/helpers';

import {
  GET_TOKEN,
  REFRESH_TOKEN,
} from 'logic/actions/authActions';

const initialState = {
  accessToken: '',
  refreshToken: '',
  lastUsedLogin: '',
  loggedIn: false,
};

const handleToken = (
  state,
  {
    meta: { previousAction: { payload: { request: { data: { username: lastUsedLogin } = {} } = {} } = {} } = {} },
    payload: {
      data: {
        accessToken,
        refreshToken,
      } = {},
    } = {},
  },
) => ({
  accessToken,
  refreshToken,
  lastUsedLogin,
  loggedIn: true,
});

export default createReducer(initialState, {
  [success(GET_TOKEN)]: handleToken,
  [success(REFRESH_TOKEN)]: handleToken,
}, [{
  matcher: ({ type }) => (shouldResetState({ type }) && type !== success(GET_TOKEN)),
  reducer: (state) => ({
    ...initialState, lastUsedLogin: state.lastUsedLogin,
  }),
}]);
