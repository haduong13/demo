import * as Sentry from '@sentry/react';
import { createReducer } from '@reduxjs/toolkit';

import {
  success,
  shouldResetState,
} from 'logic/reducers/helpers';

import {
  REFRESH_ME,
  UPDATE_ME,
} from 'logic/actions/meActions';

const initialState = { me: {} };

const handleMe = (
  state,
  { payload: { data = {} } = {} },
) => {
  const email = data?.email;

  if (email) {
    state.me = data;
    Sentry.setUser({ email });
  }
};

export default createReducer(initialState, {
  [success(REFRESH_ME)]: handleMe,
  [success(UPDATE_ME)]: handleMe,
}, [{
  matcher: (action) => (shouldResetState(action)),
  reducer: () => {
    Sentry.setUser({ email: 'Not logged in' });
    return initialState;
  },
}]);
