import { createReducer } from '@reduxjs/toolkit';

import {
  success,
  shouldResetState,
} from 'logic/reducers/helpers';

import {
  GET_OWN_BANK_ACCOUNTS,
} from 'logic/actions/bankAccountsActions';

const initialState = { bankAccounts: [] };

const handleGetOwnBankAccounts = (
  state,
  { payload: { data = [] } = {} },
) => {
  state.bankAccounts = data;
};

export default createReducer(initialState, { [success(GET_OWN_BANK_ACCOUNTS)]: handleGetOwnBankAccounts }, [{
  matcher: (action) => (shouldResetState(action)),
  reducer: () => (initialState),
}]);
