/* eslint-disable import/no-unused-modules */
export const REFRESH_ME = 'me/REFRESH_ME';
export const UPDATE_ME = 'me/UPDATE_ME';
export const UPDATE_MY_PASSWORD = 'me/UPDATE_MY_PASSWORD';
export const DELETE_MY_ACCOUNT = 'me/DELETE_MY_ACCOUNT';
/* eslint-enable import/no-unused-modules */

export const refreshMe = () => ({
  type: REFRESH_ME,
  payload: {
    request: {
      method: 'GET',
      url: '/api/webapp/users/me/',
    },
  },
});

export const updateMe = (data) => ({
  type: UPDATE_ME,
  payload: {
    request: {
      method: 'PATCH',
      url: '/api/webapp/users/me/',
      data,
    },
  },
});

export const updateMyPassword = (data) => ({
  type: UPDATE_MY_PASSWORD,
  payload: {
    request: {
      method: 'PATCH',
      url: '/api/webapp/users/change-password/',
      data,
    },
  },
});

export const deleteMyAccount = () => ({
  type: DELETE_MY_ACCOUNT,
  payload: {
    request: {
      method: 'DELETE',
      url: '/api/webapp/users/me/',
    },
  },
});
