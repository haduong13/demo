/* eslint-disable import/no-unused-modules */
export const GET_COUNTRIES = 'countries/GET_COUNTRIES';
/* eslint-enable import/no-unused-modules */

export const getCountries = () => ({
  type: GET_COUNTRIES,
  payload: {
    request: {
      method: 'GET',
      url: '/api/webapp/countries/',
    },
  },
});
