/* eslint-disable import/no-unused-modules */
export const FETCH_CLIENT_SQUARE_LOGO = 'clients/FETCH_CLIENT_SQUARE_LOGO';
/* eslint-enable import/no-unused-modules */

export const fetchClientSquareLogo = (clientId) => ({
  type: FETCH_CLIENT_SQUARE_LOGO,
  payload: {
    request: {
      method: 'GET',
      url: `/api/webapp/clients/${clientId}/square-logo/`,
    },
  },
});
