import queryString from 'query-string';

/* eslint-disable import/no-unused-modules */
export const GET_OWN_PAYMENT_CARDS = 'paymentCards/GET_OWN_PAYMENT_CARDS';
export const INIT_TOKENIZATION = 'paymentCards/INIT_TOKENIZATION';
export const POST_TOKENIZATION = 'paymentCards/POST_TOKENIZATION';
export const COMPLETE_TOKENIZATION = 'paymentCards/COMPLETE_TOKENIZATION';
export const DELETE_CARD = 'paymentCards/DELETE_CARD';
/* eslint-enable import/no-unused-modules */

const CB_VISA_MASTERCARD = 'payment_method_type_cb_visa_mastercard';

export const getOwnPaymentCards = () => ({
  type: GET_OWN_PAYMENT_CARDS,
  payload: {
    request: {
      method: 'GET',
      url: '/api/webapp/payment-cards/',
    },
  },
});

const initTokenization = (data) => ({
  type: INIT_TOKENIZATION,
  payload: {
    request: {
      method: 'POST',
      url: '/api/webapp/payment-cards/tokenization/',
      data,
    },
  },
});

const postTokenization = (url, data) => ({
  type: POST_TOKENIZATION,
  noAuth: true,
  payload: {
    request: {
      method: 'POST',
      url,
      data: queryString.stringify(data),
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      responseType: 'text',
    },
  },
});

const completeTokenization = (id, data) => ({
  type: COMPLETE_TOKENIZATION,
  payload: {
    request: {
      method: 'PATCH',
      url: `/api/webapp/payment-cards/tokenization/${id}/`,
      data,
    },
  },
});

export const tokenizeCard = ({
  currency,
  cardNumber,
  cardExpirationDate,
  cardCvx,
}) => (dispatch) => new Promise((resolve, reject) => {
  dispatch(initTokenization({
    currency,
    paymentMethodType: CB_VISA_MASTERCARD,
  }))
    .then(({
      payload: {
        data: {
          id,
          cardRegistrationURL,
          data,
          accessKeyRef,
        } = {},
      } = {},
    }) => {
      dispatch(postTokenization(cardRegistrationURL, {
        accessKeyRef,
        data,
        cardNumber,
        cardExpirationDate,
        cardCvx,
      }))
        .then(({ payload: { data: postTokenizationData } = {} }) => {
          dispatch(completeTokenization(id, { postTokenizationData }))
            .then(({ payload: { data } = {} }) => {
              resolve(data);
            })
            .catch((err) => {
              reject(err);
            });
        }).catch((err) => {
          reject(err);
        });
    })
    .catch((err) => {
      reject(err);
    });
});

export const deleteCard = (cardId) => ({
  type: DELETE_CARD,
  payload: {
    request: {
      method: 'DELETE',
      url: `/api/webapp/payment-cards/${cardId}/`,
    },
  },
});
