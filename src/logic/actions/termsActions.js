/* eslint-disable import/no-unused-modules */
export const GET_TERMS = 'terms/GET_TERMS';
/* eslint-enable import/no-unused-modules */

export const getTerms = (lang = 'en') => ({
  type: GET_TERMS,
  payload: {
    request: {
      method: 'GET',
      url: '/api/webapp/terms/',
      params: { lang },
    },
  },
});
