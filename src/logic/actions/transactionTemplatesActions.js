/* eslint-disable import/no-unused-modules */
export const REFRESH_TRANSACTION_TEMPLATE = 'transactionTemplates/REFRESH_TRANSACTION_TEMPLATE';
export const FETCH_TRANSACTION_TEMPLATE_PICTURE = 'transactionTemplates/FETCH_TRANSACTION_TEMPLATE_PICTURE';
export const DRY_RUN_TRANSACTION_FROM_TEMPLATE = 'transactionTemplates/DRY_RUN_TRANSACTION_FROM_TEMPLATE';
export const CREATE_TRANSACTION_FROM_TEMPLATE = 'transactionTemplates/CREATE_TRANSACTION_FROM_TEMPLATE';
/* eslint-enable import/no-unused-modules */

export const refreshTransactionTemplate = (transactionTemplateId) => ({
  type: REFRESH_TRANSACTION_TEMPLATE,
  transactionTemplateId,
  payload: {
    request: {
      method: 'GET',
      url: `/api/webapp/transaction-templates/${transactionTemplateId}/`,
    },
  },
});

export const fetchTransactionTemplatePicture = (transactionTemplateId) => ({
  type: FETCH_TRANSACTION_TEMPLATE_PICTURE,
  payload: {
    request: {
      method: 'GET',
      url: `/api/webapp/transaction-templates/${transactionTemplateId}/picture/`,
    },
  },
});

export const dryRunTransactionFromTemplate = (transactionTemplateId, data = {}) => ({
  type: DRY_RUN_TRANSACTION_FROM_TEMPLATE,
  transactionTemplateId,
  payload: {
    request: {
      method: 'POST',
      url: `/api/webapp/transaction-templates/${transactionTemplateId}/transactions/?dry-run`,
      data,
    },
  },
});

export const createTransactionFromTemplate = (transactionTemplateId, data = {}) => ({
  type: CREATE_TRANSACTION_FROM_TEMPLATE,
  transactionTemplateId,
  payload: {
    request: {
      method: 'POST',
      url: `/api/webapp/transaction-templates/${transactionTemplateId}/transactions/`,
      data,
    },
  },
});
