/* eslint-disable import/no-unused-modules */
export const REQUEST_RESET_PASSWORD = 'resetPassword/REQUEST_RESET_PASSWORD';
export const RESET_PASSWORD = 'resetPassword/RESET_PASSWORD';
/* eslint-enable import/no-unused-modules */

export const requestResetPassword = (email) => ({
  type: REQUEST_RESET_PASSWORD,
  payload: {
    request: {
      method: 'POST',
      url: '/api/webapp/reset-password/',
      data: { email },
    },
  },
});

export const resetPassword = (token, password) => ({
  type: RESET_PASSWORD,
  payload: {
    request: {
      method: 'POST',
      url: `/api/webapp/reset-password/${token}/`,
      data: { password },
    },
  },
});
