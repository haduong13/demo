/* eslint-disable import/no-unused-modules */
export const GET_OWN_ACTIVE_TRANSACTIONS = 'transactions/GET_OWN_ACTIVE_TRANSACTIONS';
export const GET_OWN_ENDED_TRANSACTIONS = 'transactions/GET_OWN_ENDED_TRANSACTIONS';
export const GET_TRANSACTION = 'transactions/GET_TRANSACTION';
export const DO_PREAUTH = 'transactions/DO_PREAUTH';
export const POST_PROCESS_PREAUTH = 'transactions/POST_PROCESS_PREAUTH';
export const ACCEPT_TRANSACTION = 'transactions/ACCEPT_TRANSACTION';
export const REJECT_TRANSACTION = 'transactions/REJECT_TRANSACTION';
export const COMPLETE_TRANSACTION = 'transactions/COMPLETE_TRANSACTION';
export const REQUEST_CANCEL_TRANSACTION = 'transactions/REQUEST_CANCEL_TRANSACTION';
export const REJECT_CANCEL_TRANSACTION = 'transactions/REJECT_CANCEL_TRANSACTION';
export const CANCEL_TRANSACTION = 'transactions/CANCEL_TRANSACTION';
export const EVALUATE_TRANSACTION = 'transactions/EVALUATE_TRANSACTION';
export const FETCH_TRANSACTION_PICTURE = 'transactions/FETCH_TRANSACTION_PICTURE';
export const FETCH_TRANSACTION_VERIFICATION_CODE = 'transactions/FETCH_TRANSACTION_VERIFICATION_CODE';
/* eslint-enable import/no-unused-modules */

export const getOwnActiveTransactions = (page = 1) => ({
  type: GET_OWN_ACTIVE_TRANSACTIONS,
  payload: {
    request: {
      method: 'GET',
      url: '/api/webapp/transactions/',
      params: {
        active: 1,
        page,
      },
    },
  },
});

export const getOwnEndedTransactions = (page = 1) => ({
  type: GET_OWN_ENDED_TRANSACTIONS,
  payload: {
    request: {
      method: 'GET',
      url: '/api/webapp/transactions/',
      params: {
        active: 0,
        page,
      },
    },
  },
});

export const getTransaction = (transactionId) => ({
  type: GET_TRANSACTION,
  transactionId,
  payload: {
    request: {
      method: 'GET',
      url: `/api/webapp/transactions/${transactionId}/`,
    },
  },
});

export const fetchTransactionVerificationCode = (transactionId) => ({
  type: FETCH_TRANSACTION_VERIFICATION_CODE,
  transactionId,
  payload: {
    request: {
      method: 'GET',
      url: `/api/webapp/transactions/${transactionId}/verification-code/`,
    },
  },
});

export const fetchTransactionPicture = (transactionId) => ({
  type: FETCH_TRANSACTION_PICTURE,
  payload: {
    request: {
      method: 'GET',
      url: `/api/webapp/transactions/${transactionId}/picture/`,
    },
  },
});

export const doPreauth = (transactionId, paymentMethodId) => ({
  type: DO_PREAUTH,
  payload: {
    request: {
      method: 'PATCH',
      url: `/api/webapp/transactions/${transactionId}/preauth/`,
      data: { paymentMethodId },
    },
  },
});

export const postProcessPreauth = (transactionId, mangoPreauthId) => ({
  type: POST_PROCESS_PREAUTH,
  transactionId,
  payload: {
    request: {
      method: 'PATCH',
      url: `/api/webapp/transactions/${transactionId}/preauth/post-process/`,
      data: { mangoPreauthId },
    },
  },
});

export const acceptTransaction = (transactionId) => ({
  type: ACCEPT_TRANSACTION,
  transactionId,
  payload: {
    request: {
      method: 'PATCH',
      url: `/api/webapp/transactions/${transactionId}/accept/`,
    },
  },
});

export const rejectTransaction = (transactionId) => ({
  type: REJECT_TRANSACTION,
  transactionId,
  payload: {
    request: {
      method: 'PATCH',
      url: `/api/webapp/transactions/${transactionId}/reject/`,
    },
  },
});

export const completeTransaction = (transactionId, verificationCode = null) => ({
  type: COMPLETE_TRANSACTION,
  transactionId,
  payload: {
    request: {
      method: 'PATCH',
      url: `/api/webapp/transactions/${transactionId}/complete/`,
      data: { verificationCode },
    },
  },
});

export const requestCancelTransaction = (transactionId) => ({
  type: REQUEST_CANCEL_TRANSACTION,
  transactionId,
  payload: {
    request: {
      method: 'PATCH',
      url: `/api/webapp/transactions/${transactionId}/request-cancel/`,
    },
  },
});

export const rejectCancelTransaction = (transactionId) => ({
  type: REJECT_CANCEL_TRANSACTION,
  transactionId,
  payload: {
    request: {
      method: 'PATCH',
      url: `/api/webapp/transactions/${transactionId}/reject-cancel/`,
    },
  },
});

export const cancelTransaction = (transactionId) => ({
  type: CANCEL_TRANSACTION,
  transactionId,
  payload: {
    request: {
      method: 'PATCH',
      url: `/api/webapp/transactions/${transactionId}/cancel/`,
    },
  },
});

export const evaluateTransaction = (transactionId, data) => ({
  type: EVALUATE_TRANSACTION,
  transactionId,
  payload: {
    request: {
      method: 'PATCH',
      url: `/api/webapp/transactions/${transactionId}/evaluate/`,
      data,
    },
  },
});
