import {
  clientId,
  clientSecret,
} from 'helpers/env';
import { refreshMe } from 'logic/actions/meActions';

/* eslint-disable import/no-unused-modules */
export const GET_TOKEN = 'auth/GET_TOKEN';
export const REFRESH_TOKEN = 'auth/REFRESH_TOKEN';
export const LOGOUT = 'auth/LOGOUT';
/* eslint-enable import/no-unused-modules */

export const doGetToken = ({
  email, password,
}) => ({
  type: GET_TOKEN,
  noAuth: true,
  payload: {
    request: {
      method: 'POST',
      url: '/oauth/v2/token/',
      data: {
        username: email,
        password,
        client_id: clientId,
        client_secret: clientSecret,
        grant_type: 'password',
      },
    },
  },
});

export const getToken = ({
  email,
  password,
}) => (dispatch) => new Promise((resolve, reject) => {
  dispatch(doGetToken({
    email,
    password,
  }))
    .then(() => {
      resolve(dispatch(refreshMe()));
    })
    .catch((err) => {
      reject(err);
    });
});

export const doRefreshToken = (token) => ({
  type: REFRESH_TOKEN,
  noAuth: true,
  payload: {
    request: {
      method: 'POST',
      url: '/oauth/v2/token/',
      data: {
        refresh_token: token,
        client_id: clientId,
        client_secret: clientSecret,
        grant_type: 'refresh_token',
      },
    },
  },
});

export const refreshToken = (token) => (dispatch) => new Promise((resolve, reject) => {
  dispatch(doRefreshToken(token))
    .then(() => {
      resolve(dispatch(refreshMe()));
    })
    .catch((err) => {
      reject(err);
    });
});

export const logout = () => ({
  type: LOGOUT,
  payload: {
    request: {
      method: 'POST',
      url: '/api/webapp/users/logout/',
    },
  },
});
