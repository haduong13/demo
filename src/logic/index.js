import {
  createStore,
  applyMiddleware,
  combineReducers,
} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import {
  persistStore,
  persistCombineReducers,
} from 'redux-persist';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import thunk from 'redux-thunk';
import axiosMiddleware from 'redux-axios-middleware';
import axios from 'axios';
import * as Sentry from '@sentry/react';
import deepfilter from 'deep-filter';

import storage from 'redux-persist/lib/storage';

import {
  persistentReducers,
  // volatileReducers,
} from 'logic/reducers';
import {
  apiUrl,
  isProd,
} from 'helpers/env';

import { logout } from 'logic/actions/authActions';

const persistConfig = {
  key: 'persistent',
  storage,
  stateReconciler: autoMergeLevel2,
};

const persistentReducer = persistCombineReducers(
  persistConfig,
  persistentReducers,
);
// const volatileReducer = combineReducers(
  // volatileReducers,
// );
const rootReducer = combineReducers({
  persistent: persistentReducer,
  // volatile: volatileReducer,
});

const client = axios.create({
  baseURL: apiUrl,
  responseType: 'json',
});

const axiosMiddlewareOptions = {
  returnRejectedPromiseOnError: true,
  interceptors: {
    request: [
      ({
        getState, getSourceAction,
      }, request) => {
        const {
          persistent: {
            authReducer: {
              accessToken,
              loggedIn = false,
            } = {},
          } = {},
        } = getState();

        const { noAuth = false } = getSourceAction(request);

        if (loggedIn && !accessToken && !isProd) {
          console.error('Logged in but no accessToken !'); // eslint-disable-line no-console
        }

        if (accessToken && !noAuth) {
          request.headers.Authorization = `Bearer ${accessToken}`;
        }

        return request;
      },
    ],
    response: [
      {
        success(c, res) {
          // Filter out null values to allow for simplier destructuring (null values become undefined, so default values are used instead)
          res.data = deepfilter(res.data, (v) => (v !== null));

          return Promise.resolve(res);
        },
        error({
          getState,
          dispatch,
        }, error = {}) {
          const {
            response: {
              status: responseStatus = 0,
              data: responseData = {},
            } = {},
            config: {
              method: requestMethod = '',
              url: requestUrl = '',
            } = {},
          } = error;

          const { persistent: { authReducer: { loggedIn = false } = {} } = {} } = getState();

          let errorMessage = `${requestMethod.toUpperCase()} ${requestUrl}: `;
          if (responseStatus) {
            if (responseData) {
              errorMessage += `${responseStatus} error from backend with data: ${JSON.stringify(responseData)}`;
            } else {
              errorMessage += `${responseStatus} error`;
            }
          } else {
            errorMessage += 'No response from backend';
          }

          if (!isProd) {
            console.error(errorMessage); // eslint-disable-line no-console
          }

          switch (responseStatus) {
            case 429:
              alert('Tu as effectué trop de tentatives. Attends quelques minutes avant d\'essayer à nouveau.'); // eslint-disable-line no-alert
              break;

            case 401:
              if (loggedIn) {
                dispatch(logout());
              }
              break;

            case 0:
              break;

            default:
              Sentry.captureException(errorMessage);
          }

          return Promise.reject(error);
        },
      },
    ],
  },
};

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk, axiosMiddleware(client, axiosMiddlewareOptions))),
);

const persistor = persistStore(store);

export {
  persistor,
  store,
};
