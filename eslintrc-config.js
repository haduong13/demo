module.exports = {
  root: true,
  env: {
    browser: true,
    es6: true,
  },
  parser: 'babel-eslint',
  extends: [
    'airbnb',
    'plugin:json/recommended',
  ],
  plugins: [
    'autofix',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  rules: {
    'quotes': [2, 'single'],
    'linebreak-style': 0,
    'jsx-a11y/label-has-associated-control': 0,
    'react/style-prop-object': [2, { 'allow': ['FormattedNumber'] }],
    'no-plusplus': [2, { 'allowForLoopAfterthoughts': true }],
    'no-shadow': 0,
    'react/jsx-filename-extension': 0,
    'react/jsx-props-no-spreading': 0,
    'max-len': 0,
    'no-param-reassign': ['error', { 'ignorePropertyModificationsFor': ['state'] }],
    'radix': ['error', 'as-needed'],
    'react/prop-types': 0,
    'import/no-unresolved': 0,
    'react-hooks/exhaustive-deps': 0,
  },
};